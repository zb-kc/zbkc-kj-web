// import * as echarts from 'echarts'
export default function (props) {
  let vacantArea = Math.max(...props.yAxisDataz)
  let vacantFloor = Math.floor(vacantArea)
  let len = String(vacantFloor).length
  let num = 100
  for (let i = 0; i <= len.length; i++) {
    num = num * 10
  }
  let footArr = []
  props.yAxisDataz && Array.isArray(props.yAxisDataz) && props.yAxisDataz.length > 0 && props.yAxisDataz.forEach((item, index) => {
    footArr[index] = (1 / num)
  })
  return {
    tooltip: {
      trigger: 'axis',
      borderColor: 'none',
      backgroundColor: 'transparent',
      borderWidth: 0,
      padding: 0,
      textStyle: {
        color: 'white'
      },
      formatter: function (params) {
        var str = `<div style = " 
        border:1px solid #02fdff;
        background-color: rgba(0,0,0,0.5);
        text-align: left;
        padding:5px 12px;
        >
         <span style=" display: inline-block; margin-top:40px; margin-right:40px;">${params[0].name}</span>
         <br/>
         <span style="display: inline-block; margin-right:16px; margin-bottom: 12px;">
         ${params[0].marker}
         <span>${params[0].seriesName}&nbsp;&nbsp;&nbsp;&nbsp;</span>
         <span style="color:#02fdff;">${params[0].value}万元</span>
         <br/>
       
         ${params[1].marker}
         <span>${params[1].seriesName}&nbsp;&nbsp;&nbsp;&nbsp;</span>
         <span style="color:#02fdff;">${params[1].value}元/㎡</span>
        </span>
        `
        return str
      }
    },
    color: ['#01ffff', '#fdef00'],
    legend: {
      // icon: 'rect',
      left: 'center',
      itemWidth: 20,
      itemHeight: 10,
      itemGap: 20,
      textStyle: {
        fontSize: 14,
        color: '#fff'
      },
      selectedMode: false, // 图例点击失效
      data: props.legendData ? props.legendData : []
    },
    grid: {
      top: '18%',
      left: '3%',
      right: '3%',
      bottom: '9%',
      containLabel: true
    },
    xAxis: [
      {
        type: 'category',
        data: props.xAxisData ? props.xAxisData : [],
        axisPointer: {
          type: 'shadow'
        },
        axisTick: {
          show: false
        },
        offset: 4,
        axisLine: {
          show: true,
          lineStyle: {
            type: 'dashed',
            opacity: 0.2
          }
        },

        axisLabel: {
          show: true,
          interval: 0,
          textStyle: {
            color: '#fff', // 坐标值得具体的颜色
            lineHeight: 16 // 图例文字行高
          }
        }
      }
    ],
    yAxis: [
      {
        type: 'value',
        // name: '（万元）',
        nameTextStyle: {
          color: '#fff',
          padding: [0, 0, 0, 5, 0]
        },
        min: 0,
        // max: 1600, // vacant
        // interval: (1600 / 5), // vacant
        axisLabel: {
          formatter: '{value}',
          textStyle: {
            color: '#fff' // 坐标值得具体的颜色
          }
        },
        axisLine: {
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2
          }
        },
        splitLine: { // x轴底线
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2 // x轴底线透明度
          }
        }
      },
      {
        type: 'value',
        // name: '（元/㎡）',
        nameTextStyle: { // 单位字体颜色
          color: '#fff',
          padding: [0, 0, 0, 5, 0]
        },
        min: 0,
        // max: 20, // price
        // interval: (20 / 5), // price
        axisLabel: {
          formatter: '{value}'
        },
        axisLine: {
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2
          }
        },
        splitLine: { // y轴底线
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2 // y轴底线透明度
          }
        }
      }
    ],
    series: [
      {
        name: props.legendData[0],
        type: 'pictorialBar',
        barCategoryGap: '0%',
        symbol: 'path://M0,10 L10,10 C5.5,10 5.5,5 5,0 C4.5,5 4.5,10 0,10 z',
        label: {
          show: true,
          position: 'insideBottom',
          //distance: 10,
          color: 'rgba(0,213,255,0.5)',
        },
        itemStyle: {
          normal: {
            color: {
              type: 'linear',
              x: 0,
              y: 1,
              x2: 0,
              y2: 0,
              colorStops: [
                {
                  offset: 1,
                  color: '#00d5ff'
                },
                {
                  offset: 0.2,
                  color: 'rgba(0, 213, 255, 0.3)'
                },
                {
                  offset: 0,
                  color: 'rgba(0,0,0,.2)'
                }
              ],
              global: false //  缺省为  false
            }
          },
          emphasis: {
            opacity: 1
          }
        },
        data: props.yAxisDataz
      },
      {
        name: props.legendData[1],
        type: 'line',
        yAxisIndex: 1,
        symbol: 'circle',
        symbolSize: 6,
        zlevel: 2,
        itemStyle: {
          color: '#fff143',
          borderColor: '#fff143', //  拐点边框颜色
          borderWidth: 1, //  拐点边框宽度
          shadowColor: '#fff143', //  阴影颜色
          shadowBlur: 10, //  阴影渐变范围控制
          emphasis: { // 突出效果配置(鼠标置于拐点上时)
            borderColor: '#fff143', //  拐点边框颜色
            borderWidth: 2, //  拐点边框宽度
            shadowColor: '#fff143', //  阴影颜色
            shadowBlur: 14 //  阴影渐变范围控制
          }
        },
        lineStyle: {
          // type: 'dashed'
          width: 1, // 折线粗线
          color: '#fff143',
          shadowColor: '#fff143',
          shadowBlur: 6,
          zlevel: 2
        },
        data: props.yAxisDatas,
        label: {
          show: true,
          position: 'top',
          //distance: 10,
          color: '#fff143',
        }
      }
    ],
    dataZoom: [
      {
        type: 'inside',
        show: true,
        height: 10,
        start: 0,
        end: props.xAxisData.length > 13 ? 28 : 100
      }
    ]
  }
}
