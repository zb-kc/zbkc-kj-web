import {get, post} from '@/until/request'

// 首页
export function bigScreenData () {
  return get('/zbkc/big/wyAssetInfo/home/info')
}

// 产业用房-地表
export function landmarkData () {
  return get('/zbkc/big/wyAssetInfo/map/cyStreet')
}

// 政府物业-房屋编码核查
export function govermentHouseCodeCheck () {
  return get('/zbkc/big/wyAssetInfo/house/code')
}

// 请求详细地址
export function groupstatus (obj) {
  return post('/zbkc/sysOrg/status?' + obj)
}

// 五大类基本信息统计
export function fiveListData (index) {
  return post('/zbkc/big/wyAssetInfo/cy/wyFivelist/' + index)
}

// 五大类：产业用房-预到期企业
export function industryPreCompany (obj) {
  return post('/zbkc/big/wyAssetInfo/cy/ydqlist', obj)
}

// 五大类：物业列表
export function industryHouseList (obj) {
  return post('/zbkc/big/wyAssetInfo/cy/cywylist', obj)
}

// 后四类：物业统计
export function propertyStatisticsList (index) {
  return get('/zbkc/big/wyAssetInfo/cy/wy/' + index)
}

// 租赁社会物业左侧边数据统计
export function rentStatisticsList () {
  return get('/zbkc/big/wyAssetInfo/zl/basic/info')
}

// 租赁社会物业-列表
export function rentWyList (obj) {
  return post('/zbkc/big/wyAssetInfo/zl/wy/list', obj)
}

// 租赁社会物业-更多分析
export function rentMoreAnalysis () {
  return get('/zbkc/big/wyAssetInfo/zl/more/analysis')
}

// 产业用房-物业列表-园区详情
export function parkDetailList (id) {
  return get('/zbkc/big/wyAssetInfo/cy/details/' + id)
}

// 产业用房_扶持效益分析
export function supportAnalysis () {
  return get('/zbkc/big/wyAssetInfo/cy/regTax')
}

// 产业用房-统计分析
export function propertyAnalysis () {
  return get('/zbkc/big/wyAssetInfo/cy/analysis')
}

// 政府物业-更多分析
export function governmentAnalysis () {
  return get('/zbkc/big/wyAssetInfo/zf/analysis')
}

// 物业列表-详情（左侧+中弹窗）
export function homeDataListDetail (id) {
  return get('/zbkc/big/wyAssetInfo/wylist/details/' + id)
}

// 首页—物业资产两个列表
export function homeProperList (obj) {
  return post('/zbkc/big/wyAssetInfo/home/wy/assetsList', obj)
}

// 首页-智能选址-社区名称列表
export function homeSocietyList () {
  return get('zbkc/big/wyAssetInfo/home/wy/communityName')
}

// 首页-智能选址-街道名称列表
export function homeStreetList () {
  return get('/zbkc/big/wyAssetInfo/home/wy/streetName')
}

// 首页-智能选址-物业种类列表
export function homePropertyType () {
  return get('zbkc/big/wyAssetInfo/home/wy/wyTypeList')
}

// 政府物业-智能选址-物业查询列表
export function homeSearchList (obj) {
  return post('/zbkc/big/wyAssetInfo/home/wy/list', obj)
}

// 政府物业-产业用房-预到期列表-详情
export function preDueListDetail (code) {
  return get('/zbkc/big/wyAssetInfo/qy/details/' + code)
}

// 政府物业——产业用房——重点企业
export function KeyCompanyData () {
  return get('/zbkc/big/wyAssetInfo/cy/zdqy/')
}

// 政府物业——产业用房——重点企业_类别列表
export function companyTypeDataList (obj) {
  return post('/zbkc/big/wyAssetInfo/cy/zdqy/list/', obj)
}

// 租赁物业-更多分析
export function leaseMoreAnalysis () {
  return get('/zbkc/big/wyAssetInfo/zl/more/analysis')
}

// 租赁物业-更多分析
export function unitAreaStatistics (obj) {
  return get('/zbkc/big/wyAssetInfo/zl/more/unit/' + obj)
}

// 产业用房——园区详情_入驻企业
export function parkStatistics (obj) {
  return get('/zbkc/big/wyAssetInfo/cy/details/rzDetailsList/' + obj)
}

// 产业用房——园区详情_房屋信息
export function parkHouseInfoList (obj) {
  return post('/zbkc/big/wyAssetInfo/cy/details/house/info/', obj)
}

// 产业用房——园区详情_空置详情
export function vacancyInfoList (obj) {
  return post('/zbkc/big/wyAssetInfo/cy/details/kzDetailsList/', obj)
}

// 产业用房——园区详情_入驻企业列表
export function settleCompanyList (obj) {
  return post('/zbkc/big/wyAssetInfo/cy/details/rzqyList/', obj)
}

// 首页—房屋编码核查任务列表
export function homeCheckList (obj) {
  return post('/zbkc/big/wyAssetInfo/house/codeList/', obj)
}

// 产业用房列表-详情-更多-总体分析
export function totalAnalysis (obj) {
  return get('/zbkc/big/wyAssetInfo/cy/details/more/analysis/' + obj)
}

// 产业用房列表-企业详情-扶持效益
export function detailsAnalysis (obj) {
  return get('/zbkc/big/wyAssetInfo/qy/regTax/' + obj)
}

// 退租原因列表
export function rentData (obj) {
  console.log(obj, '---obj---')
  return post('/zbkc/big/wyAssetInfo/home/wy/analysis/', obj)
}
