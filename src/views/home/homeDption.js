import * as echarts from 'echarts'
export default function (props) {
  return {
    tooltip: {
      trigger: 'axis',
      borderColor: 'none',
      backgroundColor: 'transparent',
      borderWidth: 0,
      padding: 0,
      textStyle: {
        color: 'white'
      },
      formatter: function (params) {
        var str = `<div style = "width:120px; height:80px; 
                                 background:url( ${require('../img/tipsbg.png')}) no-repeat center center;
                                 background-size:100% 100%; 
                                 text-align: left;
                                 padding-left:16px;
                                 padding-top:10px;
                                 >
        <span style=" display: inline-block; margin-top:40px;">${params[0].name}</span>
        <br/>
         <span>
         <span style="display: inline-block; 
                      width:8px; 
                      height:8px; 
                      border-radius: 4px;
                      background-color: #13c8c8;"
                      >
         </span>
         <span>${params[0].seriesName}&nbsp;&nbsp;&nbsp;&nbsp;</span>
         <span>${params[0].value}处</span>
         </span>
        </span>`
        return str
      }
    },
    legend: {
      left: 'center',
      textStyle: {
        fontSize: 14,
        color: '#fff',
        opacity: 1
      },
      data: ['总量']
    },
    grid: {
      left: '0',
      right: '0',
      bottom: '4%',
      containLabel: true
    },
    xAxis: [
      {
        type: 'category',
        data: props.xAxisDtat,
        axisPointer: {
          type: 'shadow'
        },
        axisTick: {
          show: false
        },
        axisLine: {
          show: true,
          lineStyle: {
            type: 'dashed',
            opacity: 0.2
          }
        },
        axisLabel: {
          interval: 0,
          rotate: 45, // 角度顺时针计算的
          textStyle: {
            color: '#fff' // 坐标值得具体的颜色
          }
        }
      }
    ],
    yAxis: [
      {
        type: 'value',
        name: '单位:处',
        nameTextStyle: {
          color: '#fff'
        },
        min: 0,
        // max: 400,
        // interval: 100,
        axisLabel: {
          formatter: '{value} 处',
          textStyle: {
            color: '#fff' // 坐标值得具体的颜色
          }
        },
        axisLine: {
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2
          }
        },
        splitLine: { // x轴底线
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2 // x轴底线透明度
          }
        }
      }
      // {
      //   type: 'value',
      //   name: '单位：%',
      //   nameTextStyle: { // 单位字体颜色
      //     color: '#fff'
      //   },
      //   min: 0,
      //   max: 20,
      //   interval: 5,
      //   axisLabel: {
      //     formatter: '{value} %'
      //   },
      //   axisLine: {
      //     show: true,
      //     lineStyle: {
      //       color: ['#00FFFF'],
      //       type: 'dashed',
      //       opacity: 0.2
      //     }
      //   },
      //   splitLine: { // x轴底线
      //     show: true,
      //     lineStyle: {
      //       color: ['#00FFFF'],
      //       type: 'dashed',
      //       opacity: 0.2 // x轴底线透明度
      //     }
      //   }
      // }
    ],
    series: [
      {
        name: '总量',
        type: 'bar',
        barWidth: 15,
        barGap: '-100%',
        color: ['#00FFFF'], // 折线条的颜色
        itemStyle: { // lenged文本
          opacity: 0.7,
          color: function (params) {
            return new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
              offset: 0,
              color: '#00FFFF' // 0% 处的颜色
            }, {
              offset: 1,
              color: '#00FFFF' // 100% 处的颜色
            }], false)
          }
        },
        data: props.yAxisDataz ? props.yAxisDataz : []
      },
      { // 下半截柱子顶部圆片
        name: '',
        type: 'pictorialBar',
        symbolSize: [15, 5],
        symbolOffset: [0, -2],
        z: 12,
        itemStyle: {
          opacity: 1,
          color: function (params) {
            return new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
              offset: 0,
              color: '#00FFFF' // 0% 处的颜色
            }, {
              offset: 1,
              color: '#00FFFF' // 100% 处的颜色
            }], false)
          }
        },
        label: {
          show: true,
          position: 'top',
          fontSize: 16,
          color: '#fff'
        },
        symbolPosition: 'end',
        data: props.yAxisDataz ? props.yAxisDataz : []
      },
      { // 下半截柱子底部圆片
        name: '',
        type: 'pictorialBar',
        symbolSize: [15, 5],
        symbolOffset: [0, 3],
        z: 12,
        itemStyle: {
          opacity: 1,
          color: function (params) {
            return new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
              offset: 0,
              color: '#00FFFF' // 0% 处的颜色
            }, {
              offset: 1,
              color: '#00FFFF' // 100% 处的颜色
            }], false)
          }
        },
        data: [1, 1, 1, 1, 1]
      }
      // {
      //   name: '增速',
      //   type: 'line',
      //   yAxisIndex: 1,
      //   symbol: 'circle',
      //   symbolSize: 8,
      //   itemStyle: {
      //     color: '#FFF100',
      //     borderColor: '#d0bf00', //  拐点边框颜色
      //     borderWidth: 1, //  拐点边框宽度
      //     shadowColor: '#efdf00', //  阴影颜色
      //     shadowBlur: 10, //  阴影渐变范围控制
      //     emphasis: { // 突出效果配置(鼠标置于拐点上时)
      //       borderColor: '#ffff7f', //  拐点边框颜色
      //       borderWidth: 2, //  拐点边框宽度
      //       shadowColor: '#efdf00', //  阴影颜色
      //       shadowBlur: 14 //  阴影渐变范围控制
      //     }
      //   },
      //   lineStyle: {
      //     width: 2, // 折线粗线
      //     color: '#FFF100'
      //     // type: 'dashed'
      //   },
      //   data: props.yAxisDatas
      // }
    ]
  }
}
