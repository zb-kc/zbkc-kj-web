import * as echarts from 'echarts'
export default function (props) {
  let maxArr = [...props.yAxisDataz, ...props.yAxisDatas]
  let vacantArea = Math.max(...maxArr)
  let vacant = Math.ceil(vacantArea)
  let vacantFloor = Math.floor(vacantArea)

  let price = 100

  let len = String(vacantFloor).length
  let num = 1
  for (let i = 0; i <= len.length; i++) {
    num = num * 10
  }
  let baseData = []
  props.yAxisDataz && Array.isArray(props.yAxisDataz) && props.yAxisDataz.length > 0 && props.yAxisDataz.forEach((item, index) => {
    baseData[index] = (1 / num)
  })
  return {
    tooltip: {
      trigger: 'axis',
      borderColor: 'none',
      backgroundColor: 'transparent',
      borderWidth: 0,
      padding: 0,
      textStyle: {
        color: 'white'
      },
      axisPointer: {
        lineStyle: {
          color: {
            type: 'linear',
            x: 0,
            y: 0,
            x2: 0,
            y2: 1,
            colorStops: [{
              offset: 0,
              color: 'rgba(126,199,255,0)' // 0% 处的颜色
            }, {
              offset: 0.5,
              color: 'rgba(126,199,255,1)' // 100% 处的颜色
            }, {
              offset: 1,
              color: 'rgba(126,199,255,0)' // 100% 处的颜色
            }],
            global: false // 缺省为 false
          }
        }
      },
      formatter: (params) => {
        var str = `<div style = "
        background:url( ${require('../../../img/tipsbg.png')}) no-repeat center center;
        background-size:100% 100%;
        text-align: left;
        padding:12px;
        >
        <span style=" display: inline-block; margin-top:40px;">年份：${params[0].axisValue}</span>
        <br/>
        ${params[0].marker}
        <span>${params[0].seriesName}&nbsp;&nbsp;</span>
        <span>${params[0].value}${props.unit ? props.unit : ''}</span>
        <br/>
        ${params[1].marker}
        <span>${params[1].seriesName}&nbsp;&nbsp;</span>
        <span>${params[1].value}${props.unit ? props.unit : ''}</span>
        <br/>
        ${params[2].marker}
        <span>${params[2].seriesName}&nbsp;&nbsp;</span>
        <span>${params[2].value}${props.unit ? props.unit : ''}</span>
        </div>
        `
        return str
      }
    },
    textStyle: {
      color: '#C9C9C9',
      axisPointer: {
        lineStyle: {
          color: {
            type: 'linear',
            x: 0,
            y: 0,
            x2: 0,
            y2: 1,
            colorStops: [{
              offset: 0,
              color: 'rgba(126,199,255,0)' // 0% 处的颜色
            }, {
              offset: 0.5,
              color: 'rgba(126,199,255,1)' // 100% 处的颜色
            }, {
              offset: 1,
              color: 'rgba(126,199,255,0)' // 100% 处的颜色
            }],
            global: false // 缺省为 false
          }
        }
      }
    },
    legend: {
      type: 'scroll',
      orient: 'horizontal',
      selectedMode: false, // 图例点击失效
      right: '10%',
      top: '0',
      icon: 'circle',
      textStyle: {
        color: '#ffffff',
        fontSize: 14
      }
    },
    grid: {
      left: '2%',
      top: '21%',
      bottom: '3%',
      right: '10%',
      containLabel: true
    },
    xAxis: {
      type: 'category',
      data: props.xAxisDtat,
      axisLine: {
        show: false,
        lineStyle: {
          color: '#B5B5B5'
        }
      },
      axisTick: {
        show: false
      },
      axisLabel: {
        textStyle: {
          fontFamily: 'Microsoft YaHei',
          color: '#FFF',
          padding: 10
        },
        fontSize: 14,
        rotate: 45 // 角度顺时针计算的
      }
    },
    yAxis: [
      {
        type: 'value',
        name: '单位:㎡',
        nameTextStyle: {
          color: '#fff',
          padding: [0, 0, 0, 5, 0]
        },
        min: 0,
        max: vacant,
        interval: (vacant / 5),
        position: 'left',
        axisLabel: {
          formatter: '{value}',
          textStyle: {
            color: '#fff' // 坐标值得具体的颜色
          }
        },
        axisLine: {
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2
          }
        },
        splitLine: { // x轴底线
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2 // x轴底线透明度
          }
        }
      },
      {
        type: 'value',
        name: '单位：%',
        nameTextStyle: { // 单位字体颜色
          color: '#fff',
          padding: [0, 0, 0, 5, 0]
        },
        min: 0,
        max: price,
        interval: (price / 5),
        position: 'right',
        axisLabel: {
          formatter: '{value}',
          textStyle: {
            color: '#fff' // 坐标值得具体的颜色
          }
        },
        axisLine: {
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2
          }
        },
        splitLine: { // y轴底线
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2 // y轴底线透明度
          }
        }
      }
    ],
    series: [
      {
        type: 'bar',
        name: props.legendData[0],
        barGap: 0,
        data: props.yAxisDataz,
        barMaxWidth: 'auto',
        barWidth: 14,
        itemStyle: {
          color: {
            x: 0,
            y: 0,
            x2: 0,
            y2: 1,
            type: 'linear',
            global: false,
            colorStops: [
              {
                offset: 0,
                color: '#2450F0'
              },
              {
                offset: 1,
                color: '#2FCAFD'
              }
            ]
          }
        }
      },
      {
        name: props.legendData[1],
        type: 'bar',
        barGap: 0,
        data: props.yAxisDatas,
        barMaxWidth: 'auto',
        barWidth: 14,
        itemStyle: {
          color: {
            x: 0,
            y: 0,
            x2: 0,
            y2: 1,
            type: 'linear',
            global: false,
            colorStops: [
              {
                offset: 0,
                color: '#1A4DC0'
              },
              {
                offset: 1,
                color: '#9E2FFD'
              }
            ]
          }
        }
      },
      {
        name: props.legendData[2],
        type: 'line',
        barGap: 0,
        data: props.yAxisDatax,
        smooth: true,
        symbol: 'none',
        itemStyle: {
          normal: {
            lineStyle: {
              color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                {
                  offset: 0,
                  color: 'rgba(255, 227, 168, 0)'
                },
                {
                  offset: 0.5,
                  color: 'rgba(255, 227, 168, 1)'
                },
                {
                  offset: 1,
                  color: 'rgba(255, 227, 168, 0)'
                }
              ]),
              shadowColor: 'rgba(255, 120, 0,1)',
              shadowBlur: 8
            },
            areaStyle: {
              color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                {
                  offset: 0,
                  color: 'rgba(197, 106, 255, 1)'
                },
                {
                  offset: 0.6,
                  color: 'rgba(255, 120, 0, 0)'
                }
              ])
            }
          }
        }
      },
      {
        data: baseData,
        type: 'pictorialBar',
        barMaxWidth: '20',
        symbol: 'diamond',
        symbolOffset: ['-50%', '56%'],
        symbolSize: [14, 14],
        zlevel: 1,
        itemStyle: {
          normal: {
            color: '#2FCAFD'
          }
        }
      },
      {
        data: props.yAxisDataz,
        type: 'pictorialBar',
        barMaxWidth: '20',
        symbolPosition: 'end',
        symbol: 'diamond',
        symbolOffset: ['-50%', '-50%'],
        symbolSize: [14, 14],
        zlevel: 2,
        itemStyle: {
          normal: {
            borderWidth: 0,
            color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
              {
                offset: 0,
                color: '#2450F0'
              },
              {
                offset: 1,
                color: '#2FCAFD'
              }
            ])

          }
        }
      },
      {
        data: baseData,
        type: 'pictorialBar',
        barMaxWidth: '20',
        symbolPosition: 'end',
        symbol: 'diamond',
        symbolOffset: ['50%', '-36%'],
        symbolSize: [14, 14],
        zlevel: 1,
        itemStyle: {
          normal: {
            color: '#9b2ffb'
          }
        }
      },
      {
        data: props.yAxisDatas,
        type: 'pictorialBar',
        barMaxWidth: '20',
        symbolPosition: 'end',
        symbol: 'diamond',
        symbolOffset: ['50%', '-50%'],
        symbolSize: [14, 14],
        zlevel: 2,
        itemStyle: {
          normal: {
            borderWidth: 0,
            color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
              {
                offset: 0,
                color: '#886DF8'
              },
              {
                offset: 1,
                color: '#4781F0'
              }
            ])

          }
        }
      }
    ]
  }
}
