import * as echarts from 'echarts'
export default function (props) {
  return {
    tooltip: {
      trigger: 'axis',
      borderColor: 'none',
      backgroundColor: 'transparent',
      borderWidth: 0,
      padding: 0,
      textStyle: {
        color: 'white'
      },
      axisPointer: {
        lineStyle: {
          color: {
            type: 'linear',
            x: 0,
            y: 0,
            x2: 0,
            y2: 1,
            colorStops: [{
              offset: 0,
              color: 'rgba(126,199,255,0)' // 0% 处的颜色
            }, {
              offset: 0.5,
              color: 'rgba(126,199,255,1)' // 100% 处的颜色
            }, {
              offset: 1,
              color: 'rgba(126,199,255,0)' // 100% 处的颜色
            }],
            global: false // 缺省为 false
          }
        }
      },
      formatter: (params) => {
        var str = `<div style = " 
        border:2px solid #02436a;
        box-shadow: 0 0 6px 1px #02436a inset;
        background-color: #1195aa;
        text-align: left;
        padding:16px;
        >
        ${params[0].marker}
        ${params[0].marker}
        <span>${params[0].seriesName}&nbsp;&nbsp;</span>
        <span>${params[0].value}${props.unit ? props.unit : ''}</span>
        <br/>
        ${params[1].marker}
        <span>${params[1].seriesName}&nbsp;&nbsp;</span>
        <span>${params[1].value}${props.unit ? props.unit : ''}</span>
        </div>
        `
        return str
      }
    },
    legend: {
      textStyle: {
        color: '#fff',
        fontSize: 14
      },
      top: '3%',
      selectedMode: false, // 图例点击失效
      itemGap: 25,
      itemWidth: 18,
      icon: 'circle',
      data: props.tipData
    },
    grid: {
      top: '16%',
      left: '2%',
      right: '14%',
      bottom: '6%',
      containLabel: true
    },
    xAxis: [{
      type: 'category',
      boundaryGap: false,
      axisLine: { // 坐标轴轴线相关设置。数学上的x轴
        show: true,
        lineStyle: {
          color: '#233653',
          type: 'dashed',
          opacity: 0.2 // x轴第一根底线透明度
        }
      },
      axisLabel: { // 坐标轴刻度标签的相关设置
        textStyle: {
          color: '#fff',
          padding: 8,
          fontSize: 14
        },
        formatter: function (data) {
          return data
        }
      },
      splitLine: { // y轴底线
        show: false,
        lineStyle: {
          color: ['#00FFFF'],
          type: 'dashed',
          opacity: 0.2 // y轴底线透明度
        }
      },
      axisTick: {
        show: false
      },
      data: props.xLabel
    }],
    yAxis: [
      {
        name: '单位：家',
        nameTextStyle: {
          color: '#fff',
          fontSize: 12,
          padding: 10
        },
        min: 0,
        // max: vacant,
        // interval: (vacant / 5),
        splitLine: {
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.5
          }
        },
        axisLine: { // y轴第一根底线配置
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.5
          }
        },
        axisLabel: {
          show: true,
          textStyle: {
            color: '#fff'
            // padding: 16
          },
          formatter: function (value) {
            if (value === 0) {
              return value
            }
            return value
          }
        },
        axisTick: {
          show: false
        }
      }],
    series: [{
      name: props.tipData[0].name,
      type: 'line',
      symbol: 'circle', // 默认是空心圆（中间是白色的），改成实心圆
      showAllSymbol: true,
      symbolSize: 6, // 折线园点大小
      smooth: true,
      // stack: 'all',
      lineStyle: {
        normal: {
          width: 2,
          color: 'rgba(25,163,223,1)' // 线条颜色
        },
        borderColor: 'rgba(0,0,0,.4)'
      },
      itemStyle: {
        color: 'rgba(25,163,223,1)'// 折线园点颜色
        // borderColor: '#646ace'
        // borderWidth: 2
      },
      tooltip: {
        show: true
      },
      areaStyle: { // 区域填充样式
        normal: {
          // 线性渐变，前4个参数分别是x0,y0,x2,y2(范围0~1);相当于图形包围盒中的百分比。如果最后一个参数是‘true’，则该四个值是绝对像素位置。
          color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
            offset: 0,
            color: 'rgba(25,163,223,.3)'

          },
          {
            offset: 1,
            color: 'rgba(25,163,223, 0)'
          }
          ], false),
          shadowColor: 'rgba(25,163,223, 0.5)', // 阴影颜色
          shadowBlur: 20 // shadowBlur设图形阴影的模糊大小。配合shadowColor,shadowOffsetX/Y, 设置图形的阴影效果。
        }
      },
      data: props.goToSchool
    }, {
      name: props.tipData[1].name,
      type: 'line',
      symbol: 'circle', // 默认是空心圆（中间是白色的），改成实心圆
      showAllSymbol: true,
      symbolSize: 6, // 折线园点大小
      smooth: true,
      // stack: 'all',
      lineStyle: {
        normal: {
          width: 2,
          color: '#c619ff' // 线条颜色
        },
        borderColor: 'rgba(210,230,255,.4)'
      },
      itemStyle: {
        color: '#c619ff'// 折线园点颜色
        // borderColor: '#646ace',
        // borderWidth: 2
      },
      tooltip: {
        show: true
      },
      areaStyle: { // 区域填充样式
        normal: {
          // 线性渐变，前4个参数分别是x0,y0,x2,y2(范围0~1);相当于图形包围盒中的百分比。如果最后一个参数是‘true’，则该四个值是绝对像素位置。
          color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
            offset: 0,
            color: 'rgba(210,230,255,.3)'
          },
          {
            offset: 1,
            color: 'rgba(210,230,255, 0)'
          }
          ], false),
          shadowColor: 'rgba(210,230,255, 0.5)', // 阴影颜色
          shadowBlur: 20 // shadowBlur设图形阴影的模糊大小。配合shadowColor,shadowOffsetX/Y, 设置图形的阴影效果。
        }
      },
      data: props.goOutSchool
    }]
  }
}
