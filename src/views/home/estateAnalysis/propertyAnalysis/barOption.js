// import * as echarts from 'echarts'
export default function (props) {
  let baseData = []
  for (let i = 0; i < ((props.data)[0]).length; i++) {
    baseData[i] = {
      type: 'bar',
      barMaxWidth: '10',
      itemStyle: {
        normal: {
          color: props.color[i]
        }
      }
    }
  }
  return {
    tooltip: {
      trigger: 'item',
      borderColor: 'none',
      backgroundColor: 'transparent',
      borderWidth: 0,
      padding: 0,
      textStyle: {
        color: 'white'
      },
      formatter: function (params) {
        var str = `<div style = "
                                 border:2px solid #02436a;
                                 box-shadow: 0 0 6px 1px #02436a inset;
                                 background-color: #1195aa;
                                 text-align: left;
                                 padding-left:16px;
                                 padding-top:10px;
                                 >
         <span style=" display: inline-block; margin-top:40px; margin-right:40px;">${params.name}</span>
         <br/>
         <span style="display: inline-block; margin-right:16px; margin-bottom: 12px;">
         ${params.marker}
         <span>${params.seriesName}&nbsp;&nbsp;&nbsp;&nbsp;</span>
         <span>${params.value[params.seriesIndex + 1]}处</span>
        </span>
        `
        return str
      }
    },
    legend: {
      type: 'plain',
      orient: 'horizontal',
      selectedMode: false, // 图例点击失效
      left: '1%',
      right: '1%',
      bottom: '3%',
      icon: 'circle',
      textStyle: {
        color: '#ffffff',
        fontSize: 14
      }
    },
    grid: {
      containLabel: true,
      left: '1.5%',
      top: '10%',
      bottom: '24%',
      right: '1.5%'
    },
    xAxis: {
      type: 'category',
      axisLine: {
        show: false,
        lineStyle: {
          color: '#B5B5B5'
        }
      },
      axisTick: {
        show: false
      },
      axisLabel: {
        textStyle: {
          fontFamily: 'Microsoft YaHei',
          color: '#FFF',
          padding: 2
        },
        fontSize: 14
      }
    },
    yAxis: [
      {
        type: 'value',
        name: '单位:处',
        nameTextStyle: {
          color: '#fff',
          padding: [5, 0, 0, 0]
        },
        axisLine: {
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.5
          }
        },
        splitLine: { // y轴底线
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.5// x轴底线透明度
          }
        },
        axisLabel: {
          show: true,
          textStyle: {
            fontFamily: 'Microsoft YaHei',
            color: '#FFF'
          },
          fontSize: 14
        }
      }
    ],
    dataset: {
      source: props.data
    },
    series: baseData
  }
}
