import * as echarts from 'echarts'
export default function (props) {
  return {
    grid: {
      left: '2%',
      right: '4%',
      bottom: '12%',
      top: '12%',
      containLabel: true
    },
    tooltip: {
      trigger: 'item',
      borderColor: 'none',
      backgroundColor: 'transparent',
      borderWidth: 0,
      padding: 0,
      textStyle: {
        color: 'white'
      },
      formatter: function (params) {
        var str = `<div style = " 
        border:2px solid #02436a;
        box-shadow: 0 0 6px 1px #02436a inset;
        background-color: #1195aa;
        text-align: left;
        padding: 12px 16px;
        >
         <span>
         ${params.marker}
         <span>${params.name}&nbsp;&nbsp;</span>
         <span>${params.value}个</span>
        </span>
        `
        return str
      }
    },
    xAxis: {
      type: 'category',
      data: props.xAxisDtat ? props.xAxisDtat : [],
      axisLine: {
        show: false,
        lineStyle: {
          color: '#B5B5B5'
        }
      },
      offset: 1,
      axisTick: {
        show: false,
        length: 9,
        alignWithLabel: true,
        lineStyle: {
          color: '#7DFFFD'
        }
      },
      axisLabel: {
        textStyle: {
          fontFamily: 'Microsoft YaHei',
          color: '#FFF',
          padding: 10
        },
        fontSize: 14,
        margin: 6, // margin:文字到x轴的距离
        //rotate: 45 // 角度顺时针计算的
      }
    },
    yAxis: {
      type: 'value',
      name: '单位：个',
      nameTextStyle: {
        color: '#fff'
      },
      axisLine: {
        show: true,
        lineStyle: {
          color: ['#00FFFF'],
          type: 'dashed',
          opacity: 0.5
        }
      },
      splitLine: { // y轴底线
        show: true,
        lineStyle: {
          color: ['#00FFFF'],
          type: 'dashed',
          opacity: 0.5// x轴底线透明度
        }
      },
      axisLabel: {
        show: true,
        textStyle: {
          fontFamily: 'Microsoft YaHei',
          color: '#FFF'
        },
        fontSize: 14
      },
      axisTick: {
        show: false
      }
    },
    series: [
      {
        data: props.yAxisDataz ? props.yAxisDataz : [],
        type: 'pictorialBar',
        barMaxWidth: '20',

        symbolPosition: 'end',
        symbol: 'diamond',
        symbolOffset: [0, '-50%'],
        symbolSize: [20, 12],
        zlevel: 2,
        z: 2,
        itemStyle: {
          opacity: 1,
          color: function (params) {
            return new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
              offset: 0,
              color: '#00c5d7' // 0% 处的颜色
            }, {
              offset: 1,
              color: '#00c5d7' // 100% 处的颜色
            }], false)
          }
        }
      },
      {
        data: props.yAxisDataz ? props.yAxisDataz : [],
        type: 'bar',
        barMaxWidth: 'auto',
        barWidth: 20,
        itemStyle: {
          color: {
            x: 0,
            y: 1,
            x2: 0,
            y2: 0,
            type: 'linear',
            colorStops: [
              {
                offset: 0,
                color: '#00D7E9'
              },
              {
                offset: 1,
                color: 'rgba(0, 167, 233,0.3)'
              }
            ]
          }
        },
        label: {
          show: true,
          position: 'top',
          distance: 10,
          color: '#fff'
        }
      },
      {
        data: [1, 1, 1, 1, 1, 1, 1],
        type: 'pictorialBar',
        barMaxWidth: '20',
        symbol: 'diamond',
        symbolOffset: [0, '50%'],
        symbolSize: [20, 10],
        z: 1,
        zlevel: 1,
        itemStyle: {
          opacity: 1,
          color: function (params) {
            return new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
              offset: 0,
              color: '#00ecff' // 0% 处的颜色
            }, {
              offset: 1,
              color: '#00ecff' // 100% 处的颜色
            }], false)
          }
        }
      }
    ]
  }
}
