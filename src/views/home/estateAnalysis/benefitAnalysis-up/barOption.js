import * as echarts from 'echarts'
import {formatInt} from '@/views/common/fn.js'
export default function (props) {
  let arr = [...props.yAxisDataz, ...props.yAxisDatas]
  let vacantArea = Math.max(...arr)
  let barMax = formatInt(vacantArea)
  let vacantFloor = Math.floor(vacantArea)

  let lineArr = [...props.yAxisDatax, ...props.yAxisDatay]
  let unitPrice = Math.max(...lineArr)
  let lineMax = formatInt(unitPrice)

  let len = String(vacantFloor).length
  let num = 100
  for (let i = 0; i <= len.length; i++) {
    num = num * 10
  }
  return {
    tooltip: {
      trigger: 'axis',
      borderColor: 'none',
      backgroundColor: 'transparent',
      borderWidth: 0,
      padding: 0,
      textStyle: {
        color: 'white'
      },
      axisPointer: {
        lineStyle: {
          color: {
            type: 'linear',
            x: 0,
            y: 0,
            x2: 0,
            y2: 1,
            colorStops: [{
              offset: 0,
              color: 'rgba(126,199,255,0)' // 0% 处的颜色
            }, {
              offset: 0.5,
              color: 'rgba(126,199,255,1)' // 100% 处的颜色
            }, {
              offset: 1,
              color: 'rgba(126,199,255,0)' // 100% 处的颜色
            }],
            global: false // 缺省为 false
          }
        }
      },
      formatter: (params) => {
        var str = `<div style = "
        border:2px solid #02436a;
        box-shadow: 0 0 6px 1px #02436a inset;
        background-color: #1195aa;
        text-align: left;
        padding:12px;
        >
        <span style=" display: inline-block; margin-top:40px;">年份：${params[0].axisValue}</span>
        <br/>
        ${params[0].marker}
        <span>${params[0].seriesName}&nbsp;&nbsp;</span>
        <span>${params[0].value}${props.unit1 ? props.unit1 : ''}</span>
        <br/>
        ${params[2].marker}
        <span>${params[2].seriesName}&nbsp;&nbsp;</span>
        <span>${params[2].value}${props.unit1 ? props.unit1 : ''}</span>
        <br/>
        ${params[4].marker}
        <span>${params[4].seriesName}&nbsp;&nbsp;</span>
        <span>${params[4].value}${props.unit2 ? props.unit2 : ''}</span>
        <br/>
        ${params[5].marker}
        <span>${params[5].seriesName}&nbsp;&nbsp;</span>
        <span>${params[5].value}${props.unit2 ? props.unit2 : ''}</span>
        </div>
        `
        return str
      }
    },
    textStyle: {
      color: '#C9C9C9',
      axisPointer: {
        lineStyle: {
          color: {
            type: 'linear',
            x: 0,
            y: 0,
            x2: 0,
            y2: 1,
            colorStops: [{
              offset: 0,
              color: 'rgba(126,199,255,0)' // 0% 处的颜色
            }, {
              offset: 0.5,
              color: 'rgba(126,199,255,1)' // 100% 处的颜色
            }, {
              offset: 1,
              color: 'rgba(126,199,255,0)' // 100% 处的颜色
            }],
            global: false // 缺省为 false
          }
        }
      }
    },
    legend: {
      type: 'plain',
      orient: 'horizontal',
      selectedMode: false, // 图例点击失效
      itemGap: 12,
      itemWidth: 14,
      itemHeight: 14,
      align: 'left',
      right: '2%',
      top: '-1%',
      left: '3%',
      icon: 'circle',
      textStyle: {
        padding: -2,
        color: '#ffffff',
        fontSize: 12
      },
      itemStyle: {
        borderWidth: 0
      },
      data: props.logoName
    },
    grid: {
      left: '4%',
      top: '26%',
      bottom: '4%',
      right: '13%',
      containLabel: true
    },
    xAxis: {
      type: 'category',
      data: props.xAxisDtat,
      axisLine: {
        show: false,
        lineStyle: {
          color: '#B5B5B5'
        }
      },
      axisTick: {
        show: false
      },
      axisLabel: {
        textStyle: {
          fontFamily: 'Microsoft YaHei',
          color: '#FFF',
          padding: 4
        },
        fontSize: 14
        // rotate: 45 // 角度顺时针计算的
      }
    },
    yAxis: [
      {
        type: 'value',
        name: `单位：${props.unit1}`,
        nameTextStyle: {
          color: '#fff',
          fontSize: 14,
          padding: [0, 0, 0, 0]
        },
        min: 0,
        max: barMax,
        interval: (barMax / 5),
        axisLine: {
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.5
          }
        },
        splitLine: { // y轴底线
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.5// x轴底线透明度
          }
        },
        axisLabel: {
          show: true,
          textStyle: {
            fontFamily: 'Microsoft YaHei',
            color: '#FFF'
          },
          fontSize: 14
        }
      },
      {
        type: 'value',
        name: `单位：${props.unit2}`,
        position: 'right',
        show: true,
        nameTextStyle: { // 单位字体颜色
          color: '#fff',
          padding: [0, 0, 0, 5, 0]
        },
        min: 0,
        max: lineMax,
        interval: (lineMax / 5),
        axisLabel: {
          show: true,
          textStyle: {
            fontFamily: 'Microsoft YaHei',
            color: '#FFF'
          },
          fontSize: 14
        },
        axisLine: {
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2
          }
        },
        splitLine: { // y轴底线
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2 // y轴底线透明度
          }
        }
      }
    ],
    series: [
      {
        z: 2,
        name: props.logoName[0],
        type: 'pictorialBar',
        symbolPosition: 'end',
        data: props.yAxisDataz,
        symbol: 'diamond',
        symbolOffset: ['-20%', '-50%'],
        symbolSize: [15, 9],
        itemStyle: {
          borderColor: '#32ffee',
          color: '#32ffee'
        }
      }, {
        z: 2,
        type: 'bar',
        name: props.logoName[0],
        barWidth: 14,
        barGap: '-50%',
        data: props.yAxisDataz,
        itemStyle: {
          color: {
            type: 'linear',
            x: 0,
            x2: 1,
            y: 0,
            y2: 0,
            colorStops: [
              { offset: 0, color: 'rgba(50, 255, 238, .7)' },
              { offset: 0.5, color: 'rgba(50, 255, 238, .7)' },
              { offset: 0.5, color: 'rgba(50, 255, 238, .3)' },
              { offset: 1, color: 'rgba(50, 255, 238, .3)' }
            ]
          }
        }
      },
      {
        z: 3,
        name: props.logoName[1],
        type: 'pictorialBar',
        yAxisIndex: 0,
        symbolPosition: 'end',
        data: props.yAxisDatas,
        symbol: 'diamond',
        symbolOffset: ['26%', '-50%'],
        symbolSize: [15, 9],
        itemStyle: {
          borderColor: '#ffd11a',
          color: '#ffd11a'
        }
      }, {
        z: 3,
        type: 'bar',
        name: props.logoName[1],
        yAxisIndex: 0,
        barWidth: 14,
        data: props.yAxisDatas,
        itemStyle: {
          color: {
            type: 'linear',
            x: 0,
            x2: 1,
            y: 0,
            y2: 0,
            colorStops: [
              { offset: 0, color: 'rgba(255, 209, 26, .7)' },
              { offset: 0.5, color: 'rgba(255, 209, 26, .7)' },
              { offset: 0.5, color: 'rgba(255, 209, 26, .3)' },
              { offset: 1, color: 'rgba(255, 209, 26, .3)' }
            ]
          }
        }
      },
      {
        z: 9,
        yAxisIndex: 1,
        name: props.logoName[2],
        type: 'line',
        stack: 'all',
        symbol: `path://M9.312,4.594 C12.074,4.594 14.313,6.832 14.313,9.594 C14.313,12.355 12.074,14.594 9.312,14.594 C6.551,14.594 4.312,12.355 4.312,9.594 C4.312,6.832 6.551,4.594 9.312,4.594 Z`,
        symbolSize: [10, 10],
        color: {
          type: 'linear',
          x: 1,
          y: 0,
          x2: 0,
          y2: 0,
          // 0% 处的颜色                           // 100% 处的颜色
          colorStops: [{ offset: 0, color: '#32ffee' }, { offset: 1, color: '#8afff5'}],
          global: false // 缺省为 false
        },
        lineStyle: { color: {
          type: 'linear',
          x: 1,
          y: 0,
          x2: 0,
          y2: 0,
          // 0% 处的颜色                           // 100% 处的颜色
          colorStops: [{ offset: 0, color: '#32ffee' }, { offset: 1, color: '#8afff5'}],
          global: false // 缺省为 false
        }},
        // 修改的是线下区域的颜色
        areaStyle: {
          color: new echarts.graphic.LinearGradient(
            // 右/下/左/上
            0, 0, 0, 1, [
              { offset: 0, color: 'rgba(50, 255, 238, .1)' },
              { offset: 1, color: 'transparent' }
            ])
        },
        label: {
          show: false,
          position: 'insideBottomLeft',
          formatter: params => {
            return `${params.value}${props.unit2}`
          },
          textStyle: { fontSize: 16, color: '#32ffee' }
        },
        data: props.yAxisDatax
      }, {
        z: 9,
        yAxisIndex: 1,
        name: props.logoName[3],
        type: 'line',
        stack: 'all',
        symbol: `path://M9.312,4.594 C12.074,4.594 14.313,6.832 14.313,9.594 C14.313,12.355 12.074,14.594 9.312,14.594 C6.551,14.594 4.312,12.355 4.312,9.594 C4.312,6.832 6.551,4.594 9.312,4.594 Z`,
        symbolSize: [10, 10],
        color: {
          type: 'linear',
          x: 1,
          y: 0,
          x2: 0,
          y2: 0,
          // 0% 处的颜色                           // 100% 处的颜色
          colorStops: [{ offset: 0, color: '#ffd11a' }, { offset: 1, color: '#fff5cc'}],
          global: false // 缺省为 false
        },
        lineStyle: { color: {
          type: 'linear',
          x: 1,
          y: 0,
          x2: 0,
          y2: 0,
          // 0% 处的颜色                           // 100% 处的颜色
          colorStops: [{ offset: 0, color: '#ffd11a' }, { offset: 1, color: '#fff5cc'}],
          global: false // 缺省为 false
        }},
        // 修改的是线下区域的颜色
        areaStyle: {
          color: new echarts.graphic.LinearGradient(
            // 右/下/左/上
            0, 0, 0, 1, [
              { offset: 0, color: 'rgba(255, 209, 26, .2)' },
              { offset: 1, color: 'transparent' }
            ])
        },
        label: {
          show: false,
          position: 'insideBottomRight',
          formatter: params => {
            return `${params.value}${props.unit2}`
          },
          textStyle: { fontSize: 16, color: '#ffd11a' }
        },
        data: props.yAxisDatay
      }
    ]
  }
}
