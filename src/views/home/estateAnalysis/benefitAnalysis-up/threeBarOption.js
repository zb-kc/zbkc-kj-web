import * as echarts from 'echarts'
export default function (props) {
  return {
    grid: {
      left: '2%',
      right: '4%',
      bottom: '8%',
      top: '18%',
      containLabel: true
    },
    tooltip: {
      trigger: 'axis',
      borderColor: 'none',
      backgroundColor: 'transparent',
      borderWidth: 0,
      padding: 0,
      textStyle: {
        color: 'white'
      },
      axisPointer: {
        lineStyle: {
          color: {
            type: 'linear',
            x: 0,
            y: 0,
            x2: 0,
            y2: 1,
            colorStops: [{
              offset: 0,
              color: 'rgba(126,199,255,0)' // 0% 处的颜色
            }, {
              offset: 0.5,
              color: 'rgba(126,199,255,1)' // 100% 处的颜色
            }, {
              offset: 1,
              color: 'rgba(126,199,255,0)' // 100% 处的颜色
            }],
            global: false // 缺省为 false
          }
        }
      },
      formatter: function (params) {
        var str = `<div style = " 
                                 border:2px solid #02436a;
                                 box-shadow: 0 0 6px 1px #02436a inset;
                                 background-color: #1195aa;
                                 text-align: left;
                                 padding-left:16px;
                                 padding-top:10px;
                                 >
         <span style=" display: inline-block; margin-top:40px; margin-right:40px;">${params[0].name}</span>
         <br/>
         <span style="display: inline-block; margin-right:16px; margin-bottom: 12px;">
         ${params[0].marker}
         <span>${params[0].seriesName}&nbsp;&nbsp;&nbsp;&nbsp;</span>
         <span>${params[0].value}亿元</span>
        </span>
        `
        return str
      }
    },
    color: ['#00d7e9'],
    legend: {
      icon: 'circle',
      right: '2%',
      top: '0',
      itemWidth: 13,
      itemGap: 20,
      textStyle: {
        fontSize: 14,
        color: '#fff'
      }
    },
    xAxis: {
      type: 'category',
      data: props.xAxisDtat ? props.xAxisDtat : [],
      axisLine: {
        show: false,
        lineStyle: {
          color: '#B5B5B5'
        }
      },
      offset: 1,
      axisTick: {
        show: false,
        length: 9,
        alignWithLabel: true,
        lineStyle: {
          color: '#7DFFFD'
        }
      },
      axisLabel: {
        textStyle: {
          fontFamily: 'Microsoft YaHei',
          color: '#FFF',
          padding: 10
        },
        fontSize: 14,
        margin: 0 // margin:文字到x轴的距离
        // rotate: 45 // 角度顺时针计算的
      }
    },
    yAxis: {
      type: 'value',
      name: '单位：亿元',
      nameTextStyle: {
        color: '#fff'
      },
      axisLine: {
        show: true,
        lineStyle: {
          color: ['#00FFFF'],
          type: 'dashed',
          opacity: 0.5
        }
      },
      splitLine: { // y轴底线
        show: true,
        lineStyle: {
          color: ['#00FFFF'],
          type: 'dashed',
          opacity: 0.5// x轴底线透明度
        }
      },
      axisLabel: {
        show: true,
        textStyle: {
          fontFamily: 'Microsoft YaHei',
          color: '#FFF'
        },
        fontSize: 14
      },
      axisTick: {
        show: false
      }
    },
    series: [
      {
        name: '减少的企业成本',
        data: props.yAxisDataz ? props.yAxisDataz : [],
        type: 'pictorialBar',
        barMaxWidth: '20',
        symbolPosition: 'end',
        symbol: 'diamond',
        symbolOffset: [0, '-50%'],
        symbolSize: [20, 12],
        zlevel: 2,
        z: 2,
        itemStyle: {
          opacity: 1,
          color: function (params) {
            return new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
              offset: 0,
              color: '#00c5d7' // 0% 处的颜色
            }, {
              offset: 1,
              color: '#00c5d7' // 100% 处的颜色
            }], false)
          }
        }
      },
      {
        data: props.yAxisDataz ? props.yAxisDataz : [],
        type: 'bar',
        barMaxWidth: 'auto',
        barWidth: 20,
        itemStyle: {
          color: {
            x: 0,
            y: 1,
            x2: 0,
            y2: 0,
            type: 'linear',
            colorStops: [
              {
                offset: 0,
                color: '#00D7E9'
              },
              {
                offset: 1,
                color: 'rgba(0, 167, 233,0.3)'
              }
            ]
          }
        },
        label: {
          show: true,
          position: 'top',
          distance: 10,
          color: '#fff'
        }
      },
      {
        data: [1, 1, 1],
        type: 'pictorialBar',
        barMaxWidth: '20',
        symbol: 'diamond',
        symbolOffset: [0, '50%'],
        symbolSize: [20, 10],
        z: 1,
        zlevel: 1,
        itemStyle: {
          opacity: 1,
          color: function (params) {
            return new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
              offset: 0,
              color: '#00ecff' // 0% 处的颜色
            }, {
              offset: 1,
              color: '#00ecff' // 100% 处的颜色
            }], false)
          }
        }
      }
    ]
  }
}
