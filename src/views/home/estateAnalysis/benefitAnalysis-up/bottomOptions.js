import * as echarts from 'echarts'
export default function (props) {
  return {
    tooltip: {
      trigger: 'axis',
      borderColor: 'none',
      backgroundColor: 'transparent',
      borderWidth: 0,
      padding: 0,
      textStyle: {
        color: 'white'
      },
      axisPointer: {
        lineStyle: {
          color: {
            type: 'linear',
            x: 0,
            y: 0,
            x2: 0,
            y2: 1,
            colorStops: [{
              offset: 0,
              color: 'rgba(126,199,255,0)' // 0% 处的颜色
            }, {
              offset: 0.5,
              color: 'rgba(126,199,255,1)' // 100% 处的颜色
            }, {
              offset: 1,
              color: 'rgba(126,199,255,0)' // 100% 处的颜色
            }],
            global: false // 缺省为 false
          }
        }
      },
      formatter: (params) => {
        var str = `<div style = " 
        border:2px solid #02436a;
        box-shadow: 0 0 6px 1px #02436a inset;
        background-color: #1195aa;
        text-align: left;
        padding:12px; 
        >
        ${params[0].marker}
        ${params[0].marker}
        <span>${params[0].seriesName}&nbsp;&nbsp;</span>
        <span>${params[0].value}${props.unit ? props.unit : ''}</span>
        <br/>
        ${params[1].marker}
        <span>${params[1].seriesName}&nbsp;&nbsp;</span>
        <span>${params[1].value}${props.unit ? props.unit : ''}</span>
        <br/>
        ${params[2].marker}
        <span>${params[2].seriesName}&nbsp;&nbsp;</span>
        <span>${params[2].value}${props.unit ? props.unit : ''}</span>
        </div>
        `
        return str
      }
    },
    legend: {
      textStyle: {
        color: '#fff',
        fontSize: 14
      },
      selectedMode: false, // 图例点击失效
      itemGap: 25,
      itemWidth: 18,
      icon: 'circle',
      data: props.tipData
    },
    grid: {
      top: '12%',
      left: '2%',
      right: '4%',
      bottom: '2%',
      containLabel: true
    },
    xAxis: [{
      type: 'category',
      boundaryGap: false,
      axisLine: { // 坐标轴轴线相关设置。数学上的x轴
        show: true,
        lineStyle: {
          color: '#233653',
          type: 'dashed',
          opacity: 0.2 // x轴第一根底线透明度
        }
      },
      axisLabel: {
        show: true,
        interval: 0,
        formatter: (name) => { // 图例竖着排列时超过6个字就换行
          if (!name) return ''
          let text = ''
          if (name.length > 4) {
            let count = Math.ceil(name.length / 4) // 向上取整数
            // 一行展示4个
            if (count > 1) {
              for (let z = 1; z <= count; z++) {
                text += name.substr((z - 1) * 4, 4)
                if (z < count) {
                  text += '\n'
                }
              }
            } else {
              text += name.substr(0, 4)
            }
          } else {
            text = name
          }
          return text
        },
        // rotate: 45, // 角度顺时针计算的
        textStyle: {
          color: '#fff', // 坐标值得具体的颜色
          lineHeight: 16 // 图例文字行高
        }
      },
      splitLine: { // y轴底线
        show: false,
        lineStyle: {
          color: ['#00FFFF'],
          type: 'dashed',
          opacity: 0.2 // y轴底线透明度
        }
      },
      axisTick: {
        show: false
      },
      data: props.xLabel
    }],
    yAxis: [
      {
        type: 'value',
        name: '单位：元/㎡·月',
        nameTextStyle: {
          color: '#fff',
          fontSize: 12,
          padding: 0
        },
        min: 0,
        // max: vacant,
        // interval: (vacant / 5),
        splitLine: {
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.5
          }
        },
        axisLine: { // y轴第一根底线配置
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.5
          }
        },
        axisLabel: {
          show: true,
          textStyle: {
            color: '#fff'
            // padding: 16
          },
          formatter: function (value) {
            if (value === 0) {
              return value
            }
            return value
          }
        },
        axisTick: {
          show: false
        }
      }],
    series: [{
      type: 'line',
      name: props.tipData[0].name,
      stack: 'all',
      smooth: true,
      lineStyle: {
        color: '#92c690'
      },
      itemStyle: {
        normal: {
          color: '#92c690',
          borderColor: '#92c690'
        }
      },
      areaStyle: {
        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
          offset: 0,
          color: '#92c690b3'
        }, {
          offset: 1,
          color: '#92c69003'
        }])
      },
      data: props.goToSchool
    }, {
      type: 'line',
      name: props.tipData[1].name,
      stack: 'all',
      smooth: true,
      lineStyle: {
        color: '#219cab'
      },
      itemStyle: {
        normal: {
          color: '#219cab',
          borderColor: '#219cab'
        }
      },
      areaStyle: {
        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
          offset: 0,
          color: '#219cabb3'
        },
        {
          offset: 1,
          color: '#219cab03'
        }
        ])
      },
      data: props.goOutSchool
    }, {
      type: 'line',
      name: props.tipData[2].name,
      stack: 'all',
      smooth: true,
      lineStyle: {
        color: '#6f68bf'
      },
      itemStyle: {
        normal: {
          color: '#6f68bf',
          borderColor: '#6f68bf'
        }
      },
      areaStyle: {
        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
          offset: 0,
          color: '#6f68bfb3'
        }, {
          offset: 1,
          color: '#6f68bf03'
        }])
      },
      data: props.goOutSchool3
    }]
  }
}
