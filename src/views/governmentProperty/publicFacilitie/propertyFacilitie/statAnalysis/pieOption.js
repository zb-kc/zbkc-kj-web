import * as echarts from 'echarts'
export default function (props) {
  var color = props.color || [
    '#1557f4',
    '#13caf6',
    '#febf00',
    '#ff8c19',
    '#fd198a'
  ];
  var title = props.title || '';
  var legend = props.legendData || [];
  var seriesData = props.data || []

  var sum = seriesData.reduce((s, cur) => s + cur.value, 0);//合计
  var getRate = function (val) {
    var rate = 0;//百分比
    if (sum > 0) {
      rate = (val / sum) * 100;
      rate = rate.toFixed(2);
    }
    return rate;
  }

  return {
    color: color,
    title: [{
      left: 25,
      top: 15,
      text: title,
      textStyle: {
        fontSize: 12,
        color: '#DDEEFF',
      },
    },
    {
      text: sum.toFixed(2) + '万㎡',
      top: '46%',
      textAlign: "center",
      left: "28%",
      textStyle: {
        color: '#fff',
        fontSize: 22,
        fontWeight: '400'
      }
    }],
    grid: {
      top: '15%',
      left: 0,
      right: '1%',
      bottom: 5,
      containLabel: true,
    },
    tooltip: {
      show: true, // 是否显示提示框
      trigger: 'item',
      borderColor: 'none',
      backgroundColor: 'transparent',
      borderWidth: 0,
      padding: 0,
      formatter: function (params) {
        //console.log(params, '---params---')
        let str = `<div style = "
        border:1px solid #02fdff;
        background-color: rgba(0,0,0,0.5);
        text-align: left;
        padding:5px 12px;
            >
            ${params.marker}${params.marker}
            <span>${params.name}&nbsp;&nbsp;&nbsp;&nbsp;</span>
            <span style="color:#02fdff;">${getRate(params.value)}%</span>
            </div>`
        return str
      },
      textStyle: {
        color: 'white'
      }
    },
    legend: {
      orient: 'vertical',
      top: 'center',
      right: 20,
      icon: 'react',
      itemWidth: 10,
      itemHeight: 10,
      selectedMode: false,// 图例点击失效
      textStyle: {
        align: 'left',
        verticalAlign: 'middle',
        rich: {
          name: {
            color: 'rgba(255,255,255,0.9)',
            fontSize: 12,
            align: 'left',
            width: 50
          },
          rate: {
            color: 'rgba(255,255,255,0.9)',
            fontSize: 12,
            align: 'right',
            width: 65
          },
        },
      },
      data: legend,
      formatter: (name) => {
        if (seriesData.length) {
          const item = seriesData.filter((item) => item.name === name)[0];
          return `{name|${name}：} {rate| ${getRate(item.value)}%}`;
        }
      },
    },
    series: [{
      name: '需求类型占比',
      type: 'pie',
      center: ['30%', '50%'],
      radius: ['40%', '50%'],
      label: {
        show: false
      },
      data: seriesData,
    },
    {
      type: 'pie',
      name: '外层细圆环',
      center: ['30%', '50%'],
      radius: ['54%', '55%'],
      hoverAnimation: false,
      clockWise: false,
      itemStyle: {
        normal: {
          color: '#4bc6c1',
        },
      },
      label: {
        show: false,
      },
      data: [100],
    },
    {
      type: 'pie',
      name: '内层层细圆环',
      center: ['30%', '50%'],
      radius: ['35%', '36%'],
      hoverAnimation: false,
      clockWise: false,
      itemStyle: {
        normal: {
          color: '#4bc6c1',
        },
      },
      label: {
        show: false,
      },
      data: [100],
    },

    ],
  }
}
