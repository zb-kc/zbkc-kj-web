import * as echarts from 'echarts'
export default function (props) {
  console.log(props, 'props')
  let vacantArea = Math.max(...props.yAxisDataz)
  // let vacant = Math.ceil(vacantArea)
  let vacantFloor = Math.floor(vacantArea)

  // let unitPrice = Math.max(...props.yAxisDatas)
  // let price = Math.ceil(unitPrice)

  let len = String(vacantFloor).length
  let num = 100
  for (let i = 0; i <= len.length; i++) {
    num = num * 10
  }
  let footArr = []
  props.yAxisDataz && Array.isArray(props.yAxisDataz) && props.yAxisDataz.length > 0 && props.yAxisDataz.forEach((item, index) => {
    footArr[index] = (1 / num)
  })
  return {
    tooltip: {
      trigger: 'axis',
      borderColor: 'none',
      backgroundColor: 'transparent',
      borderWidth: 0,
      padding: 0,
      textStyle: {
        color: 'white'
      },
      formatter: function (params) {
        var str = `<div style = " 
        border:1px solid #02fdff;
        background-color: rgba(0,0,0,0.5);
        text-align: left;
        padding:5px 12px;
        >
         <span style=" display: inline-block; margin-top:40px; margin-right:40px;">${params[0].name}</span>
         <br/>
         <span style="display: inline-block; margin-right:16px; margin-bottom: 12px;">
         ${params[0].marker}
         <span>${params[0].seriesName}&nbsp;&nbsp;&nbsp;&nbsp;</span>
         <span style="color:#02fdff;">${params[0].value}㎡</span>
         <br/>
       
         ${params[1].marker}
         <span>${params[1].seriesName}&nbsp;&nbsp;&nbsp;&nbsp;</span>
         <span style="color:#02fdff;">${params[1].value}元/㎡</span>
        </span>
        `
        return str
      }
    },
    color: ['#01ffff', '#fdef00'],
    legend: {
      left: 'center',
      itemWidth: 20,
      itemHeight: 10,
      textStyle: {
        fontSize: 14,
        color: '#fff'
        // lineHeight: 22 // 图例文字行高
      },
      selectedMode: false, // 图例点击失效
      data: props.legendData ? props.legendData : []

    },
    grid: {
      top: '18%',
      left: '3%',
      right: '3%',
      bottom: '9%',
      containLabel: true
    },
    xAxis: [
      {
        type: 'category',
        data: props.xAxisData ? props.xAxisData : [],
        axisPointer: {
          type: 'shadow'
        },
        axisTick: {
          show: false
        },
        offset: 4,
        axisLine: {
          show: true,
          lineStyle: {
            type: 'dashed',
            opacity: 0.2
          }
        },

        axisLabel: {
          show: true,
          interval: 0,
          formatter: (name) => { // 图例竖着排列时超过6个字就换行
            if (!name) return ''
            let text = ''
            if (name.length > 4) {
              let count = Math.ceil(name.length / 4) // 向上取整数
              // 一行展示4个
              if (count > 1) {
                for (let z = 1; z <= count; z++) {
                  text += name.substr((z - 1) * 4, 4)
                  if (z < count) {
                    text += '\n'
                  }
                }
              } else {
                text += name.substr(0, 4)
              }
            } else {
              text = name
            }
            return text
          },
          //rotate: 45, // 角度顺时针计算的
          textStyle: {
            color: '#fff', // 坐标值得具体的颜色
            lineHeight: 16 // 图例文字行高
          }
        }
      }
    ],
    yAxis: [
      {
        type: 'value',
        name: '单位：㎡',
        nameTextStyle: {
          color: '#fff',
          padding: [0, 0, 0, 5, 0]
        },
        min: 0,
        max: 30000, // vacant
        interval: (30000 / 6), // vacant
        axisLabel: {
          formatter: '{value}',
          textStyle: {
            color: '#fff' // 坐标值得具体的颜色
          }
        },
        axisLine: {
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2
          }
        },
        splitLine: { // x轴底线
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2 // x轴底线透明度
          }
        }
      },
      {
        type: 'value',
        name: '单位：元/㎡',
        nameTextStyle: { // 单位字体颜色
          color: '#fff',
          padding: [0, 0, 0, 5, 0]
        },
        min: 0,
        max: 240, // price
        interval: (240 / 6), // price
        axisLabel: {
          formatter: '{value}'
        },
        axisLine: {
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2
          }
        },
        splitLine: { // y轴底线
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2 // y轴底线透明度
          }
        }
      }
    ],
    series: [
      {
        name: props.legendData[0],
        type: 'bar',
        barWidth: 15,
        barGap: '-100%',
        itemStyle: { // lenged文本
          opacity: 0.8,
          // color: function (params) {
          //   return new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
          //     offset: 0,
          //     color: '#106e7a' // 0% 处的颜色
          //   }, {
          //     offset: 1,
          //     color: '#00ffff' // 100% 处的颜色
          //   }], false)
          // }
          color: '#02fffc',
        },
        data: props.yAxisDataz
      },
      {
        name: props.legendData[1],
        type: 'line',
        yAxisIndex: 1,
        symbol: 'circle',
        symbolSize: 6,
        zlevel: 2,
        itemStyle: {
          color: '#fff143',
          borderColor: '#04f004', //  拐点边框颜色
          borderWidth: 1, //  拐点边框宽度
          shadowColor: '#04f004', //  阴影颜色
          shadowBlur: 10, //  阴影渐变范围控制
          emphasis: { // 突出效果配置(鼠标置于拐点上时)
            borderColor: '#04f004', //  拐点边框颜色
            borderWidth: 2, //  拐点边框宽度
            shadowColor: '#04f004', //  阴影颜色
            shadowBlur: 14 //  阴影渐变范围控制
          }
        },
        lineStyle: {
          // type: 'dashed'
          width: 1, // 折线粗线
          color: '#04f004',
          shadowColor: '#04f004',
          shadowBlur: 6,
          zlevel: 2
        },
        data: props.yAxisDatas
      },
      // { // 柱子顶部圆片
      //   name: '',
      //   type: 'pictorialBar',
      //   symbolSize: [14, 10],
      //   symbolOffset: [0, -6],
      //   symbolPosition: 'end',
      //   z: 1,
      //   zlevel: 1,
      //   itemStyle: {
      //     opacity: 1,
      //     color: function (params) {
      //       return new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
      //         offset: 0,
      //         color: '#0ea4aa' // 0% 处的颜色
      //       }, {
      //         offset: 1,
      //         color: '#0ea4aa' // 100% 处的颜色
      //       }], false)
      //     }
      //   },
      //   data: props.yAxisDataz
      // },
      // { // 柱子底部圆片
      //   name: '',
      //   type: 'pictorialBar',
      //   symbolSize: [14, 10],
      //   symbolOffset: [0, -6],
      //   symbolPosition: 'end',
      //   z: 1,
      //   zlevel: 1,
      //   itemStyle: {
      //     opacity: 1,
      //     color: function (params) {
      //       return new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
      //         offset: 0,
      //         color: '#00FFFF' // 0% 处的颜色
      //       }, {
      //         offset: 1,
      //         color: '#00FFFF' // 100% 处的颜色
      //       }], false)
      //     }
      //   },
      //   data: footArr
      // }
    ],
    dataZoom: [
      {
        type: 'slider',
        show: true,
        filterMode: 'none',
        height: 10,
        with: '100%',
        textStyle: {
          color: '#fff'
        },
        // zoomLock: true,
        orient: 'horizontal',
        startValue: 0,
        endValue: 0,
        xAxisIndex: [0],
        showDataShadow: false,
        fillerColor: new echarts.graphic.LinearGradient(1, 0, 0, 0, [{
          // 给颜色设置渐变色 前面4个参数，给第一个设置1，第四个设置0 ，就是水平渐变
          // 给第一个设置0，第四个设置1，就是垂直渐变
          offset: 1,
          color: 'rgba(0, 99, 179, 1)'
        }, {
          offset: 0,
          color: 'rgba(29, 217, 239, 1)'
        }]),
        backgroundColor: 'rgba(0,0,0,0.3)',
        // 拖拽手柄样式 svg 路径
        handleIcon: 'M512 512m-208 0a6.5 6.5 0 1 0 416 0 6.5 6.5 0 1 0-416 0Z M512 192C335.264 192 192 335.264 192 512c0 176.736 143.264 320 320 320s320-143.264 320-320C832 335.264 688.736 192 512 192zM512 800c-159.072 0-288-128.928-288-288 0-159.072 128.928-288 288-288s288 128.928 288 288C800 671.072 671.072 800 512 800z',
        left: 18,
        right: 18,
        bottom: 18,
        start: 0,
        end: props.xAxisData.length > 13 ? 28 : 100
      },
      {
        type: 'inside',
        show: true,
        height: 10,
        start: 0,
        end: props.xAxisData.length > 13 ? 28 : 100
      }
    ]
  }
}
