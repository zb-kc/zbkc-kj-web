import * as echarts from 'echarts'
export default function (props) {
  const CubeLeft = echarts.graphic.extendShape({
    shape: {
      x: 0,
      y: 0
    },
    buildPath: function (ctx, shape) {
      const xAxisPoint = shape.xAxisPoint
      const c0 = [shape.x, shape.y]
      const c1 = [shape.x - 9, shape.y - 9]
      const c2 = [xAxisPoint[0] - 9, xAxisPoint[1] - 9]
      const c3 = [xAxisPoint[0], xAxisPoint[1]]
      ctx.moveTo(c0[0], c0[1]).lineTo(c1[0], c1[1]).lineTo(c2[0], c2[1]).lineTo(c3[0], c3[1]).closePath()
    }
  })
  const CubeRight = echarts.graphic.extendShape({
    shape: {
      x: 0,
      y: 0
    },
    buildPath: function (ctx, shape) {
      const xAxisPoint = shape.xAxisPoint
      const c1 = [shape.x, shape.y]
      const c2 = [xAxisPoint[0], xAxisPoint[1]]
      const c3 = [xAxisPoint[0] + 18, xAxisPoint[1] - 9]
      const c4 = [shape.x + 18, shape.y - 9]
      ctx.moveTo(c1[0], c1[1]).lineTo(c2[0], c2[1]).lineTo(c3[0], c3[1]).lineTo(c4[0], c4[1]).closePath()
    }
  })
  const CubeTop = echarts.graphic.extendShape({
    shape: {
      x: 0,
      y: 0
    },
    buildPath: function (ctx, shape) {
      const c1 = [shape.x, shape.y]
      const c2 = [shape.x + 18, shape.y - 9]
      const c3 = [shape.x + 9, shape.y - 18]
      const c4 = [shape.x - 9, shape.y - 9]
      ctx.moveTo(c1[0], c1[1]).lineTo(c2[0], c2[1]).lineTo(c3[0], c3[1]).lineTo(c4[0], c4[1]).closePath()
    }
  })
  echarts.graphic.registerShape('CubeLeft', CubeLeft)
  echarts.graphic.registerShape('CubeRight', CubeRight)
  echarts.graphic.registerShape('CubeTop', CubeTop)



  return {
    title: {
      text: '',
      top: 32,
      left: 18,
      textStyle: {
        color: '#00F6FF',
        fontSize: 24
      }
    },
    grid: {
      top: '18%',
      left: '3%',
      right: '3%',
      bottom: '9%',
      containLabel: true
    },
    xAxis: {
      type: 'category',
      data: props.xAxisData,
      axisLine: {
        show: true,
        lineStyle: {
          type: 'dashed',
          opacity: 0.2
        }
      },
      axisLabel: {
        show: true,
        interval: 0,
        formatter: (name) => { // 图例竖着排列时超过6个字就换行
          if (!name) return ''
          let text = ''
          if (name.length > 5) {
            let count = Math.ceil(name.length / 5) // 向上取整数
            // 一行展示4个
            if (count > 1) {
              for (let z = 1; z <= count; z++) {
                text += name.substr((z - 1) * 5, 5)
                if (z < count) {
                  text += '\n'
                }
              }
            } else {
              text += name.substr(0, 5)
            }
          } else {
            text = name
          }
          return text
        },
        //rotate: 45, // 角度顺时针计算的
        textStyle: {
          color: '#fff', // 坐标值得具体的颜色
          lineHeight: 16 // 图例文字行高
        }
      },
      offset: 4,
      axisTick: {
        show: false,
      },
    },
    yAxis: [
      {
        type: 'value',
        name: '单位：元/㎡',
        nameTextStyle: {
          color: '#fff',
          padding: [0, 0, 0, 5, 0]
        },
        min: 0,
        max: 120, // vacant
        interval: (120 / 6), // vacant
        axisLabel: {
          formatter: '{value}',
          textStyle: {
            color: '#fff' // 坐标值得具体的颜色
          }
        },
        axisLine: {
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2
          }
        },
        splitLine: { // x轴底线
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2 // x轴底线透明度
          }
        }
      }
    ],
    series: [{
      type: 'custom',
      renderItem: function (params, api) {
        const location = api.coord([api.value(0), api.value(1)])
        return {
          type: 'group',
          children: [{
            type: 'CubeLeft',
            shape: {
              api,
              xValue: api.value(0),
              yValue: api.value(1),
              x: location[0],
              y: location[1],
              xAxisPoint: api.coord([api.value(0), 0])
            },
            style: {
              fill: 'rgba(65,152,227,0.2)'
            }
          }, {
            type: 'CubeRight',
            shape: {
              api,
              xValue: api.value(0),
              yValue: api.value(1),
              x: location[0],
              y: location[1],
              xAxisPoint: api.coord([api.value(0), 0])
            },
            style: {
              fill: 'rgba(65,152,227,0.3)'
            }
          }, {
            type: 'CubeTop',
            shape: {
              api,
              xValue: api.value(0),
              yValue: api.value(1),
              x: location[0],
              y: location[1],
              xAxisPoint: api.coord([api.value(0), 0])
            },
            style: {
              fill: 'rgba(65,152,227,0.25)'
            }
          }]
        }
      },
      data: props.max
    }, {
      type: 'custom',
      renderItem: (params, api) => {
        const location = api.coord([api.value(0), api.value(1)])
        return {
          type: 'group',
          children: [{
            type: 'CubeLeft',
            shape: {
              api,
              xValue: api.value(0),
              yValue: api.value(1),
              x: location[0],
              y: location[1],
              xAxisPoint: api.coord([api.value(0), 0])
            },
            style: {
              fill: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                offset: 0,
                color: '#3B80E2'
              },
              {
                offset: 1,
                color: '#49BEE5'
              }
              ])
            }
          }, {
            type: 'CubeRight',
            shape: {
              api,
              xValue: api.value(0),
              yValue: api.value(1),
              x: location[0],
              y: location[1],
              xAxisPoint: api.coord([api.value(0), 0])
            },
            style: {
              fill: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                offset: 0,
                color: '#3B80E2'
              },
              {
                offset: 1,
                color: '#49BEE5'
              }
              ])
            }
          }, {
            type: 'CubeTop',
            shape: {
              api,
              xValue: api.value(0),
              yValue: api.value(1),
              x: location[0],
              y: location[1],
              xAxisPoint: api.coord([api.value(0), 0])
            },
            style: {
              fill: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                offset: 0,
                color: '#3B80E2'
              },
              {
                offset: 1,
                color: '#49BEE5'
              }
              ])
            }
          }]
        }
      },
      data: props.yAxisDatax
    }, {
      type: 'bar',
      itemStyle: {
        color: 'transparent'
      },
      data: props.max
    }],
    tooltip: {
      trigger: 'axis',
      borderColor: 'none',
      backgroundColor: 'transparent',
      borderWidth: 0,
      padding: 0,
      textStyle: {
        color: 'white'
      },
      axisPointer: {
        lineStyle: {
          color: {
            type: 'linear',
            x: 0,
            y: 0,
            x2: 0,
            y2: 1,
            colorStops: [{
              offset: 0,
              color: 'rgba(126,199,255,0)' // 0% 处的颜色
            }, {
              offset: 0.5,
              color: 'rgba(126,199,255,1)' // 100% 处的颜色
            }, {
              offset: 1,
              color: 'rgba(126,199,255,0)' // 100% 处的颜色
            }],
            global: false // 缺省为 false
          }
        }
      },
      formatter: (params) => {
        console.log(params)
        var str = `<div style = " 
        border:1px solid #02fdff;
        background-color: rgba(0,0,0,0.5);
        text-align: left;
        padding:5px 12px;
        >
        ${params[1].marker}
        <span>${params[1].name}&nbsp;&nbsp;</span>
        <span style="color:#02fdff;">${params[1].value}元/㎡</span>
        </div>
        `
        return str
      }
    },
  }
}
