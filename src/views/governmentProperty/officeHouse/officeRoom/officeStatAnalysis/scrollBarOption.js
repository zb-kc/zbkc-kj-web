import * as echarts from 'echarts'
export default function (props) {
  return {
    tooltip: {
      trigger: 'axis',
      borderColor: 'none',
      backgroundColor: 'transparent',
      borderWidth: 0,
      padding: 0,
      textStyle: {
        color: 'white'
      },
      axisPointer: {
        lineStyle: {
          color: {
            type: 'linear',
            x: 0,
            y: 0,
            x2: 0,
            y2: 1,
            colorStops: [{
              offset: 0,
              color: 'rgba(126,199,255,0)' // 0% 处的颜色
            }, {
              offset: 0.5,
              color: 'rgba(126,199,255,1)' // 100% 处的颜色
            }, {
              offset: 1,
              color: 'rgba(126,199,255,0)' // 100% 处的颜色
            }],
            global: false // 缺省为 false
          }
        }
      },
      formatter: (params) => {
        var str = `<div style = " 
        border:1px solid #02fdff;
        background-color: rgba(0,0,0,0.5);
        text-align: left;
        padding:5px 12px;
        >
        ${params[0].marker}
        <span>${params[0].name}&nbsp;&nbsp;</span>
        <span style="color:#02fdff;">${params[0].value}万㎡</span>
        </div>
        `
        return str
      }
    },
    legend: {
      textStyle: {
        color: '#fff',
        fontSize: 12
      },
      top: '0%',
      selectedMode: false, // 图例点击失效
      itemGap: 25,
      itemWidth: 18,
      icon: 'circle',
      data: props.tipData
    },
    grid: {
      top: '18%',
      right: '3%',
      bottom: '9%',
      left: '3%',
      containLabel: true
    },
    xAxis: [{
      type: 'category',
      // boundaryGap: false,
      axisLine: { // 坐标轴轴线相关设置。数学上的x轴
        show: true,
        lineStyle: {
          color: '#233653',
          type: 'dashed',
          opacity: 0.2 // x轴第一根底线透明度
        }
      },
      axisLabel: {
        show: true,
        interval: 0,
        left: 10, // 左边的距离
        right: 10, // 右边的距离
        bottom: 10, // 右边的距离
        formatter: (name) => { // 图例竖着排列时超过6个字就换行
          if (!name) return ''
          let text = ''
          if (name.length > 4) {
            let count = Math.ceil(name.length / 4) // 向上取整数
            // 一行展示4个
            if (count > 1) {
              for (let z = 1; z <= count; z++) {
                text += name.substr((z - 1) * 4, 4)
                if (z < count) {
                  text += '\n'
                }
              }
            } else {
              text += name.substr(0, 4)
            }
          } else {
            text = name
          }
          return text
        },
        textStyle: {
          color: '#fff', // 坐标值得具体的颜色
          lineHeight: 16 // 图例文字行高
        }
      },
      splitLine: { // y轴底线
        show: false,
        lineStyle: {
          color: ['#00FFFF'],
          type: 'dashed',
          opacity: 0.2 // y轴底线透明度
        }
      },
      axisTick: {
        show: false
      },
      data: props.xAxisData
    }],
    yAxis: [
      {
        name: '单位：万㎡',
        nameTextStyle: {
          color: '#fff',
          fontSize: 12
          // padding: 10
        },
        min: 0,
        // max: 700,
        // interval: 100,
        splitLine: {
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2
          }
        },
        axisLine: { // y轴第一根底线配置
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2
          }
        },
        axisLabel: {
          show: true,
          textStyle: {
            color: '#fff'
            // padding: 16
          },
          formatter: function (value) {
            if (value === 0) {
              return value
            }
            return value
          }
        },
        axisTick: {
          show: false
        }
      }],
    dataZoom: [
      {
        type: 'slider',
        show: true,
        filterMode: 'none',
        height: 10,
        with: '100%',
        textStyle: {
          color: '#fff'
        },
        // zoomLock: true,
        orient: 'horizontal',
        startValue: 0,
        endValue: 0,
        xAxisIndex: [0],
        showDataShadow: false,
        fillerColor: new echarts.graphic.LinearGradient(1, 0, 0, 0, [{
          // 给颜色设置渐变色 前面4个参数，给第一个设置1，第四个设置0 ，就是水平渐变
          // 给第一个设置0，第四个设置1，就是垂直渐变
          offset: 1,
          color: 'rgba(0, 99, 179, 1)'
        }, {
          offset: 0,
          color: 'rgba(29, 217, 239, 1)'
        }]),
        backgroundColor: 'rgba(0,0,0,0.3)',
        // 拖拽手柄样式 svg 路径
        handleIcon: 'M512 512m-208 0a6.5 6.5 0 1 0 416 0 6.5 6.5 0 1 0-416 0Z M512 192C335.264 192 192 335.264 192 512c0 176.736 143.264 320 320 320s320-143.264 320-320C832 335.264 688.736 192 512 192zM512 800c-159.072 0-288-128.928-288-288 0-159.072 128.928-288 288-288s288 128.928 288 288C800 671.072 671.072 800 512 800z',
        left: 18,
        right: 18,
        bottom: 18,
        start: 0,
        end: props.xAxisData.length > 13 ? 28 : 100
      },
      {
        type: 'inside',
        show: true,
        height: 10,
        //start: 0,
        //end: props.yAxisDatax.length > 9 ? 10 : 50
      }
    ],
    series: [{
      type: 'bar',
      data: props.yAxisDatax,
      barWidth: '15px',
      itemStyle: {
        normal: {
          // color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
          //   offset: 0,
          //   color: 'rgba(0,244,255,1)' // 0% 处的颜色
          // }, {
          //   offset: 1,
          //   color: 'rgba(0,77,167,1)' // 100% 处的颜色
          // }], false),
          // barBorderRadius: [30, 30, 30, 30],
          // shadowColor: 'rgba(0,160,221,1)',
          // shadowBlur: 4
          color:'#02fffc'
        }
      }
    }]
  }
}
