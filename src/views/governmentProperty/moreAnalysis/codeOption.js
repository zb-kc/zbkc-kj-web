import * as echarts from 'echarts'
export default function (props) {
  return {
    tooltip: {
      show: true, // 是否显示提示框
      trigger: 'item',
      borderColor: 'none',
      backgroundColor: 'transparent',
      borderWidth: 0,
      padding: 0,
      formatter: function (params) {
        console.log(params, '---params---')
        let str = `<div style = "
                                 border:2px solid #02436a;
                                 box-shadow: 0 0 6px 1px #02436a inset;
                                 background-color: #1195aa;
                                 text-align: left;
                                 padding-left:16px;
                                 padding-right:16px;
                                 padding-top:10px;
                                 padding-bottom:10px;
            >
            <br/>
            <span style="display: inline-block;"></span>
            <span>${params.marker}</span>
            <span>${params.name}&nbsp;&nbsp;&nbsp;&nbsp;</span>
            <span style="display: inline-block;">${params.value}处</span>
            </span>`
        return str
      },
      textStyle: {
        color: 'white'
      }
    },
    grid: {
      top: '15%',
      right: '4%',
      left: '12%',
      bottom: '12%'
    },
    xAxis: [{
      type: 'category',
      data: props.xAxisData,
      axisPointer: {
        type: 'shadow'
      },
      axisTick: {
        show: false
      },
      offset: 4,
      axisLine: {
        show: true,
        lineStyle: {
          type: 'dashed',
          opacity: 0.2
        }
      },

      axisLabel: {
        show: true,
        interval: 0,
        textStyle: {
          color: '#fff', // 坐标值得具体的颜色
          lineHeight: 16 // 图例文字行高
        }
      }
    }],
    yAxis: [{
      type: 'value',
      name: '单位：处',
      nameTextStyle: {
        color: '#fff',
        padding: [0, 0, 0, 5, 0]
      },
      min: 0,
      axisLabel: {
        formatter: '{value}',
        textStyle: {
          color: '#fff' // 坐标值得具体的颜色
        }
      },
      axisLine: {
        show: true,
        lineStyle: {
          color: ['#00FFFF'],
          type: 'dashed',
          opacity: 0.2
        }
      },
      splitLine: { // x轴底线
        show: true,
        lineStyle: {
          color: ['#00FFFF'],
          type: 'dashed',
          opacity: 0.2 // x轴底线透明度
        }
      }
    }],
    series: [{
      type: 'bar',
      data: props.yAxisDatax,
      barWidth: 12,
      itemStyle: {
        normal: {
          color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
            offset: 0,
            color: '#43da55' // 0% 处的颜色
          }, {
            offset: 1,
            color: '#65d9a1' // 100% 处的颜色
          }], false),
          barBorderRadius: [30, 30, 30, 30],
          shadowColor: '#65d9a1',
          shadowBlur: 4
        }
      },
      label: {
        normal: {
          show: true,
          lineHeight: 30,
          width: 80,
          height: 30,
          backgroundColor: 'rgba(0,160,221,0.1)',
          borderRadius: 200,
          position: ['-8', '-60'],
          distance: 1,
          formatter: [
            '    {d|●}',
            ' {a|{c}处}     \n',
            '    {b|}'
          ].join(','),
          rich: {
            d: {
              color: '#43da55'
            },
            a: {
              color: '#fff',
              align: 'center'
            },
            b: {
              width: 1,
              height: 30,
              borderWidth: 1,
              borderColor: '#234e6c',
              align: 'left'
            }
          }
        }
      }
    }]
  }
}
