// import * as echarts from 'echarts'
export default function (props) {
  return {
    tooltip: {
      show: true, // 是否显示提示框
      trigger: 'item',
      borderColor: 'none',
      backgroundColor: 'transparent',
      borderWidth: 0,
      padding: 0,
      formatter: function (params) {
        let str = `<div style = "
                                 border:2px solid #02436a;
                                 box-shadow: 0 0 6px 1px #02436a inset;
                                 background-color: #1195aa;
                                 text-align: left;
                                 padding-left:16px;
                                 padding-right:16px;
                                 padding-top:10px;
                                 padding-bottom:10px;
            >
            <br/>
            <span style="display: inline-block;"></span>
            <span>${props.data[0].name}&nbsp;&nbsp;&nbsp;&nbsp;</span>
            <span style="display: inline-block;">${params.value[0]}处</span>
            <br/>
            <span>${props.data[1].name}&nbsp;&nbsp;&nbsp;&nbsp;</span>
            <span style="display: inline-block;">${params.value[1]}处</span>
            <br/>
            <span>${props.data[2].name}&nbsp;&nbsp;&nbsp;&nbsp;</span>
            <span style="display: inline-block;">${params.value[2]}处</span>
            <br/>
            <span>${props.data[3].name}&nbsp;&nbsp;&nbsp;&nbsp;</span>
            <span style="display: inline-block;">${params.value[3]}处</span>
            <br/>
            <span>${props.data[4].name}&nbsp;&nbsp;&nbsp;&nbsp;</span>
            <span style="display: inline-block;">${params.value[4]}处</span>
            </span>`
        return str
      },
      textStyle: {
        color: 'white'
      }
    },
    radar: {
      radius: '62%', // 大小
      nameGap: 1, // 图中工艺等字距离图的距离
      center: ['50%', '48%'], // 图的位置
      name: {
        textStyle: {
          color: 'rgba(101, 213, 255, 1)',
          fontSize: 14
        },
        formatter: function (name) {
          return name
        }
      },
      indicator: props.data ? props.data : [],
      axisLine: {
        lineStyle: {
          color: 'rgba(153, 209, 246, 0.2)'
        }
      },
      splitArea: {
        show: false,
        areaStyle: {
          color: 'rgba(255,0,0,0)' // 图表背景的颜色
        }
      },
      splitLine: {
        show: true,
        lineStyle: {
          width: 1,
          color: 'rgba(153, 209, 246, 0.3)' // 设置网格的颜色
        }
      }
    },

    series: [
      {
        name: '产业用房扶持企业各规模数量统计',
        type: 'radar',
        symbol: 'angle',
        itemStyle: {
          normal: {
            areaStyle: { type: 'default' }
          }
        },
        data: [
          {
            symbol: 'circle',
            symbolSize: 5,
            value: props.value ? props.value : [],
            areaStyle: { color: 'rgba(64, 205, 241, 0.2)' },
            itemStyle: {
              normal: {
                borderWidth: 1,
                color: 'RGBA(0, 34, 66, 1)',
                borderColor: 'rgba(146, 225, 255, 1)'
              }
            },
            lineStyle: {
              color: 'rgba(146, 225, 255, 1)',
              width: 1
            }
          }
        ]
      }
    ]
  }
}
