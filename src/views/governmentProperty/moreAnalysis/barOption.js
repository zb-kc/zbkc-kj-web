// import * as echarts from 'echarts'
export default function (props) {
  let barWidth = 14
  return {
    tooltip: {
      trigger: 'item',
      borderColor: 'none',
      backgroundColor: 'transparent',
      borderWidth: 0,
      padding: 0,
      textStyle: {
        color: 'white'
      },
      formatter: function (params) {
        var str = `<div style = "
        border:2px solid #02436a;
        box-shadow: 0 0 6px 1px #02436a inset;
        background-color: #1195aa;
        text-align: left;
        padding-left:16px;
        padding-top:10px;
        >
         <span style=" display: inline-block; margin-top:40px; margin-right:40px;">${params.name}</span>
         <br/>
         <span style="display: inline-block; margin-right:16px; margin-bottom: 12px;">
         ${params.marker}
         <span>${params.seriesName}&nbsp;&nbsp;&nbsp;&nbsp;</span>
         <span>${params.value}</span>
        </span>
        `
        return str
      }
    },
    legend: {
      left: 'center',
      top: '78%',
      icon: 'circle',
      padding: 0, // 图例内边距
      itemWidth: 12, // 图例标记的图形宽度。
      itemGap: 10, // 图例每项之间的间隔。
      itemHeight: 12, //  图例标记的图形高度。
      width: '120', // 图例组件的宽度
      height: 'auto', // 图例组件的高度
      textStyle: {
        fontSize: 12,
        color: '#fff',
        padding: 0,
        lineHeight: 12 // 行高
      },
      selectedMode: false, // 图例点击失效
      data: props.legendData ? props.legendData : []
    },
    grid: {
      top: '3%',
      left: '2%',
      right: '6%',
      bottom: '24%',
      containLabel: true
    },
    xAxis: [
      {
        type: 'category',
        axisPointer: {
          type: 'shadow'
        },
        axisTick: {
          show: false
        },
        offset: 4,
        axisLine: {
          show: true,
          lineStyle: {
            type: 'dashed',
            opacity: 0.2
          }
        },

        axisLabel: {
          show: true,
          interval: 0,
          // formatter: (name) => { // 图例竖着排列时超过6个字就换行
          //   if (!name) return ''
          //   let text = ''
          //   if (name.length > 4) {
          //     let count = Math.ceil(name.length / 4) // 向上取整数
          //     // 一行展示4个
          //     if (count > 1) {
          //       for (let z = 1; z <= count; z++) {
          //         text += name.substr((z - 1) * 4, 4)
          //         if (z < count) {
          //           text += '\n'
          //         }
          //       }
          //     } else {
          //       text += name.substr(0, 4)
          //     }
          //   } else {
          //     text = name
          //   }
          //   return text
          // },
          // rotate: 45, // 角度顺时针计算的
          textStyle: {
            color: '#fff', // 坐标值得具体的颜色
            lineHeight: 12, // 图例文字行高
            padding: [6, 0, 8, 0]
          }
        },
        data: props.xAxisData ? props.xAxisData : []
      }

    ],
    yAxis: [
      {
        type: 'value',
        name: '',
        nameTextStyle: {
          color: '#fff',
          padding: [0, 0, 0, 5, 0]
        },
        // min: 0,
        // max: 20, // vacant
        // interval: (20 / 5), // vacant
        axisLabel: {
          formatter: '{value}',
          textStyle: {
            color: '#fff' // 坐标值得具体的颜色
          }
        },
        axisLine: {
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2
          }
        },
        splitLine: { // x轴底线
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2 // x轴底线透明度
          }
        }
      }
    ],
    series: [
      {
        type: 'bar',
        name: props.legendData[0],
        stack: 'all',
        barMaxWidth: 'auto',
        barWidth: barWidth,
        itemStyle: {
          color: {
            x: 0,
            y: 0,
            x2: 0,
            y2: 1,
            type: 'linear',
            global: false,
            colorStops: [
              {
                offset: 0,
                color: '#54f49c'
              },
              {
                offset: 1,
                color: '#03874c'
              }
            ]
          }
        },
        data: props.yAxisDatax
      },
      {
        type: 'bar',
        name: props.legendData[1],
        stack: 'all',
        barMaxWidth: 'auto',
        barWidth: barWidth,
        itemStyle: {
          color: {
            x: 0,
            y: 0,
            x2: 0,
            y2: 1,
            type: 'linear',
            global: false,
            colorStops: [
              {
                offset: 0,
                color: '#06fbfe'
              },
              {
                offset: 1,
                color: '#017ebb'
              }
            ]
          }
        },
        data: props.yAxisDatay
      },
      {
        type: 'bar',
        name: props.legendData[2],
        stack: 'all',
        barMaxWidth: 'auto',
        barWidth: barWidth,
        itemStyle: {
          color: {
            x: 0,
            y: 0,
            x2: 0,
            y2: 1,
            type: 'linear',
            global: false,
            colorStops: [
              {
                offset: 0,
                color: '#32ffee'
              },
              {
                offset: 1,
                color: '#009f8f'
              }
            ]
          }
        },
        data: props.yAxisDataz
      }
    ]
  }
}
