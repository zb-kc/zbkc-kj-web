// import * as echarts from 'echarts'
export default function (props) {
  var arrName = getArrayValue(props.data, 'name')
  var arrValue = getArrayValue(props.data, 'value')
  var sumValue = eval(arrValue.join('+'))
  var objData = array2obj(props.data, 'name')
  var optionData = getData(props.data)

  function getArrayValue (array, id) {
    var key = id || 'value'
    var res = []
    if (array) {
      array.forEach(function (t) {
        res.push(t[key])
      })
    }
    return res
  }

  function array2obj (array, key) {
    var resObj = {}
    for (var i = 0; i < array.length; i++) {
      resObj[array[i][key]] = array[i]
    }
    return resObj
  }

  function getData (data) {
    var res = {
      series: [],
      yAxis: []
    }
    for (let i = 0; i < data.length; i++) {
      res.series.push({
        name: '',
        type: 'pie',
        clockWise: false, // 顺时加载
        hoverAnimation: false, // 鼠标移入变大
        radius: [73 - i * 15 + '%', 68 - i * 15 + '%'],
        center: ['42%', '47%'],
        label: {
          show: false
        },
        itemStyle: {
          label: {
            show: false
          },
          labelLine: {
            show: false
          },
          borderWidth: 5
        },
        data: [{
          value: data[i].value,
          name: data[i].name
        }, {
          value: sumValue - data[i].value,
          name: '',
          itemStyle: {
            color: 'rgba(0,0,0,0)',
            borderWidth: 0
          },
          tooltip: {
            show: false
          },
          hoverAnimation: false
        }]
      })
      res.series.push({
        name: '',
        type: 'pie',
        silent: true,
        z: 1,
        clockWise: false, // 顺时加载
        hoverAnimation: false, // 鼠标移入变大
        radius: [73 - i * 15 + '%', 68 - i * 15 + '%'],
        center: ['42%', '47%'],
        label: {
          show: false
        },
        itemStyle: {
          label: {
            show: false
          },
          labelLine: {
            show: false
          },
          borderWidth: 5
        },
        data: [{
          value: 7.5,
          itemStyle: {
            color: 'rgb(3, 31, 62)',
            borderWidth: 0
          },
          tooltip: {
            show: false
          },
          hoverAnimation: false
        }, {
          value: 2.5,
          name: '',
          itemStyle: {
            color: 'rgba(0,0,0,0)',
            borderWidth: 0
          },
          tooltip: {
            show: false
          },
          hoverAnimation: false
        }]
      })
      res.yAxis.push((data[i].value / sumValue * 100).toFixed(2) + '%')
    }
    return res
  }

  return {
    legend: {
      show: true,
      icon: 'circle',
      top: '9.5%',
      bottom: '50%',
      left: '41%',
      data: arrName,
      width: 40,
      itemWidth: 10,
      itemHeight: 10,
      padding: [0, 14],
      itemGap: 9.5,
      formatter: function (name) {
        return '{title|' + name + '}{value|' + (objData[name].value) + '}  {title|亿元}'
      },

      textStyle: {
        rich: {
          title: {
            fontSize: 14,
            lineHeight: 20,
            color: '#fff'
          },
          value: {
            fontSize: 14,
            lineHeight: 20,
            color: '#00d5ff'
          }
        }
      }
    },
    tooltip: {
      show: true,
      trigger: 'item',
      borderColor: 'none',
      backgroundColor: 'transparent',
      borderWidth: 0,
      padding: 0,
      textStyle: {
        color: 'white'
      },
      formatter: function (params) {
        console.log(params, '--params---')
        let str = `<div style = "
                                 border:2px solid #02436a;
                                 box-shadow: 0 0 6px 1px #02436a inset;
                                 background-color: #1195aa;
                                 text-align: left;
                                 padding-left:16px;
                                 padding-right:16px;
                                 padding-top:10px;
                                 padding-bottom:10px;
            >
            <span>${params.marker}</span>
            <span>${params.marker}</span>
            <span>${params.name}&nbsp;&nbsp;</span>
            <span>${params.value}亿元</span>
            </span>`
        return str
      }
    },
    color: ['#198cff', '#00ffff', '#0dffa1', '#ffc619', '#ff198c'],
    xAxis: [{
      show: false
    }],
    series: optionData.series
  }
}
