import * as echarts from 'echarts'
export default function (props) {
  let arr = [...props.data1, ...props.data2]
  let vacantArea = Math.max(...arr)
  let vacantFloor = Math.floor(vacantArea)
  let len = String(vacantFloor).length
  let num = 100
  for (let i = 0; i <= len.length; i++) {
    num = num * 10
  }
  let footArr = []
  props.data2 && Array.isArray(props.data2) && props.data2.length > 0 && props.data2.forEach((item, index) => {
    footArr[index] = (1 / num)
  })
  var barWidth = 14
  return {
    tooltip: {
      trigger: 'axis',
      borderColor: 'none',
      backgroundColor: 'transparent',
      borderWidth: 0,
      padding: 0,
      textStyle: {
        color: 'white'
      },
      formatter: function (params) {
        let val = 0
        for (let key in props.percentage) {
          if (key === params[5].name) {
            val = props.percentage[key]
          }
        }
        var str = `<div style = " 
        border:2px solid #02436a;
        box-shadow: 0 0 6px 1px #02436a inset;
        background-color: #1195aa;
        text-align: left;
        padding-left:16px;
        padding-top:10px;
                                 >
         <span style=" display: inline-block; margin-top:40px; margin-right:40px;">${params[0].name}</span>
         <br/>
         <span style="display: inline-block; margin-right:16px; margin-bottom: 12px;">
         ${params[0].marker}
         <span>${(params[0].seriesName).slice(0, 3)}&nbsp;&nbsp;</span>
         <span>${params[0].value}万㎡</span>
         <br/>

         ${params[5].marker}
         <span>${(params[5].seriesName).slice(0, 6)}&nbsp;&nbsp;</span>
         <span>${params[2].value}万㎡&nbsp;&nbsp;</span>
         <span>${val}%</span>
        </span>
        `
        return str
      }
    },
    color: ['#01ffff', '#ff9a22'],
    legend: {
      left: 'center',
      icon: 'circle',
      textStyle: {
        fontSize: 14,
        color: '#fff',
        padding: [0, 0, 0, -6]
        // lineHeight: 22 // 图例文字行高
      },
      selectedMode: false, // 图例点击失效
      data: props.legendData ? props.legendData : []
    },
    grid: {
      top: '12%',
      left: '4%',
      right: '4%',
      bottom: '2%',
      containLabel: true
    },
    xAxis: [
      {
        type: 'category',
        data: props.xAxisData ? props.xAxisData : [],
        axisPointer: {
          type: 'shadow'
        },
        axisTick: {
          show: false
        },
        offset: 4,
        axisLine: {
          show: true,
          lineStyle: {
            type: 'dashed',
            opacity: 0.2
          }
        },

        axisLabel: {
          show: true,
          interval: 0,
          // formatter: (name) => { // 图例竖着排列时超过6个字就换行
          //   if (!name) return ''
          //   let text = ''
          //   if (name.length > 4) {
          //     let count = Math.ceil(name.length / 4) // 向上取整数
          //     // 一行展示4个
          //     if (count > 1) {
          //       for (let z = 1; z <= count; z++) {
          //         text += name.substr((z - 1) * 4, 4)
          //         if (z < count) {
          //           text += '\n'
          //         }
          //       }
          //     } else {
          //       text += name.substr(0, 4)
          //     }
          //   } else {
          //     text = name
          //   }
          //   return text
          // },
          rotate: 45, // 角度顺时针计算的
          textStyle: {
            color: '#fff', // 坐标值得具体的颜色
            lineHeight: 16, // 图例文字行高
            padding: 2
          }
        }
      }
    ],
    yAxis: [
      {
        type: 'value',
        nameTextStyle: {
          color: '#fff',
          padding: [0, 0, 0, 5, 0]
        },
        axisLabel: {
          formatter: '{value}',
          textStyle: {
            color: '#fff' // 坐标值得具体的颜色
          }
        },
        axisLine: {
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2
          }
        },
        splitLine: { // x轴底线
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2 // x轴底线透明度
          }
        }
      }
    ],
    series: [{ // 上半截柱子
      name: props.legendData[0],
      type: 'bar',
      barWidth: barWidth,
      barGap: '-100%',
      z: 0,
      itemStyle: {
        color: 'rgba(255, 255, 255, .4)',
        opacity: 0.7
      },
      data: props.data2
    },
    { // 下半截柱子
      name: props.legendData[0],
      type: 'bar',
      barWidth: barWidth,
      barGap: '-100%',
      itemStyle: { // lenged文本
        opacity: 0.7,
        color: function (params) {
          return new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
            offset: 0,
            color: '#00EC28' // 0% 处的颜色
          }, {
            offset: 1,
            color: '#5DF076' // 100% 处的颜色
          }], false)
        }
      },
      data: props.data1
    },
    { // 替代柱状图 默认不显示颜色，是最下方柱图（邮件营销）的value值 - 20
      type: 'bar',
      barWidth: barWidth,
      barGap: '-100%',
      stack: 'all',
      itemStyle: {
        color: 'transparent'
      },
      data: props.data1
    },
    { // 上半截柱子顶部圆片
      name: props.legendData[0],
      type: 'pictorialBar',
      symbolSize: [barWidth, 8],
      symbolOffset: [0, -5],
      z: 12,
      symbolPosition: 'end',
      itemStyle: {
        color: 'rgba(255, 255, 255, .5)',
        opacity: 1
      },
      label: {
        show: true,
        position: 'top',
        fontSize: 12,
        color: '#00d5ff',
        lineHeight: 14,
        // padding: [0, 0, 20, 0],
        formatter: function (item) {
          if ((item.name === '住宅') || (item.name === '商业')) {
            return `${item.value}万㎡\n\n`
          } else if (item.name === '行政办公') {
            return `${item.value}万㎡\n`
          } else {
            return `${item.value}万㎡`
          }
        }
      },
      data: props.data2
    },
    { // 下半截柱子顶部圆片
      name: props.legendData[1],
      type: 'pictorialBar',
      symbolSize: [barWidth, 8],
      symbolOffset: [0, -4],
      z: 12,
      itemStyle: {
        opacity: 1,
        color: function (params) {
          return new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
            offset: 0,
            color: '#00EC28' // 0% 处的颜色
          }, {
            offset: 1,
            color: '#5DF076' // 100% 处的颜色
          }], false)
        }
      },
      label: {
        show: true,
        position: 'top',
        fontSize: 12,
        color: '#00d5ff',
        lineHeight: 14,
        padding: [0, 0, -2, 0],
        formatter: function (item) {
          let val = ''
          for (let key in props.percentage) {
            if (key === item.name) {
              val = `${item.value}万㎡\n${props.percentage[key]}%`
            }
          }
          return val
        }
      },
      symbolPosition: 'end',
      data: props.data1
    },
    { // 下半截柱子底部圆片
      name: props.legendData[1],
      type: 'pictorialBar',
      symbolSize: [barWidth, 8],
      symbolOffset: [0, 5],
      z: 12,
      itemStyle: {
        opacity: 1,
        color: function (params) {
          return new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
            offset: 0,
            color: '#00EC28' // 0% 处的颜色
          }, {
            offset: 1,
            color: '#5DF076' // 100% 处的颜色
          }], false)
        }
      },
      data: footArr
    }
    ]
  }
}
