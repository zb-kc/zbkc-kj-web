import * as echarts from 'echarts'
export default function (props) {
  console.log(props, 'props')
  let vacantArea = Math.max(...props.yAxisDataz)
  // let vacant = Math.ceil(vacantArea)
  let vacantFloor = Math.floor(vacantArea)

  // let unitPrice = Math.max(...props.yAxisDatas)
  // let price = Math.ceil(unitPrice)

  let len = String(vacantFloor).length
  let num = 100
  for (let i = 0; i <= len.length; i++) {
    num = num * 10
  }
  let footArr = []
  props.yAxisDataz && Array.isArray(props.yAxisDataz) && props.yAxisDataz.length > 0 && props.yAxisDataz.forEach((item, index) => {
    footArr[index] = (1 / num)
  })
  return {
    tooltip: {
      trigger: 'axis',
      borderColor: 'none',
      backgroundColor: 'transparent',
      borderWidth: 0,
      padding: 0,
      textStyle: {
        color: 'white'
      },
      formatter: function (params) {
        var str = `<div style = " 
        border:2px solid #02436a;
        box-shadow: 0 0 6px 1px #02436a inset;
        background-color: #1195aa;
        text-align: left;
        padding-left:16px;
        padding-top:10px;
        >
         <span style=" display: inline-block; margin-top:40px; margin-right:40px;">${params[0].name}</span>
         <br/>
         <span style="display: inline-block; margin-right:16px; margin-bottom: 12px;">
         ${params[1].marker}
         <span>${params[1].seriesName}&nbsp;&nbsp;&nbsp;&nbsp;</span>
         <span>${params[1].value}万㎡</span>
         <br/>
       
         ${params[0].marker}
         <span>${params[0].seriesName}&nbsp;&nbsp;&nbsp;&nbsp;</span>
         <span>${params[0].value}%</span>
        </span>
        `
        return str
      }
    },
    color: ['#01ffff', '#fdef00'],
    legend: {
      left: 'center',
      textStyle: {
        fontSize: 14,
        color: '#fff'
        // lineHeight: 22 // 图例文字行高
      },
      selectedMode: false, // 图例点击失效
      data: props.legendData ? props.legendData : []

    },
    grid: {
      top: '18%',
      left: '2%',
      right: '6%',
      bottom: '2%',
      containLabel: true
    },
    xAxis: [
      {
        type: 'category',
        data: props.xAxisData ? props.xAxisData : [],
        axisPointer: {
          type: 'shadow'
        },
        axisTick: {
          show: false
        },
        offset: 4,
        axisLine: {
          show: true,
          lineStyle: {
            type: 'dashed',
            opacity: 0.2
          }
        },

        axisLabel: {
          show: true,
          interval: 0,
          // formatter: (name) => { // 图例竖着排列时超过6个字就换行
          //   if (!name) return ''
          //   let text = ''
          //   if (name.length > 4) {
          //     let count = Math.ceil(name.length / 4) // 向上取整数
          //     // 一行展示4个
          //     if (count > 1) {
          //       for (let z = 1; z <= count; z++) {
          //         text += name.substr((z - 1) * 4, 4)
          //         if (z < count) {
          //           text += '\n'
          //         }
          //       }
          //     } else {
          //       text += name.substr(0, 4)
          //     }
          //   } else {
          //     text = name
          //   }
          //   return text
          // },
          rotate: 45, // 角度顺时针计算的
          textStyle: {
            color: '#fff', // 坐标值得具体的颜色
            lineHeight: 16 // 图例文字行高
          }
        }
      }
    ],
    yAxis: [
      {
        type: 'value',
        name: '单位：万㎡',
        nameTextStyle: {
          color: '#fff',
          padding: [0, 0, 0, 5, 0]
        },
        min: 0,
        max: 60, // vacant
        interval: (60 / 5), // vacant
        axisLabel: {
          formatter: '{value}',
          textStyle: {
            color: '#fff' // 坐标值得具体的颜色
          }
        },
        axisLine: {
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2
          }
        },
        splitLine: { // x轴底线
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2 // x轴底线透明度
          }
        }
      },
      {
        type: 'value',
        name: '单位：%',
        nameTextStyle: { // 单位字体颜色
          color: '#fff',
          padding: [0, 0, 0, 5, 0]
        },
        min: 0,
        max: 60, // price
        interval: (60 / 5), // price
        axisLabel: {
          formatter: '{value}'
        },
        axisLine: {
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2
          }
        },
        splitLine: { // y轴底线
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2 // y轴底线透明度
          }
        }
      }
    ],
    series: [
      {
        name: props.legendData[0],
        type: 'line',
        yAxisIndex: 1,
        symbol: 'circle',
        symbolSize: 8,
        zlevel: 2,
        itemStyle: {
          color: '#FFF100',
          borderColor: '#d0bf00', //  拐点边框颜色
          borderWidth: 1, //  拐点边框宽度
          shadowColor: '#efdf00', //  阴影颜色
          shadowBlur: 10, //  阴影渐变范围控制
          emphasis: { // 突出效果配置(鼠标置于拐点上时)
            borderColor: '#ffff7f', //  拐点边框颜色
            borderWidth: 2, //  拐点边框宽度
            shadowColor: '#efdf00', //  阴影颜色
            shadowBlur: 14 //  阴影渐变范围控制
          }
        },
        lineStyle: {
          // type: 'dashed'
          width: 2, // 折线粗线
          color: '#FFF100',
          shadowColor: '#FFF100',
          shadowBlur: 6,
          zlevel: 2
        },
        data: props.yAxisDatas
      },
      {
        name: props.legendData[1],
        type: 'bar',
        barWidth: 14,
        barGap: '-100%',
        itemStyle: { // lenged文本
          opacity: 0.8,
          color: function (params) {
            return new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
              offset: 0,
              color: '#106e7a' // 0% 处的颜色
            }, {
              offset: 1,
              color: '#00ffff' // 100% 处的颜色
            }], false)
          }
        },
        data: props.yAxisDataz
      },
      { // 柱子顶部圆片
        name: '',
        type: 'pictorialBar',
        symbolSize: [14, 10],
        symbolOffset: [0, -6],
        symbolPosition: 'end',
        z: 1,
        zlevel: 1,
        itemStyle: {
          opacity: 1,
          color: function (params) {
            return new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
              offset: 0,
              color: '#0ea4aa' // 0% 处的颜色
            }, {
              offset: 1,
              color: '#0ea4aa' // 100% 处的颜色
            }], false)
          }
        },
        data: props.yAxisDataz
      },
      { // 柱子底部圆片
        name: '',
        type: 'pictorialBar',
        symbolSize: [14, 10],
        symbolOffset: [0, -6],
        symbolPosition: 'end',
        z: 1,
        zlevel: 1,
        itemStyle: {
          opacity: 1,
          color: function (params) {
            return new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
              offset: 0,
              color: '#00FFFF' // 0% 处的颜色
            }, {
              offset: 1,
              color: '#00FFFF' // 100% 处的颜色
            }], false)
          }
        },
        data: footArr
      }
    ]
  }
}
