import * as echarts from 'echarts'
export default function (props) {
  var maxArr = []
  let len = props.yAxisDataz.length
  for (let j = 0; j < len.length; j++) {
    maxArr[j] = 100
  }
  return {
    tooltip: {
      trigger: 'item',
      borderColor: 'none',
      backgroundColor: 'transparent',
      borderWidth: 0,
      padding: 0,
      textStyle: {
        color: 'white'
      },
      formatter: function (params) {
        console.log(params, 'params')
        var str = `<div style = " 
        border:2px solid #02436a;
        box-shadow: 0 0 6px 1px #02436a inset;
        background-color: #1195aa;
        text-align: left;
        padding: 12px 16px;
                                 >
         <span style="display: inline-block;">
         ${params.marker}
         <span>${params.name}&nbsp;&nbsp;&nbsp;&nbsp;</span>
         <span>${params.value}${props.unit ? props.unit : '度'}</span>
         </span>
        `
        return str
      }
    },
    // color: ['#0a2a3f'],
    // legend: {
    //   left: 'center',
    //   textStyle: {
    //     fontSize: 14,
    //     color: '#fff'
    //     // lineHeight: 22 // 图例文字行高
    //   },
    //   selectedMode: false, // 图例点击失效
    //   data: props.legendData ? props.legendData : []
    // },
    grid: {
      top: '18%',
      left: '6%',
      right: '3%',
      bottom: '4%',
      containLabel: true
    },
    xAxis: [
      {
        type: 'category',
        data: props.xAxisData ? props.xAxisData : [],
        axisPointer: {
          type: 'shadow'
        },
        axisTick: {
          show: false
        },
        offset: 4,
        axisLine: {
          show: true,
          lineStyle: {
            type: 'dashed',
            opacity: 0.2
          }
        },

        axisLabel: {
          show: true,
          interval: 0,
          // formatter: (name) => { // 图例竖着排列时超过6个字就换行
          //   if (!name) return ''
          //   let text = ''
          //   if (name.length > 4) {
          //     let count = Math.ceil(name.length / 4) // 向上取整数
          //     // 一行展示4个
          //     if (count > 1) {
          //       for (let z = 1; z <= count; z++) {
          //         text += name.substr((z - 1) * 4, 4)
          //         if (z < count) {
          //           text += '\n'
          //         }
          //       }
          //     } else {
          //       text += name.substr(0, 4)
          //     }
          //   } else {
          //     text = name
          //   }
          //   return text
          // },
          rotate: 45, // 角度顺时针计算的
          textStyle: {
            padding: 8,
            color: '#fff' // 坐标值得具体的颜色
            // lineHeight: 16 // 图例文字行高
            // padding: [30, 0, 0, 0]
          }
        }
      }
    ],
    yAxis: [
      {
        type: 'value',
        name: `单位：${props.unit ? props.unit : '度'}`,
        nameTextStyle: {
          color: '#fff',
          padding: [0, 0, 0, 5]
        },
        min: 0,
        // max: tenLen,
        // interval: (tenLen / 5),
        axisLabel: {
          formatter: '{value}',
          textStyle: {
            color: '#fff' // 坐标值得具体的颜色
          }
        },
        axisLine: {
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2
          }
        },
        splitLine: { // x轴底线
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2 // x轴底线透明度
          }
        }
      }
    ],
    series: [{
      name: '数量',
      barMinHeight: 10,
      type: 'pictorialBar',
      barCategoryGap: '60%',
      // symbol: 'path://M0,10 L10,10 L5,0 L0,10 z',
      symbol: 'path://M0,10 L10,10 C5.5,10 5.5,5 5,0 C4.5,5 4.5,10 0,10 z',
      itemStyle: {
        normal: {
          // barBorderRadius: 5,
          // 渐变色
          color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
            offset: 0,
            color: '#01EAED'
          },
          {
            offset: 0.5,
            color: '#02C4DD'
          },
          {
            offset: 1,
            color: '#029ED9'
          }
          ])
        }
      },
      // label: {
      //   normal: {
      //     show: true,
      //     position: 'top',
      //     textStyle: {
      //       color: '#fff',
      //       fontSize: 12
      //     }
      //   }
      // },
      data: props.yAxisDataz,
      z: 10
    }
    ]
  }
}
