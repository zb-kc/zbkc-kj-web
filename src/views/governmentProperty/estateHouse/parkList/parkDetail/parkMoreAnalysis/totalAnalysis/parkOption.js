import * as echarts from 'echarts'
// import {formatInt} from '@/views/common/fn'
export default function (props) {
  let vacantArea = Math.max(...props.yAxisDataz)
  // let vacant = Math.ceil(vacantArea)
  let vacantFloor = Math.floor(vacantArea)

  let len = String(vacantFloor).length
  let num = 100
  for (let i = 0; i <= len.length; i++) {
    num = num * 10
  }
  let footArr = []
  props.yAxisDataz && Array.isArray(props.yAxisDataz) && props.yAxisDataz.length > 0 && props.yAxisDataz.forEach((item, index) => {
    footArr[index] = (1 / num)
  })
  // let tenLen = formatInt(vacant, props.numKey)
  return {
    tooltip: {
      trigger: 'item',
      borderColor: 'none',
      backgroundColor: 'transparent',
      borderWidth: 0,
      padding: 0,
      textStyle: {
        color: 'white'
      },
      axisPointer: {
        lineStyle: {
          color: {
            type: 'linear',
            x: 0,
            y: 0,
            x2: 0,
            y2: 1,
            colorStops: [{
              offset: 0,
              color: 'rgba(126,199,255,0)' // 0% 处的颜色
            }, {
              offset: 0.5,
              color: 'rgba(126,199,255,1)' // 100% 处的颜色
            }, {
              offset: 1,
              color: 'rgba(126,199,255,0)' // 100% 处的颜色
            }],
            global: false // 缺省为 false
          }
        }
      },
      formatter: function (params) {
        var str = `<div style = " 
        border:2px solid #02436a;
        box-shadow: 0 0 6px 1px #02436a inset;
        background-color: #1195aa;
        text-align: left;
        padding: 12px 16px;
        >
         <span style="display: inline-block;">
         ${params.marker}
         <span>${params.name}&nbsp;&nbsp;&nbsp;&nbsp;</span>
         <span>${params.value}${props.unit ? props.unit : '度'}</span>
         </span>
        `
        return str
      }
    },
    // color: ['#0a2a3f'],
    // legend: {
    //   left: 'center',
    //   textStyle: {
    //     fontSize: 14,
    //     color: '#fff'
    //     // lineHeight: 22 // 图例文字行高
    //   },
    //   selectedMode: false, // 图例点击失效
    //   data: props.legendData ? props.legendData : []
    // },
    grid: {
      top: '18%',
      left: '6%',
      right: '3%',
      bottom: '4%',
      containLabel: true
    },
    xAxis: [
      {
        type: 'category',
        data: props.xAxisData ? props.xAxisData : [],
        axisPointer: {
          type: 'shadow'
        },
        axisTick: {
          show: false
        },
        offset: 4,
        axisLine: {
          show: true,
          lineStyle: {
            type: 'dashed',
            opacity: 0.2
          }
        },

        axisLabel: {
          show: true,
          interval: 0,
          // formatter: (name) => { // 图例竖着排列时超过6个字就换行
          //   if (!name) return ''
          //   let text = ''
          //   if (name.length > 4) {
          //     let count = Math.ceil(name.length / 4) // 向上取整数
          //     // 一行展示4个
          //     if (count > 1) {
          //       for (let z = 1; z <= count; z++) {
          //         text += name.substr((z - 1) * 4, 4)
          //         if (z < count) {
          //           text += '\n'
          //         }
          //       }
          //     } else {
          //       text += name.substr(0, 4)
          //     }
          //   } else {
          //     text = name
          //   }
          //   return text
          // },
          rotate: 45, // 角度顺时针计算的
          textStyle: {
            padding: 8,
            color: '#fff' // 坐标值得具体的颜色
            // lineHeight: 16 // 图例文字行高
            // padding: [30, 0, 0, 0]
          }
        }
      }
    ],
    yAxis: [
      {
        type: 'value',
        name: `单位：${props.unit ? props.unit : '度'}`,
        nameTextStyle: {
          color: '#fff',
          padding: [0, 0, 0, 5]
        },
        min: 0,
        // max: tenLen,
        // interval: (tenLen / 5),
        axisLabel: {
          formatter: '{value}',
          textStyle: {
            color: '#fff' // 坐标值得具体的颜色
          }
        },
        axisLine: {
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2
          }
        },
        splitLine: { // x轴底线
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2 // x轴底线透明度
          }
        }
      }
    ],
    series: [
      {
        // name: props.legendData[0],
        type: 'bar',
        barWidth: 30,
        barGap: '-100%',
        itemStyle: { // lenged文本
          opacity: 0.8,
          color: function (params) {
            return new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
              offset: 0,
              color: '#0a2a3f' // 0% 处的颜色
            }, {
              offset: 1,
              color: '#00ffff' // 100% 处的颜色
            }], false)
          }
        },
        data: props.yAxisDataz
      },

      { // 柱子顶部闪烁圆片
        // name: '',
        type: 'effectScatter',
        rippleEffect: {
          period: 1,
          scale: 5,
          brushType: 'fill'
        },
        z: 1,
        zlevel: 1,
        symbolPosition: 'end',
        symbolSize: [7, 3],
        symbolOffset: [0, 0],
        itemStyle: {
          normal: {
            shadowColor: 'rgba(0, 0, 0, 0.3)',
            shadowBlur: 5,
            shadowOffsetY: 3,
            shadowOffsetX: 0,
            color: 'rgba(100,255,255,1)'
          }
        },
        data: props.yAxisDataz
      },
      { // 柱子底部圆片
        name: '',
        type: 'pictorialBar',
        symbolSize: [30, 15],
        symbolOffset: [0, -6],
        symbolPosition: 'end',
        z: 1,
        zlevel: 1,
        itemStyle: {
          opacity: 1,
          color: function (params) {
            return new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
              offset: 0,
              color: '#00FFFF' // 0% 处的颜色
            }, {
              offset: 1,
              color: '#00FFFF' // 100% 处的颜色
            }], false)
          }
        },
        data: footArr
      }
    ]
  }
}
