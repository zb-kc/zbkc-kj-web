import * as echarts from 'echarts'
import {formatInt} from '@/views/common/fn.js'
export default function (props) {
  let maxArr = [...props.yAxisDatas, ...props.yAxisDataz]
  let vacantArea = Math.max(...maxArr)
  let vacant = formatInt(vacantArea, String(vacantArea).length - 1) > 10 ? formatInt(vacantArea, String(vacantArea).length - 1) : 10

  let vacantFloor = Math.floor(vacantArea)
  let len = String(vacantFloor).length
  let num = 100
  for (let i = 0; i <= len.length; i++) {
    num = num * 10
  }
  let footArr = []
  props.yAxisDataz && Array.isArray(props.yAxisDataz) && props.yAxisDataz.length > 0 && props.yAxisDataz.forEach((item, index) => {
    footArr[index] = (1 / num)
  })

  return {
    tooltip: {
      trigger: 'axis',
      borderColor: 'none',
      backgroundColor: 'transparent',
      borderWidth: 0,
      padding: 0,
      textStyle: {
        color: 'white'
      },
      formatter: function (params) {
        console.log(params, 'params')
        var str = `<div style = " 
        border:2px solid #02436a;
        box-shadow: 0 0 6px 1px #02436a inset;
        background-color: #1195aa;
        text-align: left;
        padding: 12px 16px;
        >
         <span style=" display: inline-block;">${params[0].name}</span>
         <br/>
         <span style="display: inline-block;">
         ${params[0].marker}
         <span>${params[0].seriesName}&nbsp;&nbsp;&nbsp;&nbsp;</span>
         <span>${params[0].value}%</span>
         <br/>
         ${params[1].marker}
         <span>${params[1].seriesName}&nbsp;&nbsp;&nbsp;&nbsp;</span>
         <span>${params[1].value}家</span>
         <br/>
         ${params[4].marker}
         <span>${params[4].seriesName}&nbsp;&nbsp;&nbsp;&nbsp;</span>
         <span>${params[4].value}家</span>
        </span>
        `
        return str
      }
    },
    color: ['#00ffff', '#87ffb7'],
    legend: {
      left: 'center',
      textStyle: {
        fontSize: 14,
        color: '#fff'
        // lineHeight: 22 // 图例文字行高
      },
      selectedMode: false, // 图例点击失效
      data: props.legendData ? props.legendData : []

    },
    grid: {
      top: '20%',
      left: '3%',
      right: '3%',
      bottom: '5%',
      containLabel: true
    },
    xAxis: [
      {
        type: 'category',
        data: props.xAxisData ? props.xAxisData : [],
        axisPointer: {
          type: 'shadow'
        },
        axisTick: {
          show: false
        },
        offset: 4,
        axisLine: {
          show: true,
          lineStyle: {
            type: 'dashed',
            opacity: 0.2
          }
        },

        axisLabel: {
          show: true,
          interval: 0,
          // formatter: (name) => { // 图例竖着排列时超过6个字就换行
          //   if (!name) return ''
          //   let text = ''
          //   if (name.length > 4) {
          //     let count = Math.ceil(name.length / 4) // 向上取整数
          //     // 一行展示4个
          //     if (count > 1) {
          //       for (let z = 1; z <= count; z++) {
          //         text += name.substr((z - 1) * 4, 4)
          //         if (z < count) {
          //           text += '\n'
          //         }
          //       }
          //     } else {
          //       text += name.substr(0, 4)
          //     }
          //   } else {
          //     text = name
          //   }
          //   return text
          // },
          // rotate: 45, // 角度顺时针计算的
          textStyle: {
            color: '#fff', // 坐标值得具体的颜色
            lineHeight: 16 // 图例文字行高
          }
        }
      }
    ],
    yAxis: [
      {
        type: 'value',
        name: '单位:家',
        nameTextStyle: {
          color: '#fff',
          padding: [0, 0, 0, 5, 0]
        },
        min: 0,
        max: vacant,
        interval: (vacant / 5),
        axisLabel: {
          formatter: '{value}',
          textStyle: {
            color: '#fff' // 坐标值得具体的颜色
          }
        },
        axisLine: {
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2
          }
        },
        splitLine: { // x轴底线
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2 // x轴底线透明度
          }
        }
      },
      {
        type: 'value',
        name: '单位：%',
        nameTextStyle: { // 单位字体颜色
          color: '#fff',
          padding: [0, 0, 0, 5, 0]
        },
        min: 0,
        max: 100,
        interval: (100 / 5),
        axisLabel: {
          formatter: '{value}'
        },
        axisLine: {
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2
          }
        },
        splitLine: { // y轴底线
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2 // y轴底线透明度
          }
        }
      }
    ],
    series: [
      {
        name: props.legendData[2],
        type: 'line',
        yAxisIndex: 1,
        symbol: 'circle',
        symbolSize: 8,
        zlevel: 2,
        itemStyle: {
          color: '#FFF100',
          borderColor: '#d0bf00', //  拐点边框颜色
          borderWidth: 1, //  拐点边框宽度
          shadowColor: '#efdf00', //  阴影颜色
          shadowBlur: 10, //  阴影渐变范围控制
          emphasis: { // 突出效果配置(鼠标置于拐点上时)
            borderColor: '#ffff7f', //  拐点边框颜色
            borderWidth: 2, //  拐点边框宽度
            shadowColor: '#efdf00', //  阴影颜色
            shadowBlur: 14 //  阴影渐变范围控制
          }
        },
        lineStyle: {
          // type: 'dashed'
          width: 2, // 折线粗线
          color: '#FFF100',
          shadowColor: '#FFF100',
          shadowBlur: 6,
          zlevel: 2
        },
        data: props.yAxisData
      },
      // 第一根柱子
      {
        name: props.legendData[0],
        type: 'bar',
        barWidth: 12,
        barGap: '-100%',
        itemStyle: { // lenged文本
          opacity: 0.8,
          color: function (params) {
            return new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
              offset: 0,
              color: '#00ffff' // 0% 处的颜色
            }, {
              offset: 1,
              color: '#00ffff' // 100% 处的颜色
            }], false)
          }
        },
        data: props.yAxisDatas
      },
      { // 柱子顶部圆片
        name: '',
        type: 'pictorialBar',
        symbolSize: [12, 7],
        symbolOffset: [-6, -2],
        symbolPosition: 'end',
        z: 1,
        zlevel: 1,
        itemStyle: {
          opacity: 1,
          color: function (params) {
            return new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
              offset: 0,
              color: '#00FFFF' // 0% 处的颜色
            }, {
              offset: 1,
              color: '#00FFFF' // 100% 处的颜色
            }], false)
          }
        },
        data: props.yAxisDatas
      },
      { // 柱子底部圆片
        name: '',
        type: 'pictorialBar',
        symbolSize: [12, 7],
        symbolOffset: [-6, -2],
        symbolPosition: 'end',
        z: 1,
        zlevel: 1,
        itemStyle: {
          opacity: 1,
          color: function (params) {
            return new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
              offset: 0,
              color: '#00FFFF' // 0% 处的颜色
            }, {
              offset: 1,
              color: '#00FFFF' // 100% 处的颜色
            }], false)
          }
        },
        data: footArr
      },
      // 第二根柱子
      {
        name: props.legendData[1],
        type: 'bar',
        barWidth: 12,
        barGap: '0',
        itemStyle: { // lenged文本
          opacity: 0.8,
          color: function (params) {
            return new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
              offset: 0,
              color: '#87ffb7' // 0% 处的颜色
            }, {
              offset: 1,
              color: '#87ffb7' // 100% 处的颜色
            }], false)
          }
        },
        data: props.yAxisDataz
      },
      { // 柱子顶部圆片
        name: '',
        type: 'pictorialBar',
        symbolSize: [12, 7],
        symbolOffset: [6, -2],
        symbolPosition: 'end',
        z: 1,
        zlevel: 1,
        itemStyle: {
          opacity: 1,
          color: function (params) {
            return new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
              offset: 0,
              color: '#64ffa8' // 0% 处的颜色
            }, {
              offset: 1,
              color: '#64ffa8' // 100% 处的颜色
            }], false)
          }
        },
        data: props.yAxisDataz
      },
      { // 柱子底部圆片
        name: '',
        type: 'pictorialBar',
        symbolSize: [12, 7],
        symbolOffset: [6, -2],
        symbolPosition: 'end',
        z: 1,
        zlevel: 1,
        itemStyle: {
          opacity: 1,
          color: function (params) {
            return new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
              offset: 0,
              color: '#64ffa8' // 0% 处的颜色
            }, {
              offset: 1,
              color: '#64ffa8' // 100% 处的颜色
            }], false)
          }
        },
        data: footArr
      }
    ]
  }
}
