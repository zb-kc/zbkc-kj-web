import * as echarts from 'echarts'
export default function (props) {
  console.log(props, 'props')
  return {
    tooltip: {
      trigger: 'axis',
      borderColor: 'none',
      backgroundColor: 'transparent',
      borderWidth: 0,
      padding: 0,
      textStyle: {
        color: 'white'
      },
      axisPointer: {
        lineStyle: {
          color: {
            type: 'linear',
            x: 0,
            y: 0,
            x2: 0,
            y2: 1,
            colorStops: [{
              offset: 0,
              color: 'rgba(126,199,255,0)' // 0% 处的颜色
            }, {
              offset: 0.5,
              color: 'rgba(126,199,255,1)' // 100% 处的颜色
            }, {
              offset: 1,
              color: 'rgba(126,199,255,0)' // 100% 处的颜色
            }],
            global: false // 缺省为 false
          }
        }
      },
      formatter: (params) => {
        var str = `<div style = " 
        background:url( ${require('../../../img/tipsbg.png')}) no-repeat center center;
        background-size:100% 100%;
        text-align: left;
        padding:12px; 
        >
        ${params[0].marker}
        ${params[0].marker}
        <span>${params[0].seriesName}&nbsp;&nbsp;</span>
        <span>${params[0].value}${props.unit ? props.unit : ''}</span>
        <br/>
        ${params[1].marker}
        <span>${params[1].seriesName}&nbsp;&nbsp;</span>
        <span>${params[1].value}${props.unit ? props.unit : ''}</span>
        </div>
        `
        return str
      }
    },
    legend: {
      textStyle: {
        color: '#fff',
        fontSize: 14
      },
      top: '1%',
      right: '11%',
      selectedMode: false, // 图例点击失效
      itemGap: 20,
      itemWidth: 13,
      itemHeight: 13,
      icon: 'circle',
      data: props.tipData
    },
    grid: {
      left: '6%',
      top: '12%',
      bottom: '19%',
      right: '12%',
      containLabel: true
    },
    xAxis: [{
      type: 'category',
      boundaryGap: false,
      axisLine: { // 坐标轴轴线相关设置。数学上的x轴
        show: true,
        lineStyle: {
          color: '#233653',
          type: 'dashed',
          opacity: 0.2 // x轴第一根底线透明度
        }
      },
      axisLabel: { // 坐标轴刻度标签的相关设置
        textStyle: {
          color: '#fff',
          padding: 8,
          fontSize: 14

        }
        // rotate: 45 // 角度顺时针计算的
      },
      splitLine: { // y轴底线
        show: false,
        lineStyle: {
          color: ['#00FFFF'],
          type: 'dashed',
          opacity: 0.2 // y轴底线透明度
        }
      },
      axisTick: {
        show: false
      },
      data: props.xLabel
    }],
    yAxis: [
      {
        name: '单位：家',
        nameTextStyle: {
          color: '#fff',
          fontSize: 14,
          padding: [0, 0, 5, 0]
        },
        min: 0,
        // max: 700,
        // interval: 100,
        splitLine: {
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.5
          }
        },
        axisLine: { // y轴第一根底线配置
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.5
          }
        },
        axisLabel: {
          show: true,
          textStyle: {
            color: '#fff'
          },
          formatter: function (value) {
            if (value === 0) {
              return value
            }
            return value
          }
        },
        axisTick: {
          show: false
        }
      }],
    series: [{
      name: '政府物业',
      type: 'line',
      symbol: 'circle', // 默认是空心圆（中间是白色的），改成实心圆
      showAllSymbol: true,
      symbolSize: 6, // 折线园点大小
      smooth: true,
      lineStyle: {
        normal: {
          width: 2,
          color: 'rgba(25,163,223,1)' // 线条颜色
        },
        borderColor: 'rgba(0,0,0,.4)'
      },
      itemStyle: {
        color: 'rgba(25,163,223,1)'// 折线园点颜色
        // borderColor: '#646ace'
        // borderWidth: 2
      },
      tooltip: {
        show: true
      },
      areaStyle: { // 区域填充样式
        normal: {
          // 线性渐变，前4个参数分别是x0,y0,x2,y2(范围0~1);相当于图形包围盒中的百分比。如果最后一个参数是‘true’，则该四个值是绝对像素位置。
          color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
            offset: 0,
            color: 'rgba(25,163,223,.3)'

          },
          {
            offset: 1,
            color: 'rgba(25,163,223, 0)'
          }
          ], false),
          shadowColor: 'rgba(25,163,223, 0.5)', // 阴影颜色
          shadowBlur: 20 // shadowBlur设图形阴影的模糊大小。配合shadowColor,shadowOffsetX/Y, 设置图形的阴影效果。
        }
      },
      data: props.goToSchool
    }, {
      name: '国企物业',
      type: 'line',
      symbol: 'circle', // 默认是空心圆（中间是白色的），改成实心圆
      showAllSymbol: true,
      symbolSize: 6, // 折线园点大小
      smooth: true,
      lineStyle: {
        normal: {
          width: 2,
          color: 'rgba(10,219,250,1)' // 线条颜色
        },
        borderColor: 'rgba(0,0,0,.4)'
      },
      itemStyle: {
        color: 'rgba(10,219,250,1)'// 折线园点颜色
        // borderColor: '#646ace',
        // borderWidth: 2
      },
      tooltip: {
        show: true
      },
      areaStyle: { // 区域填充样式
        normal: {
          // 线性渐变，前4个参数分别是x0,y0,x2,y2(范围0~1);相当于图形包围盒中的百分比。如果最后一个参数是‘true’，则该四个值是绝对像素位置。
          color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
            offset: 0,
            color: 'rgba(10,219,250,.3)'
          },
          {
            offset: 1,
            color: 'rgba(10,219,250, 0)'
          }
          ], false),
          shadowColor: 'rgba(10,219,250, 0.5)', // 阴影颜色
          shadowBlur: 20 // shadowBlur设图形阴影的模糊大小。配合shadowColor,shadowOffsetX/Y, 设置图形的阴影效果。
        }
      },
      data: props.goOutSchool
    }]
  }
}
