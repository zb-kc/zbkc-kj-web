import * as echarts from 'echarts'
export default function (props) {
  const colorList = ['#9E87FF', '#73DDFF', '#fe9a8b', '#F56948', '#9E87FF']
  return {
    tooltip: {
      trigger: 'axis',
      borderColor: 'none',
      backgroundColor: 'transparent',
      borderWidth: 0,
      padding: 0,
      textStyle: {
        color: 'white'
      },
      axisPointer: {
        lineStyle: {
          color: {
            type: 'linear',
            x: 0,
            y: 0,
            x2: 0,
            y2: 1,
            colorStops: [{
              offset: 0,
              color: 'rgba(126,199,255,0)' // 0% 处的颜色
            }, {
              offset: 0.5,
              color: 'rgba(126,199,255,1)' // 100% 处的颜色
            }, {
              offset: 1,
              color: 'rgba(126,199,255,0)' // 100% 处的颜色
            }],
            global: false // 缺省为 false
          }
        }
      },
      formatter: function (params) {
        console.log(params, 'params')
        var str = `<div style = " 
                                 border:2px solid #02436a;
                                 box-shadow: 0 0 6px 1px #02436a inset;
                                 background-color: #1195aa;
                                 text-align: left;
                                 padding-left:16px;
                                 padding-top:10px;
                                 >
         <span style=" display: inline-block; margin-top:40px; margin-right:40px;">${params[0].name}</span>
         <br/>
         <span style="display: inline-block; margin-right:16px; margin-bottom: 12px;">
         ${params[0].marker}
         <span>${params[0].seriesName}&nbsp;&nbsp;&nbsp;&nbsp;</span>
         <span>${params[0].value}人</span>
        </span>
        `
        return str
      }
    },
    legend: {
      icon: 'circle',
      right: '2%',
      top: '0',
      itemWidth: 13,
      itemGap: 20,
      textStyle: {
        fontSize: 14,
        color: '#fff'
        // lineHeight: 22 // 图例文字行高
      }
    },
    grid: {
      left: '4%',
      top: '18%',
      bottom: '2%',
      right: '10%',
      containLabel: true
    },
    xAxis: [{
      type: 'category',
      data: props.xAxisDtat,
      axisLine: { // 坐标轴轴线相关设置。数学上的x轴
        show: true,
        lineStyle: {
          color: '#233653',
          type: 'dashed',
          opacity: 0.2 // x轴第一根底线透明度
        }
      },
      axisTick: {
        show: false
      },
      axisLabel: {
        interval: 0,
        textStyle: {
          color: '#fff',
          fontSize: 14
        },
        fontSize: 12, // 默认x轴字体大小
        margin: 14 // margin:文字到x轴的距离
        // rotate: 45 // 角度顺时针计算的
      },
      boundaryGap: false
    }],
    yAxis: [
      {
        type: 'value',
        name: '单位:人',
        scale: true,
        nameTextStyle: {
          color: '#fff',
          padding: [0, 0, 5, 0]
        },
        // min: 75,
        // max: vacant,
        // minInterval: 1, // y轴刻度取整数
        // interval: 0.5,
        // axisLabel: {
        //   formatter: function (value, index) {
        //     return value.toFixed(1) //y轴刻度保留1位小数
        //   }
        // },
        axisTick: {
          show: false
        },
        axisLine: { // y轴第一根底线配置
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.5
          }
        },
        splitLine: {
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.5
          }
        }
      }
    ],
    series: [
      {
        name: '员工数量',
        type: 'line',
        data: props.yAxisDataz,
        symbolSize: 1,
        symbol: 'circle',
        smooth: true,
        yAxisIndex: 0,
        showSymbol: false,
        lineStyle: {
          width: 3,
          color: new echarts.graphic.LinearGradient(0, 1, 0, 0, [{
            offset: 0,
            color: '#9effff'
          },
          {
            offset: 1,
            color: '#9E87FF'
          }
          ]),
          shadowColor: 'rgba(158,135,255, 1)',
          shadowBlur: 8,
          shadowOffsetY: 5
        },
        itemStyle: {
          normal: {
            color: colorList[0],
            borderColor: colorList[0]
          }
        }
      }
    ]
  }
}
