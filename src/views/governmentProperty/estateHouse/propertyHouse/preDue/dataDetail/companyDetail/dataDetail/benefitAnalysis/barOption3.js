import * as echarts from 'echarts'
import {formatInt} from '@/views/common/fn.js'
export default function (props) {
  let vacantAreak = Math.max(...props.yAxisDatak).toFixed()
  let vacantAreaj = Math.max(...props.yAxisDataj).toFixed()

  let barMaxs = vacantAreak > 10 ? formatInt(vacantAreak, vacantAreak.length - 1) : 10
  let barMaxx = vacantAreaj > 10 ? formatInt(vacantAreaj, vacantAreaj.length - 1) : 10

  return {
    tooltip: {
      trigger: 'axis',
      borderColor: 'none',
      backgroundColor: 'transparent',
      borderWidth: 0,
      padding: 0,
      textStyle: {
        color: 'white'
      },
      axisPointer: {
        lineStyle: {
          color: {
            type: 'linear',
            x: 0,
            y: 0,
            x2: 0,
            y2: 1,
            colorStops: [{
              offset: 0,
              color: 'rgba(126,199,255,0)' // 0% 处的颜色
            }, {
              offset: 0.5,
              color: 'rgba(126,199,255,1)' // 100% 处的颜色
            }, {
              offset: 1,
              color: 'rgba(126,199,255,0)' // 100% 处的颜色
            }],
            global: false // 缺省为 false
          }
        }
      },
      formatter: (params) => {
        var str = `<div style = "
        border:2px solid #02436a;
        box-shadow: 0 0 6px 1px #02436a inset;
        background-color: #1195aa;
        text-align: left;
        padding:12px;
        >
        <span style=" display: inline-block; margin-top:40px;">年份：${params[0].axisValue}</span>
        <br/>
        ${params[0].marker}
        <span>${params[0].seriesName}&nbsp;&nbsp;</span>
        <span>${params[0].value}${props.unit1 ? props.unit1 : ''}</span>
        <br/>
        ${params[1].marker}
        <span>${params[1].seriesName}&nbsp;&nbsp;</span>
        <span>${params[1].value}${props.unit2 ? props.unit2 : ''}</span>
        </div>
        `
        return str
      }
    },
    legend: {
      type: 'plain',
      orient: 'horizontal',
      selectedMode: false, // 图例点击失效
      itemGap: 12,
      itemWidth: 14,
      itemHeight: 14,
      align: 'left',
      right: '2%',
      top: '1%',
      left: '6%',
      icon: 'circle',
      textStyle: {
        padding: -2,
        color: '#ffffff',
        fontSize: 12
      },
      itemStyle: {
        borderWidth: 0
      },
      data: props.logoName
    },
    grid: {
      left: '4%',
      top: '26%',
      bottom: '4%',
      right: '8%',
      containLabel: true
    },
    xAxis: {
      type: 'category',
      data: props.xAxisDtat,
      axisLine: {
        show: false,
        lineStyle: {
          color: '#B5B5B5'
        }
      },
      axisTick: {
        show: false
      },
      axisLabel: {
        textStyle: {
          fontFamily: 'Microsoft YaHei',
          color: '#FFF',
          padding: 4
        },
        fontSize: 14
        // rotate: 45 // 角度顺时针计算的
      }
    },
    yAxis: [
      {
        type: 'value',
        name: `单位：${props.unit1}`,
        nameTextStyle: {
          color: '#fff',
          fontSize: 14,
          padding: [0, 0, 0, 0]
        },
        min: 0,
        max: barMaxs,
        interval: (barMaxs / 5),
        axisLine: {
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.5
          }
        },
        splitLine: { // y轴底线
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.5// x轴底线透明度
          }
        },
        axisLabel: {
          show: true,
          textStyle: {
            fontFamily: 'Microsoft YaHei',
            color: '#FFF'
          },
          fontSize: 14
        }
      },
      {
        type: 'value',
        name: `单位：${props.unit2}`,
        position: 'right',
        show: true,
        nameTextStyle: { // 单位字体颜色
          color: '#fff',
          padding: [0, 0, 0, 5, 0]
        },
        min: 0,
        max: barMaxx,
        interval: (barMaxx / 5),
        axisLabel: {
          show: true,
          textStyle: {
            fontFamily: 'Microsoft YaHei',
            color: '#FFF'
          },
          fontSize: 14
        },
        axisLine: {
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2
          }
        },
        splitLine: { // y轴底线
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2 // y轴底线透明度
          }
        }
      }
    ],
    series: [{
      name: props.logoName[0],
      type: 'line',
      yAxisIndex: 0, // 使用的 y 轴的 index，在单个图表实例中存在多个 y轴的时候有用
      smooth: true, // 平滑曲线显示
      showAllSymbol: true, // 显示所有图形。
      symbol: 'circle', // 标记的图形为实心圆
      symbolSize: 10, // 标记的大小
      itemStyle: {
        // 折线拐点标志的样式
        color: '#058cff'
      },
      lineStyle: {
        color: '#058cff'
      },
      areaStyle: {
        color: 'rgba(5,140,255, 0.2)'
      },
      data: props.yAxisDatak
    },
    {
      name: props.logoName[1],
      type: 'bar',
      yAxisIndex: 1,
      barWidth: 15,
      itemStyle: {
        normal: {
          color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
            offset: 0,
            color: '#00FFE3'
          },
          {
            offset: 1,
            color: '#4693EC'
          }
          ])
        }
      },
      data: props.yAxisDataj
    }
    ]
  }
}
