import * as echarts from 'echarts'
import {formatInt} from '@/views/common/fn.js'
export default function (props) {
  let vacantAreas = Math.max(...props.yAxisDatas).toFixed()
  let vacantAreax = Math.max(...props.yAxisDatax).toFixed()
  let vacantAreasmin = Math.min(...props.yAxisDatas).toFixed()
  let vacantAreaxmin = Math.min(...props.yAxisDatax).toFixed()

  let barMaxsNum = formatInt(String(vacantAreas), String(vacantAreas).length - 1) > 10 ? formatInt(String(vacantAreas), String(vacantAreas).length - 1) : 10
  let barMaxxNum = formatInt(String(vacantAreax), String(vacantAreax).length - 1) > 10 ? formatInt(String(vacantAreax), String(vacantAreax).length - 1) : 10

  let barMinsNum = formatInt(String(vacantAreasmin), String(vacantAreasmin).length - 1, false)
  let barMinxNum = formatInt(String(vacantAreaxmin), String(vacantAreaxmin).length - 1, false)

  let vacants = Number(barMaxsNum) - Number(barMinsNum)
  let vacantx = Number(barMaxxNum) - Number(barMinxNum)

  return {
    tooltip: {
      trigger: 'axis',
      borderColor: 'none',
      backgroundColor: 'transparent',
      borderWidth: 0,
      padding: 0,
      textStyle: {
        color: 'white'
      },
      axisPointer: {
        lineStyle: {
          color: {
            type: 'linear',
            x: 0,
            y: 0,
            x2: 0,
            y2: 1,
            colorStops: [{
              offset: 0,
              color: 'rgba(126,199,255,0)' // 0% 处的颜色
            }, {
              offset: 0.5,
              color: 'rgba(126,199,255,1)' // 100% 处的颜色
            }, {
              offset: 1,
              color: 'rgba(126,199,255,0)' // 100% 处的颜色
            }],
            global: false // 缺省为 false
          }
        }
      },
      formatter: (params) => {
        console.log(params, 'paramsparamsparams')
        var str = `<div style = "
        border:2px solid #02436a;
        box-shadow: 0 0 6px 1px #02436a inset;
        background-color: #1195aa;
        text-align: left;
        padding:12px;
        >
        <span style=" display: inline-block; margin-top:40px;">年份：${params[0].name}</span>
        <br/>
        ${params[0].marker}
        <span>${params[0].seriesName}&nbsp;&nbsp;</span>
        <span>${params[0].value}${props.unit3 ? props.unit3 : ''}</span>
        <br/>
        ${params[1].marker}
        <span>${params[1].seriesName}&nbsp;&nbsp;</span>
        <span>${params[1].value}${props.unit4 ? props.unit4 : ''}</span>
        </div>
        `
        return str
      }
    },
    textStyle: {
      color: '#C9C9C9',
      axisPointer: {
        lineStyle: {
          color: {
            type: 'linear',
            x: 0,
            y: 0,
            x2: 0,
            y2: 1,
            colorStops: [{
              offset: 0,
              color: 'rgba(126,199,255,0)' // 0% 处的颜色
            }, {
              offset: 0.5,
              color: 'rgba(126,199,255,1)' // 100% 处的颜色
            }, {
              offset: 1,
              color: 'rgba(126,199,255,0)' // 100% 处的颜色
            }],
            global: false // 缺省为 false
          }
        }
      }
    },
    legend: {
      type: 'plain',
      orient: 'horizontal',
      selectedMode: false, // 图例点击失效
      itemGap: 12,
      itemWidth: 14,
      itemHeight: 14,
      align: 'left',
      right: '6%',
      top: '1%',
      left: '6%',
      icon: 'circle',
      textStyle: {
        padding: -2,
        color: '#ffffff',
        fontSize: 12
      },
      itemStyle: {
        borderWidth: 0
      },
      data: ['纳税金额', '单位面积纳税']
    },
    grid: {
      left: '4%',
      top: '26%',
      bottom: '4%',
      right: '4%',
      containLabel: true
    },
    xAxis: {
      type: 'category',
      data: props.xAxisDtat,
      axisLine: {
        show: false,
        lineStyle: {
          color: '#B5B5B5'
        }
      },
      axisTick: {
        show: false
      },
      axisLabel: {
        textStyle: {
          fontFamily: 'Microsoft YaHei',
          color: '#FFF',
          padding: 4
        },
        fontSize: 14
        // rotate: 45 // 角度顺时针计算的
      }
    },
    yAxis: [
      {
        type: 'value',
        name: `单位：${props.unit3}`,
        nameTextStyle: {
          color: '#fff',
          fontSize: 14,
          padding: [0, 0, 0, 0]
        },
        min: Number(barMinsNum),
        max: Number(barMaxsNum),
        interval: (Number(vacants) / 5),
        axisLine: {
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.5
          }
        },
        splitLine: { // y轴底线
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.5// x轴底线透明度
          }
        },
        axisLabel: {
          show: true,
          textStyle: {
            fontFamily: 'Microsoft YaHei',
            color: '#FFF'
          },
          fontSize: 14
        }
      },
      {
        type: 'value',
        name: `单位：${props.unit4}`,
        position: 'right',
        show: true,
        nameTextStyle: { // 单位字体颜色
          color: '#fff',
          padding: [0, 0, 0, 5, 0]
        },
        min: Number(barMinxNum),
        max: Number(barMaxxNum),
        interval: (Number(vacantx) / 5),
        axisLabel: {
          show: true,
          textStyle: {
            fontFamily: 'Microsoft YaHei',
            color: '#FFF'
          },
          fontSize: 14
        },
        axisLine: {
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2
          }
        },
        splitLine: { // y轴底线
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2 // y轴底线透明度
          }
        }
      }
    ],
    series: [{
      name: '纳税金额',
      type: 'bar',
      // stack: '总量',
      barMaxWidth: 15,
      // barGap: "10%",
      data: props.yAxisDatas.map((item) => {
        return {
          value: item,
          itemStyle: {
            normal: {
              barBorderRadius: item > 0 ? [15, 15, 0, 0] : [0, 0, 15, 15], // 左上角参数1, 右上角参数2, 右下角参数3, 左下角参数4
              color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                {
                  offset: 0,
                  color: item > 0 ? '#956FD4' : '#3EACE5'
                },
                {
                  offset: 1,
                  color: item > 0 ? '#3EACE5' : '#956FD4'
                }
              ])
            }
          }
        }
      })
    },
    {
      z: 9,
      yAxisIndex: 1,
      name: '单位面积纳税',
      type: 'line',
      stack: 'all',
      symbol: `path://M9.312,4.594 C12.074,4.594 14.313,6.832 14.313,9.594 C14.313,12.355 12.074,14.594 9.312,14.594 C6.551,14.594 4.312,12.355 4.312,9.594 C4.312,6.832 6.551,4.594 9.312,4.594 Z`,
      symbolSize: [10, 10],
      color: {
        type: 'linear',
        x: 1,
        y: 0,
        x2: 0,
        y2: 0,
        // 0% 处的颜色                           // 100% 处的颜色
        colorStops: [{ offset: 0, color: '#32ffee' }, { offset: 1, color: '#32ffee'}],
        global: false // 缺省为 false
      },
      lineStyle: { color: {
        type: 'linear',
        x: 1,
        y: 0,
        x2: 0,
        y2: 0,
        // 0% 处的颜色                           // 100% 处的颜色
        colorStops: [{ offset: 0, color: '#32ffee' }, { offset: 1, color: '#32ffee'}],
        global: false // 缺省为 false
      }},
      // 修改的是线下区域的颜色
      // areaStyle: {
      //   color: new echarts.graphic.LinearGradient(
      //     // 右/下/左/上
      //     0, 0, 0, 1, [
      //       { offset: 0, color: 'rgba(255, 209, 26, .2)' },
      //       { offset: 1, color: 'transparent' }
      //     ])
      // },
      label: {
        show: false,
        position: 'insideBottomRight',
        formatter: params => {
          return `${params.value}${props.unit2}`
        },
        textStyle: { fontSize: 16, color: '#ffd11a' }
      },
      data: props.yAxisDatax
    }
    ]
  }
}
