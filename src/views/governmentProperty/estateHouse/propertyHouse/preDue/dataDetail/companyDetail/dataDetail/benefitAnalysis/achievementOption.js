import * as echarts from 'echarts'
export default function (props) {
  return {
    tooltip: {
      trigger: 'axis',
      borderColor: 'none',
      backgroundColor: 'transparent',
      borderWidth: 0,
      padding: 0,
      textStyle: {
        color: 'white'
      },
      axisPointer: {
        lineStyle: {
          color: {
            type: 'linear',
            x: 0,
            y: 0,
            x2: 0,
            y2: 1,
            colorStops: [{
              offset: 0,
              color: 'rgba(126,199,255,0)' // 0% 处的颜色
            }, {
              offset: 0.5,
              color: 'rgba(126,199,255,1)' // 100% 处的颜色
            }, {
              offset: 1,
              color: 'rgba(126,199,255,0)' // 100% 处的颜色
            }],
            global: false // 缺省为 false
          }
        }
      },
      formatter: (params) => {
        var str = `<div style = "
        border:2px solid #02436a;
        box-shadow: 0 0 6px 1px #02436a inset;
        background-color: #1195aa;
        text-align: left;
        padding:12px;
        >
        <span style=" display: inline-block; margin-top:40px;">年份：${params[0].axisValue}</span>
        <br/>
        ${params[0].marker}
        <span>${params[0].seriesName}&nbsp;&nbsp;</span>
        <span>${params[0].value}${props.unit1 ? props.unit1 : ''}</span>
        </div>
        `
        return str
      }
    },
    legend: {
      type: 'plain',
      orient: 'horizontal',
      selectedMode: false, // 图例点击失效
      itemGap: 12,
      itemWidth: 14,
      itemHeight: 14,
      align: 'left',
      right: '2%',
      top: '3%',
      left: '6%',
      icon: 'circle',
      textStyle: {
        padding: -2,
        color: '#ffffff',
        fontSize: 12
      },
      itemStyle: {
        borderWidth: 0
      },
      data: props.logoName
    },
    grid: {
      left: '4%',
      top: '26%',
      bottom: '13%',
      right: '4%',
      containLabel: true
    },
    xAxis: {
      type: 'category',
      data: props.xAxisDtat,
      axisLine: {
        show: false,
        lineStyle: {
          color: '#B5B5B5'
        }
      },
      axisTick: {
        show: false
      },
      axisLabel: {
        textStyle: {
          fontFamily: 'Microsoft YaHei',
          color: '#FFF',
          padding: 4
        },
        fontSize: 14
        // rotate: 45 // 角度顺时针计算的
      }
    },
    yAxis: [
      {
        type: 'value',
        name: `单位：${props.unit1}`,
        nameTextStyle: {
          color: '#fff',
          fontSize: 14,
          padding: [0, 0, 0, 0]
        },
        axisLine: {
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.5
          }
        },
        splitLine: { // y轴底线
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.5// x轴底线透明度
          }
        },
        axisLabel: {
          show: true,
          textStyle: {
            fontFamily: 'Microsoft YaHei',
            color: '#FFF'
          },
          fontSize: 14
        }
      }
    ],
    series: [{
      name: props.logoName[0],
      type: 'bar',
      stack: '总量',
      barMaxWidth: 15,
      // barGap: "10%",
      data: props.yAxisData.map((item) => {
        return {
          value: item,
          itemStyle: {
            normal: {
              barBorderRadius: item > 0 ? [15, 15, 0, 0] : [0, 0, 15, 15], // 左上角参数1, 右上角参数2, 右下角参数3, 左下角参数4
              color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                {
                  offset: 0,
                  color: item > 0 ? '#956FD4' : '#3EACE5'
                },
                {
                  offset: 1,
                  color: item > 0 ? '#3EACE5' : '#956FD4'
                }
              ])
            }
          }
        }
      })
    }
    ]
  }
}
