// import * as echarts from 'echarts'
export default function (props) {
  let widthOutNumber = props.widthNum ? props.widthNum : 1.98 // 环外动画整体x轴位置，值越小越往右
  let heightOutNumber = props.heightNum ? props.heightNum : 2.42 // 环外动画整体y轴位置，值越小越往下

  let widthInNumber = props.widthInNum ? props.widthInNum : 2 // 环内动画整体x轴位置，值越小越往右
  let heightInNumber = props.heightInNum ? props.heightInNum : 2.45 // 环内动画整体y轴位置，值越小越往下

  let outLine1 = props.outsideLine1 ? props.outsideLine1 : 2.6 // 动画环外第一圈大小，值越小越大
  let outLine2 = props.outsideLine2 ? props.outsideLine2 : 2.8 // 动画环外第二圈大小，值越小越大
  let outLine3 = props.outsideLine3 ? props.outsideLine3 : 5.5 // 动画环内第三圈大小，值越小越大

  let angle = 0

  let option = {
    series: [
      {
        name: 'ring5', // 外环紫点
        type: 'custom',
        coordinateSystem: 'none',
        renderItem: function (params, api) {
          let x0 = api.getWidth() / widthOutNumber
          let y0 = api.getHeight() / heightOutNumber
          let r = Math.min(api.getWidth(), api.getHeight()) / outLine1
          let point = getCirlPoint(x0, y0, r, (90 + angle))
          return {
            type: 'circle',
            shape: {
              cx: point.x,
              cy: point.y,
              r: 4
            },
            style: {
              stroke: props.color[0] ? props.color[0] : '#8450F9',
              fill: props.color[0] ? props.color[0] : '#8450F9'
            },
            silent: true
          }
        },
        data: [0]
      },
      { // 外环紫线
        name: 'ring5',
        type: 'custom',
        coordinateSystem: 'none',
        renderItem: function (params, api) {
          return {
            type: 'arc',
            shape: {
              cx: api.getWidth() / widthOutNumber,
              cy: api.getHeight() / heightOutNumber,
              r: Math.min(api.getWidth(), api.getHeight()) / outLine1,
              startAngle: (0 + angle) * Math.PI / 180,
              endAngle: (90 + angle) * Math.PI / 180
            },
            style: {
              stroke: props.color[0] ? props.color[0] : '#8450F9',
              fill: 'transparent',
              lineWidth: 1.5
            },
            silent: true
          }
        },
        data: [0]
      },
      { // 外环深蓝点
        name: 'ring5',
        type: 'custom',
        coordinateSystem: 'none',
        renderItem: function (params, api) {
          let x0 = api.getWidth() / widthOutNumber
          let y0 = api.getHeight() / heightOutNumber
          let r = Math.min(api.getWidth(), api.getHeight()) / outLine1
          let point = getCirlPoint(x0, y0, r, (180 + angle))
          return {
            type: 'circle',
            shape: {
              cx: point.x,
              cy: point.y,
              r: 4
            },
            style: {
              stroke: props.color[1] ? props.color[1] : '#894d9c',
              fill: props.color[1] ? props.color[1] : '#894d9c'
            },
            silent: true
          }
        },
        data: [0]
      },
      { // 外环深蓝线
        name: 'ring5',
        type: 'custom',
        coordinateSystem: 'none',
        renderItem: function (params, api) {
          return {
            type: 'arc',
            shape: {
              cx: api.getWidth() / widthOutNumber,
              cy: api.getHeight() / heightOutNumber,
              r: Math.min(api.getWidth(), api.getHeight()) / outLine1,
              startAngle: (180 + angle) * Math.PI / 180,
              endAngle: (270 + angle) * Math.PI / 180
            },
            style: {
              stroke: props.color[1] ? props.color[1] : '#894d9c',
              fill: 'transparent',
              lineWidth: 1.5
            },
            silent: true
          }
        },
        data: [0]
      },
      {// 外环浅蓝点
        name: 'ring5',
        type: 'custom',
        coordinateSystem: 'none',
        renderItem: function (params, api) {
          let x0 = api.getWidth() / widthOutNumber
          let y0 = api.getHeight() / heightOutNumber
          let r = Math.min(api.getWidth(), api.getHeight()) / outLine2
          let point = getCirlPoint(x0, y0, r, (270 + -angle))
          return {
            type: 'circle',
            shape: {
              cx: point.x,
              cy: point.y,
              r: 4
            },
            style: {
              stroke: props.color[2] ? props.color[2] : '#0CD3DB',
              fill: props.color[2] ? props.color[2] : '#0CD3DB'
            },
            silent: true
          }
        },
        data: [0]
      },
      { // 外环浅蓝线
        name: 'ring5',
        type: 'custom',
        coordinateSystem: 'none',
        renderItem: function (params, api) {
          return {
            type: 'arc',
            shape: {
              cx: api.getWidth() / widthOutNumber,
              cy: api.getHeight() / heightOutNumber,
              r: Math.min(api.getWidth(), api.getHeight()) / outLine2,
              startAngle: (270 + -angle) * Math.PI / 180,
              endAngle: (40 + -angle) * Math.PI / 180
            },
            style: {
              stroke: props.color[2] ? props.color[2] : '#0CD3DB',
              fill: 'transparent',
              lineWidth: 1.5
            },
            silent: true
          }
        },
        data: [0]
      },
      { // 外环粉红点
        name: 'ring5',
        type: 'custom',
        coordinateSystem: 'none',
        renderItem: function (params, api) {
          let x0 = api.getWidth() / widthOutNumber
          let y0 = api.getHeight() / heightOutNumber
          let r = Math.min(api.getWidth(), api.getHeight()) / outLine2
          let point = getCirlPoint(x0, y0, r, (90 + -angle))
          return {
            type: 'circle',
            shape: {
              cx: point.x,
              cy: point.y,
              r: 4
            },
            style: {
              stroke: props.color[3] ? props.color[3] : '#FF8E89',
              fill: props.color[3] ? props.color[3] : '#FF8E89'
            },
            silent: true
          }
        },
        data: [0]
      },
      { // 外环粉红线
        name: 'ring5',
        type: 'custom',
        coordinateSystem: 'none',
        renderItem: function (params, api) {
          return {
            type: 'arc',
            shape: {
              cx: api.getWidth() / widthOutNumber,
              cy: api.getHeight() / heightOutNumber,
              r: Math.min(api.getWidth(), api.getHeight()) / outLine2,
              startAngle: (90 + -angle) * Math.PI / 180,
              endAngle: (220 + -angle) * Math.PI / 180
            },
            style: {
              stroke: props.color[3] ? props.color[3] : '#FF8E89',
              fill: 'transparent',
              lineWidth: 1.5
            },
            silent: true
          }
        },
        data: [0]
      },

      { //  内环深蓝线
        name: 'ring5',
        type: 'custom',
        coordinateSystem: 'none',
        renderItem: function (params, api) {
          return {
            type: 'arc',
            shape: {
              cx: api.getWidth() / widthInNumber,
              cy: api.getHeight() / heightInNumber,
              r: Math.min(api.getWidth(), api.getHeight()) / outLine3,
              startAngle: (0 + angle) * Math.PI / 180,
              endAngle: (100 + angle) * Math.PI / 180
            },
            style: {
              stroke: props.color[4] ? props.color[4] : '#FF8E89',
              fill: 'transparent',
              lineWidth: 1.5
            },
            silent: true
          }
        },
        data: [0]
      },
      { // 内环浅蓝线
        name: 'ring5',
        type: 'custom',
        coordinateSystem: 'none',
        renderItem: function (params, api) {
          return {
            type: 'arc',
            shape: {
              cx: api.getWidth() / widthInNumber,
              cy: api.getHeight() / heightInNumber,
              r: Math.min(api.getWidth(), api.getHeight()) / outLine3,
              startAngle: (120 + angle) * Math.PI / 180,
              endAngle: (220 + angle) * Math.PI / 180
            },
            style: {
              stroke: props.color[4] ? props.color[4] : '#FF8E89',
              fill: 'transparent',
              lineWidth: 1.5
            },
            silent: true
          }
        },
        data: [0]
      },
      { // 内环粉红线
        name: 'ring5',
        type: 'custom',
        coordinateSystem: 'none',
        renderItem: function (params, api) {
          return {
            type: 'arc',
            shape: {
              cx: api.getWidth() / widthInNumber,
              cy: api.getHeight() / heightInNumber,
              r: Math.min(api.getWidth(), api.getHeight()) / outLine3,
              startAngle: (240 + angle) * Math.PI / 180,
              endAngle: (340 + angle) * Math.PI / 180
            },
            style: {
              stroke: props.color[4] ? props.color[4] : '#FF8E89',
              fill: 'transparent',
              lineWidth: 1.5
            },
            silent: true
          }
        },
        data: [0]
      }
    ]

  }

  // 获取圆上面某点的坐标(x0,y0表示坐标，r半径，angle角度)
  function getCirlPoint (x0, y0, r, angle) {
    let x1 = x0 + r * Math.cos(angle * Math.PI / 180)
    let y1 = y0 + r * Math.sin(angle * Math.PI / 180)
    return {
      x: x1,
      y: y1
    }
  }
  let stepNum = props.step ? props.step : 1
  function draw () {
    angle = angle + stepNum
    props.eChart.setOption(option, true)
    // window.requestAnimationFrame(draw) // 帧动画函数
  }
  // window.requestAnimationFrame(draw) // 帧动画函数

  let timer = null
  if (props.isTimer) {
    timer = setInterval(function () {
      draw()
    }, 100)
  } else {
    if (timer) {
      clearInterval(timer)
    }
  }
}
