//横向bar
import * as echarts from 'echarts'
export default function (props) { //{category:[],data:[],colors:[],max:500,axisUnit:'（元）'}
  var charts = {
    cityList: props.category,
    cityData: props.data
  }
  var color = props.colors || ['rgba(47, 179, 204, 1)', 'rgba(85, 221, 185, 1)']

  let lineY = []
  let lineT = []
  for (var i = 0; i < charts.cityList.length; i++) {
    var data = {
      name: charts.cityList[i],
      //color: color[0] + ')',
      value: charts.cityData[i],
      itemStyle: {
        normal: {
          show: true,
          color: new echarts.graphic.LinearGradient(0, 0, 1, 0, [{
            offset: 0,
            color: color[0]
          }, {
            offset: 1,
            color: color.length > 1 ? color[1] : color[0]
          }], false),
          barBorderRadius: 10
        },
        emphasis: {
          shadowBlur: 15,
          shadowColor: 'rgba(0, 0, 0, 0.1)'
        }
      }
    }
    var data1 = {
      value: props.max || 500,
      itemStyle: {
        color: 'rgba(255,255,255,0.1)',
        barBorderRadius: 10
      }
    }
    lineY.push(data)
    lineT.push(data1)
  }



  return {
    //backgroundColor: '#000',
    title: [{
      right: 5,
      top: 25,
      text: props.axisUnit || '',
      textStyle: {
        fontSize: 11,
        color: '#DDEEFF',
      },
    }],
    // tooltip: {
    //   trigger: 'item',
    //   borderColor: 'none',
    //   backgroundColor: 'transparent',
    //   borderWidth: 0,
    //   padding: 0,
    //   textStyle: {
    //     color: 'white'
    //   },
    //   axisPointer: {
    //     type: 'shadow',
    //   },
    //   formatter: (params) => {
    //     console.log(params)
    //     var str = '';
    //     if (params.seriesName == 'bar') {
    //       str = `<div style = " 
    //                              border:1px solid #02fdff;
    //                              background-color: rgba(0,0,0,0.5);
    //                              text-align: left;
    //                              padding:5px 12px;
    //     >
    //     <span>${params.name}&nbsp;&nbsp;</span>
    //     <span style="color:#02fdff;">${params.value}家</span>
    //     </div>
    //     `
    //     }
    //     return str
    //   }
    // },
    grid: {
      borderWidth: 0,
      top: '10%',
      left: '5%',
      right: '15%',
      bottom: '3%'
    },
    color: color,
    yAxis: [{
      type: 'category',
      inverse: true,
      axisTick: {
        show: false
      },
      axisLine: {
        show: false
      },
      axisLabel: {
        show: false,
        inside: false
      },
      data: charts.cityList
    }, {
      type: 'category',
      inverse: true,
      axisLine: {
        show: false
      },
      axisTick: {
        show: false
      },
      axisLabel: {
        show: true,
        inside: false,
        textStyle: {
          color: '#ffffff',
          fontSize: '12',
          fontFamily: 'PingFangSC-Regular'
        },
        formatter: function (val, a) {
          return `${val}`
        }
      },
      splitArea: {
        show: false
      },
      splitLine: {
        show: false
      },
      data: charts.cityData
    }],
    xAxis: {
      type: 'value',
      axisTick: {
        show: false
      },
      axisLine: {
        show: false
      },
      splitLine: {
        show: false
      },
      axisLabel: {
        show: false
      }
    },
    series: [{
        name: 'total',
        type: 'bar',
        zlevel: 1,
        barGap: '-100%',
        barWidth: '10px',
        data: lineT,
        legendHoverLink: false
      },
      {
        name: 'bar',
        type: 'bar',
        zlevel: 2,
        barWidth: '10px',
        data: lineY,
        //data:charts.cityData,
        animationDuration: 1500,
        label: {
          normal: {
            color: '#ffffff',
            show: true,
            position: [0, '-24px'],
            textStyle: {
              fontSize: 12
            },
            formatter: function (a, b) {
              return a.name
            }
          }
        }
      }
    ],
    animationEasing: 'cubicOut'
  }
}
