//柱状图
import * as echarts from 'echarts'
export default function (props) {
  // props示例
  // {
  //     yAxisName: '（家）',
  //     tooltip: {
  //       show: false
  //     },
  //     axisLabelWrap: 4,//x坐标 4个字换行
  //     xAxisData: [
  //       "2019年",
  //       "2020年",
  //       "2021年",
  //     ],
  //     yAxisDatax: [
  //       67, 74, 89
  //     ],
  //   }
  var sum = props.yAxisDatax.reduce((n, m) => n + m);
  let showTooltip = true;
  if (props.tooltip) {
    if (props.tooltip.show === false) showTooltip = false;
  }

  let formatterFn = null; //x坐标换行
  if (props.axisLabelWrap) {
    formatterFn = (name) => {
      if (!name) return ''
      let text = ''
      if (name.length > props.axisLabelWrap) {
        let count = Math.ceil(name.length / props.axisLabelWrap) // 向上取整数
        // 一行展示4个
        if (count > 1) {
          for (let z = 1; z <= count; z++) {
            text += name.substr((z - 1) * props.axisLabelWrap, props.axisLabelWrap)
            if (z < count) {
              text += '\n'
            }
          }
        } else {
          text += name.substr(0, props.axisLabelWrap)
        }
      } else {
        text = name
      }
      return text
    };
  }

  return {
    tooltip: {
      show: showTooltip,
      trigger: 'axis',
      borderColor: 'none',
      backgroundColor: 'transparent',
      borderWidth: 0,
      padding: 0,
      textStyle: {
        color: 'white'
      },
      axisPointer: {
        type: 'shadow',
        // lineStyle: {
        //   color: {
        //     type: 'linear',
        //     x: 0,
        //     y: 0,
        //     x2: 0,
        //     y2: 1,
        //     colorStops: [{
        //       offset: 0,
        //       color: 'rgba(126,199,255,0)' // 0% 处的颜色
        //     }, {
        //       offset: 0.5,
        //       color: 'rgba(126,199,255,1)' // 100% 处的颜色
        //     }, {
        //       offset: 1,
        //       color: 'rgba(126,199,255,0)' // 100% 处的颜色
        //     }],
        //     global: false // 缺省为 false
        //   }
        // }
      },
      formatter: (params) => {
        var rate = 0; //百分比
        if (sum > 0) {
          rate = (params[0].value / sum) * 100;
          rate = rate.toFixed(2);
        }
        var str = `<div style = " 
                                 border:1px solid #02fdff;
                                 background-color: rgba(0,0,0,0.5);
                                 text-align: left;
                                 padding:5px 12px;
        >
        ${params[0].marker}
        <span>${params[0].name}&nbsp;&nbsp;</span>
        <span style="color:#02fdff;">${params[0].value}${props.yAxisName}</span>
        </div>
        `
        return str
      }
    },
    legend: {
      textStyle: {
        color: '#fff',
        fontSize: 12
      },
      top: '0%',
      selectedMode: false, // 图例点击失效
      itemGap: 25,
      itemWidth: 18,
      icon: 'circle',
      data: props.tipData
    },
    grid: {
      top: (props.grid && props.grid.top) ? props.grid.top : '18%',
      right: (props.grid && props.grid.right) ? props.grid.right : '3%',
      bottom: (props.grid && props.grid.bottom) ? props.grid.bottom : '9%',
      left: (props.grid && props.grid.left) ? props.grid.left : '3%',
      containLabel: true
    },
    xAxis: [{
      type: 'category',
      // boundaryGap: false,
      axisLine: { // 坐标轴轴线相关设置。数学上的x轴
        show: true,
        lineStyle: {
          color: '#233653',
          type: 'dashed',
          opacity: 0.2 // x轴第一根底线透明度
        }
      },
      axisLabel: {
        show: true,
        interval: 0,
        left: 10, // 左边的距离
        right: 10, // 右边的距离
        bottom: 10, // 右边的距离
        formatter: formatterFn,
        textStyle: {
          color: '#fff', // 坐标值得具体的颜色
          lineHeight: 16 // 图例文字行高
        }
      },
      splitLine: { // y轴底线
        show: false,
        lineStyle: {
          color: ['#00FFFF'],
          type: 'dashed',
          opacity: 0.2 // y轴底线透明度
        }
      },
      axisTick: {
        show: false
      },
      data: props.xAxisData
    }],
    yAxis: [{
      name: props.yAxisName,
      nameTextStyle: {
        color: '#fff',
        fontSize: 12
        // padding: 10
      },
      min: 0,
      // max: 700,
      // interval: 100,
      splitLine: {
        show: true,
        lineStyle: {
          color: ['#00FFFF'],
          type: 'dashed',
          opacity: 0.5
        }
      },
      axisLine: { // y轴第一根底线配置
        show: true,
        lineStyle: {
          color: ['#00FFFF'],
          type: 'dashed',
          opacity: 0.5
        }
      },
      axisLabel: {
        show: true,
        textStyle: {
          color: '#fff'
          // padding: 16
        },
        formatter: function (value) {
          if (value === 0) {
            return value
          }
          return value
        }
      },
      axisTick: {
        show: false
      }
    }],
    series: [{
      type: 'bar',
      data: props.yAxisDatax,
      barWidth: '18px',
      itemStyle: {
        normal: {
          // color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
          //   offset: 0,
          //   color: 'rgba(0,244,255,1)' // 0% 处的颜色
          // }, {
          //   offset: 1,
          //   color: 'rgba(0,77,167,1)' // 100% 处的颜色
          // }], false),
          // barBorderRadius: [30, 30, 30, 30],
          // shadowColor: 'rgba(0,160,221,1)',
          // shadowBlur: 4
          color: 'rgba(0,213,255,0.4)', //'#00d5ff'
          barBorderColor: '#00d5ff',
          barBorderWidth: 1,
        }
      },
      label: {
        show: true,
        position: 'top',
        //distance: 10,
        color: '#fff',
      }
    }]
  }
}
