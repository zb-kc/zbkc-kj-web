// import * as echarts from 'echarts'
export default function (props) {
  let num = 0
  props.data.forEach((item) => {
    num += Number(item.value)
  })
  let pieData = []
  if (props.data && Array.isArray(props.data) && props.data.length) {
    for (let i = 0; i < props.data.length; i++) {
      pieData.push(
        {
          value: props.data[i].value,
          name: props.data[i].name,
          itemStyle: {
            normal: {
              color: (props.color)[i],
              shadowBlur: 6,
              shadowColor: (props.color)[i]
            }
          }
        },
        {
          value: props.val,
          name: '',
          itemStyle: {
            normal: {
              color: 'rgba(0,0,0,0)'
            }
          }
        }
      )
    }
  }
  return {
    tooltip: {
      show: true, // 是否显示提示框
      trigger: 'item',
      borderColor: 'none',
      backgroundColor: 'transparent',
      borderWidth: 0,
      padding: 0,
      formatter: function (params) {
        if (params.data.value !== props.val) {
          let str = `<div style = "
          border:2px solid #02436a;
          box-shadow: 0 0 6px 1px #02436a inset;
          background-color: #1195aa;
          text-align: left;
          padding: 12px 16px;
            >
            <span>
            ${params.marker}
            </span>
            <span>${params.data.name}&nbsp;&nbsp;</span>
            <span>${params.data.value}${props.unit ? props.unit : ''}&nbsp;&nbsp;</span>
            <span>${((params.data.value / num) * 100).toFixed(2)}%</span>
            </span>`
          return str
        }
      },
      textStyle: {
        color: 'white'
      }
    },
    legend: {
      top: props.legendTop ? props.legendTop : '0',
      left: props.legendLeft ? props.legendLeft : 'center',
      // left: '3%',
      // right: '10%',
      // bottom: '80%',
      show: true,
      type: 'plain',
      itemWidth: 10,
      itemHeight: 10,
      orient: props.verticalHorizontal ? props.verticalHorizontal : 'horizontal',
      selectedMode: false, // 图例点击失效
      // itemGap: 10, // 图例之间的间隔
      textStyle: { // 图例文字的样式
        color: '#fff',
        fontSize: 12
        // lineHeight: 22 // 图例文字行高
      },
      formatter: function (name) {
        var data = props.data
        var tarValue = 0
        for (var i = 0; i < data.length; i++) {
          if (data[i].name === name) {
            tarValue = data[i].value
          }
        }
        var p = ((tarValue / num) * 100).toFixed(2)
        if (props.hideLegendValue) {
          return `${name}`
        } else {
          return `${name}   ${tarValue}${props.unit ? props.unit : ''}   ${p}%`
        }
      },
      data: props.data ? props.data : []
    },

    title: {
      show: props.title ? props.title : true,
      zlevel: 0, // 默认展示
      text: [
        `{name|${props.total}}`,
        `{value|${num}${props.unit}}`
      ].join('\n'),
      rich: {
        value: {
          color: '#fff',
          fontSize: 16,
          fontWeight: 'bold',
          lineHeight: 22
        },
        name: {
          color: '#fff',
          fontSize: 16,
          fontWeight: 'bold',
          lineHeight: 22
        }
      },
      top: props.titleTop ? props.titleTop : '33%',
      left: props.titleLeft ? props.titleLeft : '48%',
      textAlign: 'center',
      textStyle: {
        rich: {
          value: {
            color: '#fff',
            fontSize: 16,
            fontWeight: 'bold',
            lineHeight: 22
          },
          name: {
            color: '#fff',
            fontSize: 16,
            lineHeight: 22
          }
        }
      }
    },
    series: [
      {
        top: props.seriesTop ? props.seriesTop : '',
        left: props.seriesLeft ? props.seriesLeft : '',
        name: props.name ? props.name : '',
        type: 'pie',
        radius: props.ringRadius ? props.ringRadius : ['44%', '64%'],
        center: props.ringCenter ? props.ringCenter : ['50%', '41%'],
        avoidLabelOverlap: false,
        itemStyle: {
          borderRadius: 3 // 圆环圆角弧度
        },
        labelLine: {
          show: props.labelLine ? props.labelLine : false,
          length: props.labelLineLength1 ? props.labelLineLength1 : 24, // 视觉引导线第一段的长度。
          length2: props.labelLineLength2 ? props.labelLineLength2 : 30, // 视觉引导项第二段的长度。
          maxSurfaceAngle: props.labelLineAngle ? props.labelLineAngle : 80 // 通过调整第二段线的长度，限制引导线与扇区法线的最大夹角。设置为小于 90 度的值保证引导线不会和扇区交叉。可以设置为 0 - 180 度。
        },
        label: {
          show: props.label ? props.label : false,
          formatter: (item) => {
            if (item.value !== props.val) {
              if (props.fullName) {
                return `${item.name}：${item.value}${props.unit ? props.unit : ''}  ${(item.value / num * 100).toFixed(2)}%`
              } else if (props.anonymous) {
                return `${item.name}：${(item.value / num * 100).toFixed(2)}%`
              } else {
                return `${(item.value / num * 100).toFixed(2)}%`
              }
            }
          },
          color: '#fff',
          borderColor: '#02436a',
          backgroundColor: '#1195aa',
          padding: [8, 4, 4, 4],
          borderRadius: 3,
          rich: {
            per: {
              color: '#fff',
              borderColor: '#02436a',
              backgroundColor: '#1195aa',
              padding: [4, 4]
            }
          }
        },
        data: pieData
      }
    ]

  }
}
