//柱状+拆线
import * as echarts from 'echarts'
export default function (props) {
  // props示例
  // {
  //   bar: {
  //     unit: '（亿元）',
  //     max: 1600
  //   },
  //   line: {
  //     unit: '（万元/㎡）',
  //     max: 20
  //   },
  //   tooltip:{//tooltip一般不需要传
  //    position:function (p) {
  //      return [p[0] + 20, p[1] + 20];
  //    }
  //   },
  //   xAxisData: [
  //     '2018',
  //     '2019',
  //     '2020',
  //     '2021（1-9月）'
  //   ],
  //   legendData: ['产值（亿元）', '单位面积产值（万元/㎡）'],
  //   yAxisDataz: [
  //     987.13, 1109.97, 1411, 1324.16
  //   ],
  //   yAxisDatas: [12, 14, 17, 15]
  // }

  let barUnit = '';
  let lineUnit = '';
  if (props.bar && props.bar.unit) {
    barUnit = props.bar.unit;
  }
  if (props.line && props.line.unit) {
    lineUnit = props.line.unit;
  }
  let tooltip = {
    trigger: 'axis',
    borderColor: 'none',
    backgroundColor: 'transparent',
    borderWidth: 0,
    padding: 0,
    textStyle: {
      color: 'white'
    },
    formatter: function (params) {
      var str = `<div style = " 
      border:1px solid #02fdff;
      background-color: rgba(0,0,0,0.5);
      text-align: left;
      padding:5px 12px;
      >
       <span style=" display: inline-block; margin-top:40px; margin-right:40px;">${params[0].name}</span>
       <br/>
       <span style="display: inline-block; margin-right:16px; margin-bottom: 12px;">
       ${params[0].marker}
       <span>${params[0].seriesName}&nbsp;&nbsp;&nbsp;&nbsp;</span>
       <span style="color:#02fdff;">${params[0].value}${barUnit}</span>
       <br/>
     
       ${params[1].marker}
       <span>${params[1].seriesName}&nbsp;&nbsp;&nbsp;&nbsp;</span>
       <span style="color:#02fdff;">${params[1].value}${lineUnit}</span>
      </span>
      `
      return str
    }
  };

  if (props.tooltip && props.tooltip.position) {
    tooltip.position = props.tooltip.position;
  }
  return {
    tooltip: tooltip,
    color: ['#01ffff', '#fdef00'],
    legend: {
      left: 'center',
      itemWidth: 20,
      itemHeight: 10,
      textStyle: {
        fontSize: 14,
        color: '#fff'
        // lineHeight: 22 // 图例文字行高
      },
      selectedMode: false, // 图例点击失效
      data: props.legendData ? props.legendData : []

    },
    grid: {
      top: '18%',
      left: '3%',
      right: '3%',
      bottom: '9%',
      containLabel: true
    },
    xAxis: [{
      type: 'category',
      data: props.xAxisData ? props.xAxisData : [],
      axisPointer: {
        type: 'shadow'
      },
      axisTick: {
        show: false
      },
      offset: 4,
      axisLine: {
        show: true,
        lineStyle: {
          type: 'dashed',
          opacity: 0.2
        }
      },

      axisLabel: {
        show: true,
        interval: 0,
        // formatter: (name) => { // 图例竖着排列时超过6个字就换行
        //   if (!name) return ''
        //   let text = ''
        //   if (name.length > 4) {
        //     let count = Math.ceil(name.length / 4) // 向上取整数
        //     // 一行展示4个
        //     if (count > 1) {
        //       for (let z = 1; z <= count; z++) {
        //         text += name.substr((z - 1) * 4, 4)
        //         if (z < count) {
        //           text += '\n'
        //         }
        //       }
        //     } else {
        //       text += name.substr(0, 4)
        //     }
        //   } else {
        //     text = name
        //   }
        //   return text
        // },
        //rotate: 45, // 角度顺时针计算的
        textStyle: {
          color: '#fff', // 坐标值得具体的颜色
          lineHeight: 16 // 图例文字行高
        }
      }
    }],
    yAxis: [{
        type: 'value',
        name: barUnit,
        nameTextStyle: {
          color: '#fff',
          padding: [0, 0, 0, 5, 0]
        },
        min: 0,
        max: (props.bar && props.bar.max) || null,
        interval: (props.bar && props.bar.max) ? (props.bar.max / 5) : null,
        axisLabel: {
          formatter: '{value}',
          textStyle: {
            color: '#fff' // 坐标值得具体的颜色
          }
        },
        axisLine: {
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2
          }
        },
        splitLine: { // x轴底线
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2 // x轴底线透明度
          }
        }
      },
      {
        type: 'value',
        name: lineUnit,
        nameTextStyle: { // 单位字体颜色
          color: '#fff',
          padding: [0, 0, 0, 5, 0]
        },
        min: 0,
        max: (props.line && props.line.max) || null,
        interval: (props.line && props.line.max) ? (props.line.max / 5) : null,
        axisLabel: {
          formatter: '{value}'
        },
        axisLine: {
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2
          }
        },
        splitLine: { // y轴底线
          show: true,
          lineStyle: {
            color: ['#00FFFF'],
            type: 'dashed',
            opacity: 0.2 // y轴底线透明度
          }
        }
      }
    ],
    series: [{
        name: props.legendData[0],
        type: 'bar',
        barWidth: 18,
        barGap: '-100%',
        itemStyle: { // lenged文本
          //opacity: 0.2,
          // color: function (params) {
          //   return new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
          //     offset: 0,
          //     color: '#106e7a' // 0% 处的颜色
          //   }, {
          //     offset: 1,
          //     color: '#00ffff' // 100% 处的颜色
          //   }], false)
          // }
          color: 'rgba(0,213,255,0.4)', //'#00d5ff'
          barBorderColor: '#00d5ff',
          barBorderWidth: 1,
        },
        data: props.yAxisDataz,
        label: {
          show: true,
          position: 'inside',
          //distance: 10,
          color: 'rgba(0,213,255,1)',
        }
      },
      {
        name: props.legendData[1],
        type: 'line',
        yAxisIndex: 1,
        symbol: 'circle',
        symbolSize: 4,
        zlevel: 2,
        // itemStyle: {
        //   color: '#66ff00',
        //   borderColor: 'rgba(102, 255, 0, 0.5)',
        //   borderWidth: 5
        // },
        // smooth: false,
        // lineStyle: {
        //   width: 1,
        //   color: {
        //     type: 'linear',
        //     x: 0,
        //     y: 0,
        //     x2: 0,
        //     y2: 1,
        //     colorStops: [{
        //       offset: 0,
        //       color: 'rgba(255,255,0,1)'
        //     }, {
        //       offset: 1,
        //       color: 'rgba(0,255,0,0.5)'
        //     }],
        //     global: false // 缺省为 false
        //   },
        //   shadowColor: 'rgba(0,0,0,0.6)',
        //   shadowBlur: 4,
        //   shadowOffsetY: 6,
        //   zlevel: 2
        // },
        // areaStyle: {
        //   color: new echarts.graphic.LinearGradient(0, 1, 0, 0, [{
        //     offset: 0,
        //     color: 'rgba(0,255,0,0)'
        //   }, {
        //     offset: 1,
        //     color: 'rgba(0,255,0,0.15)'
        //   }]),
        // },
        itemStyle: {
          color: '#fff143',
          borderColor: '#fff143', //  拐点边框颜色
          borderWidth: 1, //  拐点边框宽度
          shadowColor: '#fff143', //  阴影颜色
          shadowBlur: 10, //  阴影渐变范围控制
          emphasis: { // 突出效果配置(鼠标置于拐点上时)
            borderColor: '#fff143', //  拐点边框颜色
            borderWidth: 2, //  拐点边框宽度
            shadowColor: '#fff143', //  阴影颜色
            shadowBlur: 14 //  阴影渐变范围控制
          }
        },
        lineStyle: {
          // type: 'dashed'
          width: 1, // 折线粗线
          color: '#fff143',
          shadowColor: '#fff143',
          shadowBlur: 6,
          zlevel: 2
        },

        data: props.yAxisDatas,
        label: {
          show: true,
          position: 'top',
          //distance: 10,
          color: '#fff143',
        }
      },
      // { // 柱子顶部圆片
      //   name: '',
      //   type: 'pictorialBar',
      //   symbolSize: [14, 10],
      //   symbolOffset: [0, -6],
      //   symbolPosition: 'end',
      //   z: 1,
      //   zlevel: 1,
      //   itemStyle: {
      //     opacity: 1,
      //     color: function (params) {
      //       return new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
      //         offset: 0,
      //         color: '#0ea4aa' // 0% 处的颜色
      //       }, {
      //         offset: 1,
      //         color: '#0ea4aa' // 100% 处的颜色
      //       }], false)
      //     }
      //   },
      //   data: props.yAxisDataz
      // },
      // { // 柱子底部圆片
      //   name: '',
      //   type: 'pictorialBar',
      //   symbolSize: [14, 10],
      //   symbolOffset: [0, -6],
      //   symbolPosition: 'end',
      //   z: 1,
      //   zlevel: 1,
      //   itemStyle: {
      //     opacity: 1,
      //     color: function (params) {
      //       return new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
      //         offset: 0,
      //         color: '#00FFFF' // 0% 处的颜色
      //       }, {
      //         offset: 1,
      //         color: '#00FFFF' // 100% 处的颜色
      //       }], false)
      //     }
      //   },
      //   data: footArr
      // }
    ],
    dataZoom: [
      // {
      //   type: 'slider',
      //   show: true,
      //   filterMode: 'none',
      //   height: 10,
      //   with: '100%',
      //   textStyle: {
      //     color: '#fff'
      //   },
      //   // zoomLock: true,
      //   orient: 'horizontal',
      //   startValue: 0,
      //   endValue: 0,
      //   xAxisIndex: [0],
      //   showDataShadow: false,
      //   fillerColor: new echarts.graphic.LinearGradient(1, 0, 0, 0, [{
      //     // 给颜色设置渐变色 前面4个参数，给第一个设置1，第四个设置0 ，就是水平渐变
      //     // 给第一个设置0，第四个设置1，就是垂直渐变
      //     offset: 1,
      //     color: 'rgba(0, 99, 179, 1)'
      //   }, {
      //     offset: 0,
      //     color: 'rgba(29, 217, 239, 1)'
      //   }]),
      //   backgroundColor: 'rgba(0,0,0,0.3)',
      //   // 拖拽手柄样式 svg 路径
      //   handleIcon: 'M512 512m-208 0a6.5 6.5 0 1 0 416 0 6.5 6.5 0 1 0-416 0Z M512 192C335.264 192 192 335.264 192 512c0 176.736 143.264 320 320 320s320-143.264 320-320C832 335.264 688.736 192 512 192zM512 800c-159.072 0-288-128.928-288-288 0-159.072 128.928-288 288-288s288 128.928 288 288C800 671.072 671.072 800 512 800z',
      //   left: 18,
      //   right: 18,
      //   bottom: 18,
      //   start: 0,
      //   end: props.xAxisData.length > 13 ? 28 : 100
      // },
      // {
      //   type: 'inside',
      //   show: true,
      //   height: 10,
      //   start: 0,
      //   end: props.xAxisData.length > 13 ? 28 : 100
      // }
    ]
  }
}
