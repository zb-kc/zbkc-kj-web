// 数值取整为10的倍数
export function formatInt (num, prec = 2, ceil = true) {
  const len = String(num).length
  if (len <= prec) { return num };

  const mult = Math.pow(10, prec)
  return ceil
    ? Math.ceil(num / mult) * mult
    : Math.floor(num / mult) * mult
}
