import * as echarts from 'echarts'
export default function (props) {
  var color = props.color || [
    '#1557f4',
    '#13caf6',
    '#febf00',
    '#ff8c19',
    '#fd198a'
  ];
  var title = props.title || '';
  var legend = props.legendData || [];
  var seriesData = props.data || []

  var sum = seriesData.reduce((s, cur) => s + cur.value, 0); //合计
  var getRate = function (val) {
    var rate = 0; //百分比
    if (sum > 0) {
      rate = (val / sum) * 100;
      rate = rate.toFixed(2);
    }
    return rate;
  }

  return {
    color: color,
    title: [{
        left: 160,
        top: 15,
        text: title,
        textStyle: {
          fontSize: 12,
          color: '#DDEEFF',
        },
      },
      // {
      //   text: sum + '家',
      //   top: '46%',
      //   textAlign: "center",
      //   left: "44%",
      //   textStyle: {
      //     color: '#fff',
      //     fontSize: 22,
      //     fontWeight: '400'
      //   }
      // }
    ],
    grid: {
      top: '5%',
      //left: '25%',
      // right: '1%',
      // bottom: 5,
      // containLabel: true,
    },
    tooltip: {
      show: true, // 是否显示提示框
      trigger: 'item',
      borderColor: 'none',
      backgroundColor: 'transparent',
      borderWidth: 0,
      padding: 0,
      position: function (p) { //其中p为当前鼠标的位置
        return [p[0] - 100, p[1] + 20];
      },
      formatter: function (params) {
        //console.log(params, '---params---')
        let str = `<div style = "
        border:1px solid #02fdff;
        background-color: rgba(0,0,0,0.5);
        text-align: left;
        padding:5px 12px;
            >
            ${params.marker}${params.marker}
            <span>${params.name}&nbsp;&nbsp;</span>
            <span style="color:#02fdff;">${params.value}${props.unit}&nbsp;&nbsp;${getRate(params.value)}%</span>
            </div>`
        return str
      },
      textStyle: {
        color: 'white'
      }
    },
    legend: {
      //orient: 'vertical',
      //top: 'center',
      //right: 5,
      bottom: 5,
      width: 140,
      icon: 'react',
      itemWidth: 5,
      itemHeight: 5,
      selectedMode: false, // 图例点击失效
      textStyle: {
        align: 'left',
        verticalAlign: 'middle',
        rich: {
          name: {
            color: 'rgba(255,255,255,0.9)',
            fontSize: 12,
            padding: [2, 0, 0, 0],
            align: 'left',
            width: 50
          },
          rate: {
            color: 'rgba(255,255,255,0.9)',
            fontSize: 12,
            align: 'right',
            width: 65
          },
        },
      },
      data: legend,
      formatter: (name) => {
        if (seriesData.length) {
          const item = seriesData.filter((item) => item.name === name)[0];
          //return `{name|${name}：} {rate| ${getRate(item.value)}%}`;
          return `{name|${name}}`;
        }
      },
    },
    series: [{
        name: '需求类型占比',
        type: 'pie',
        center: ['45%', '45%'],
        radius: ['40%', '53%'],
        // label: {
        //   show: false
        // },
        data: seriesData,
        itemStyle: {
          normal: {
            label: {
              show: true,
              position: 'outside',
              color: '#ddd',
              padding: [0, -40, 10, -50],
              overflow: 'none',
              formatter: function (params) {
                let percent = getRate(params.value);
                if (params.value !== '') {
                  return percent + '%\n\n' + params.value + props.unit;
                } else {
                  return '';
                }
              },
            },
            labelLine: {
              length: 25,
              length2: 42,
              show: true,
              color: '#00ffff'
            }
          }
        },
      },
      {
        type: 'pie',
        name: '外层细圆环',
        center: ['45%', '45%'],
        radius: ['57%', '59%'],
        hoverAnimation: false,
        clockWise: false,
        itemStyle: {
          normal: {
            //color: 'rgba(0,213,255,0.3)',
            color: new echarts.graphic.LinearGradient(
              0, 0, 0, 1,
              [
                  {offset: 0, color: 'rgba(0,213,255,1)'},
                  {offset: 0.4, color: 'rgba(0,213,255,0.4)'},
                  {offset: 0.6, color: 'rgba(33,138,253,0.4)'},
                  {offset: 0.8, color: 'rgba(33,138,253,0)'},
                  {offset: 1, color: 'rgba(33,138,253,0)'}
              ]
            )
          },
        },
        label: {
          show: false,
        },
        data: [100],
      },
      {
        type: 'pie',
        name: '外外层细圆环',
        center: ['45%', '45%'],
        radius: ['63%', '64%'],
        hoverAnimation: false,
        clockWise: false,
        itemStyle: {
          normal: {
            //color: 'rgba(0,213,255,0.3)',
            color: new echarts.graphic.LinearGradient(
              0, 0, 0, 1,
              [
                  {offset: 0, color: 'rgba(0,213,255,0.5)'},
                  {offset: 0.3, color: 'rgba(0,213,255,0.1)'},
                  {offset: 0.7, color: 'rgba(0,213,255,0)'},
                  {offset: 1, color: 'rgba(0,213,255,0)'}
              ]
            )
          },
        },
        label: {
          show: false,
        },
        data: [100],
      },

    ],
  }
}
