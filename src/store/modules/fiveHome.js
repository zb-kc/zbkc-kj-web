import { industryHouseList } from '@/views/request'
import { openModal } from '@/until/bigUrl.js'
export default {
  namespaced: true, // 命名空间
  state: {
    count: {},
    name: ''
  },
  getters: {
    getCount (state) {
      console.log(state.count, 'state.count')
      return state.count
    },
    getName (state) {
      console.log(state.name, 'state.name')
      return state.name
    }
  },
  mutations: {
    // getTitle (state, title) {
    //   console.log(state, title, 'title')
    // },
    minus (state, params) {
      console.log(state, 'params---state')
      state.count = params
      console.log(params, 'params')
    }
  },
  actions: {
    asyncGetData ({commit}, payload) {
      let currentName = {
        1: '产业用房',
        2: '行政办公',
        3: '商业',
        4: '公共服务设施',
        5: '住宅'
      }
      console.log(payload, 'payload')
      if (payload.mark === 1) {
        openModal(6, '/estateAnalysis', () => { })
      } else if (payload.mark === 2) {
        openModal(6, '/analysisList', () => { })
      } else if (payload.mark === 3) {
        openModal(6, '/business', () => { })
      }
      industryHouseList(
        { p: 1,
          size: 20,
          index: '',
          type: currentName[payload.mark]
        }
      ).then((res) => {
        console.log(res, '---res---')
        commit('minus', res.data.data)
      })
    }
  }
}
