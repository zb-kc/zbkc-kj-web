import Vue from 'vue'
import Vuex from 'vuex'

// 导入模块
import productRoom from './modules/productRoom'
import fiveHome from './modules/fiveHome'

Vue.use(Vuex)

export default new Vuex.Store({
  // 对模块进行注册
  modules: {
    fiveHome,
    productRoom
  }
})
