// export default function (path) {
//   let pathUrl = ('http://192.168.1.208:9005') + path
//   return pathUrl
// }
import {bigScreenData, landmarkData} from '@/views/request.js'
import MapUtil from '@/until/mapUtil'

let pathUrl = 'http://192.168.1.208:9005'
// let pathUrl = 'http://192.168.1.246:9005'
// let pathUrl = 'http://192.168.1.76:9005'
// let pathUrl = 'http://10.200.62.5:9005'

// 关闭弹框
// 1:左弹框；4：中间小弹框；5：中间中弹框
// 6:中间大弹框；7:中间小弹框，指向三维地图某点
// 11:右弹框
// 注： 关闭按钮在弹窗里面的时候，点击按钮就全部销毁了，不会有回调函数和返回值
export function closeModal (type, path, isLoad) {
  if (type === 6) {
    let url = sessionStorage.getItem('landmarkUrl')
    if (isLoad === 'isMark') {
      window.AC && window.AC.ClearCustomWindowEx({
        callbackFn: function (data) {
          //console.log('ClearCustomWindowEx', data)
        }
      })
    } else if ((path === '/estateHous') || (url === '/estateHous')) {
      landmarkData().then((res) => {
        let streetList = res.data.data
        sessionStorage.setItem('landmark', JSON.stringify(streetList.length))
        for (let i = 0; i <= streetList.length; i++) {
          let areaSite = Array.isArray(streetList) && streetList.length > 0 && streetList[i].address && streetList[i].address.split(',')
          let id = String(i)
          let longitude = Number(areaSite[0])
          let latitude = Number(areaSite[1])
          let {street, area, percentage} = streetList[i]
          let mix = String(percentage)
          let mapUrl = `${pathUrl}/mapSiteMix?streetName=${street}&areaNum=${area}&mix=${mix}`
          window.AC.ShowCustomWindowEx(id, mapUrl,
            longitude,
            latitude,
            24,
            234,
            92,
            1,
            {
              isLocat: false,
              range: [1, 50000],
              pivot: [0, 0],
              //onclickFn: 'MapUtil.locatTo.jiedao(\'440305001000\', true)',
              callbackFn: function (data) {
                //console.dir(data)
              }
            })
        }
      })
    } else {
      bigScreenData().then((res) => {
        let streetList = res.data.data.streetList
        sessionStorage.setItem('landmark', JSON.stringify(streetList.length))
        for (let i = 0; i <= streetList.length; i++) {
          let areaSite = Array.isArray(streetList) && streetList.length > 0 && streetList[i] && streetList[i].address && streetList[i].address.split(',')
          let id = String(i)
          let longitude = Number(areaSite[0])
          let latitude = Number(areaSite[1])
          let {street, area, num, percentage} = streetList[i]
          let mix = String(percentage)
          let mapUrl =
                 (url === '/home' || path === '/home')
                   ? `${pathUrl}/mapSite?streetName=${street}&areaNum=${area}&number=${num}`
                   : `${pathUrl}/mapSiteMix?streetName=${street}&areaNum=${area}&mix=${mix}`

          window.AC.ShowCustomWindowEx(id, mapUrl,
            longitude,
            latitude,
            24,
            234,
            92,
            1,
            {
              isLocat: false,
              range: [1, 50000],
              pivot: [0, 0],
              //onclickFn: 'MapUtil.locatTo.jiedao(\'440305001000\', true)',
              callbackFn: function (data) {
                //console.dir(data)
              }
            })
        }
      })
    }
    setTimeout(() => {
      window.AC && window.AC.HideWindow(type, {
        callbackFn: function ({result}) {
          //console.log('HideWindow', result)
        }
      })
    }, 400)
  } else {
    window.AC && window.AC.HideWindow(type, {
      callbackFn: function ({result}) {
        //console.log('HideWindow', result)
      }
    })
  }
}

// type值与closeModal一致
export function openModal (type, clickUrl, callback) {
  if (type === 6) {
    window.AC && window.AC.ClearCustomWindowEx({
      callbackFn: function (data) {
        //console.log('ClearCustomWindowEx', data)
      }
    })
  }
  // let landmark = sessionStorage.getItem('landmark')
  // if (type === 6 && landmark) {
  //   let num = Number(landmark)
  //   for (let i = 0; i <= num; i++) {
  //     let id = String(i)
  //     window.AC.HideCustomWindowEx(id, {
  //       callbackFn: function (data) {
  //         console.log(data)
  //       }
  //     })
  //   }
  // }
  window.AC && window.AC.ShowWindow(type, `${pathUrl + clickUrl}`, {
    callbackFn: ({result}) => {
      //console.log('ShowWindow', result)
      // result = 0，切换成功
      if (result === 0) callback && callback()
    }
  })
}

export function basePath () {
  return pathUrl
}
