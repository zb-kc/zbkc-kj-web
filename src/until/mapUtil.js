const WINDOW_TYPE = {
  LEFT_MAX_WINDOW: 1, //左侧弹窗 
  CENTER_MIN_WINDOW: 4, //中间小弹窗
  CENTER_MIDDLE_WINDOW: 5, //中间中等弹窗 
  CENTER_MAX_WINDOW: 6 //中间大弹窗 
}
const LOCATION_TYPE = {
  NANSHAN: 1, //南山区
  STREET: 2, //街道
  COMMUNITY: 3, //社区
  ZONE: 4, //小区
  BUILDING: 7, //楼栋
}
const ICON = {

}

const ROOTPATH = window.location.protocol + "//" + window.location.host
export default {
  window: {
    ROOTPATH: ROOTPATH,
    windowType: WINDOW_TYPE,
    close(type) {
      try {
        window.AC.ClearLabels();
        window.AC.HideWindow(type || "6");
      } catch (error) {
        console.log("Close window " + (type || "default(6)"));
      }
    },
    showWindow(type, url) {
      try {
        let rurl = "";
        if (url.indexOf("http") >= 0) {
          rurl = url
        } else {

          url = url.replace(/^\/vs/, '/vs2');
          rurl = this.ROOTPATH + url;
        }
        window.AC.ShowWindow(type, rurl);
      } catch (error) {
        url = url.replace(/^\/vs[2]*/, '');
        window.open(url);
      }
    },
    left(url) {
      this.showWindow(this.windwowType.LEFT_MAX_WINDOW, url);
    },
    min(url) {
      this.showWindow(this.windowType.CENTER_MIN_WINDOW, url);
    },
    middle(url) {
      this.showWindow(this.windowType.CENTER_MIDDLE_WINDOW, url);
    },
    max(url) {
      this.showWindow(this.windowType.CENTER_MAX_WINDOW, url);
    },
  },
  locatTo: {
    locationType: LOCATION_TYPE,
    locaTo(type, codes, isHighlight, cb) {
      this.removeLine();
      if (isHighlight == null) {
        if (codes.length == 1) {
          isHighlight = true;
        } else {
          isHighlight = false;
        }
      }
      try {
        //console.log('isHighlight====',isHighlight)
        window.AC.LocatTo(type, codes, isHighlight, {
          callbackFn: cb || function () {}
        })
        window.AC.PropertySpace.clearBim();
      } catch (error) {
        console.log("location to " + JSON.stringify(codes));
      }
    },
    ClearLabels() {
      try {
        window.AC.ClearLabels()
      } catch (e) {
        console.log("ClearLabels")
      }
    },
    floor(code, wybm) {
      let wycodes = wybm.split('_') || [];
      var fcode = "";
      if (wycodes.length >= 3) {
        fcode = wycodes[2];
        fcode = "F" + fcode;
        try {
          //    this.loudong([code]);
          if (code.length > 19) {
            code = code.substring(0, 19);
          }
          window.AC.HideWindow("4")
          //    window.AC.PropertySpace.changePano(code,fcode); 
          window.AC.Pano.showFloor({
            gbcode: code,
            floor: fcode
          })
        } catch (error) {
          console.log("Floor:: " + JSON.stringify("房屋编码：" + code + " 物业编码：" + wybm + " 楼层::" + fcode))
        }
      }

    },
    NANSHAN() {
      try {
        window.AC.LocatTo(this.locationType.NANSHAN, ["440305"])
      } catch (error) {}
    },
    jiedao(areaid, isHighlight) {
      this.locaTo(this.locationType.STREET, [areaid], isHighlight)
    },
    shequ(areaid, isHighlight) {
      this.locaTo(this.locationType.COMMUNITY, [areaid], isHighlight)
    },
    xiaoqu(areaid, isHighlight) {
      console.log(areaid)
      this.locaTo(this.locationType.ZONE, [areaid], isHighlight)
    },
    loudong(codes, isHighlight, cb) {
      let rcodes = [],
        iscode = {};
      for (let i in codes) {
        let c = codes[i];
        if (c) {
          if (c.length > 19) {
            c = c.substring(0, 19);
          }
          if (!iscode[c]) {
            iscode[c] = true;
            rcodes.push(c);
          }
        }
      }
      this.locaTo(this.locationType.BUILDING, rcodes, isHighlight, cb)
    },
    linePoints: [],
    createLine(coordArray, id, isAppend) {
      let pointsStr = coordArray.points;
      let pointa = pointsStr.split(";");
      let points = [];
      for (let i in pointa) {
        let pp = pointa[i];
        if (pp) {
          let pps = pp.split(',');
          points.push({
            lng: pps[0] * 1,
            lat: pps[1] * 1,
            height: 60
          })
        }
      }
      if (points.length > 0) {
        points.push(points[0]); //形成闭合
        try {
          this.removeLine();
          this.linePoints.push(id);

          window.AC.CreateLineByBMapCoord(points, id, true);
        } catch (error) {
          console.log("CreateLine::" + JSON.stringify(points) + " ID::" + id + ' TITLE::' + coordArray.title)
        }
      } else {
        console.log("CreateLine:: 无效坐标集 " + pointsStr + ' TITLE::' + coordArray.title);
      }
    },
    removeLine() {
      if (this.linePoints.length > 0) {
        try {
          window.AC.DeleteImageLabels(this.linePoints);
        } catch (error) {
          console.log('CreateLine:: Remove :: ' + this.linePoints);
        }
        this.linePoints = [];
      }
    }
  },
  highLight: {
    locationType: LOCATION_TYPE,
    highLight(type, codes) {
      try {
        window.AC.HighlightRegion(type, codes, true)
      } catch (error) {}
    },
    jiedao(areaid, isHighlight) {
      this.highLight(this.locationType.STREET, [areaid])
    },
    shequ(areaid, isHighlight) {
      this.highLight(this.locationType.COMMUNITY, [areaid])
    },
    xiaoqu(areaid, isHighlight) {
      this.highLight(this.locationType.ZONE, [areaid])
    },
    loudong(codes, isHighlight) {
      let rcodes = [],
        iscode = {};
      for (let i in codes) {
        let c = codes[i];
        if (c) {
          if (c.length > 19) {
            c = c.substring(0, 19);
          }
          if (!iscode[c]) {
            iscode[c] = true;
            rcodes.push(c);
          }
        }
      }
      this.highLight(this.locationType.BUILDING, rcodes)
    },
  },
  pointLabel: {
    codes: [],
    ROOTPATH: ROOTPATH,
    locationType: LOCATION_TYPE,
    pointLabel(type, codes, func, icon, isHighlight) {
      this.remove();
      this.codes = codes;
      try {
        if (!func) {
          func = function () {}
        }
        window.AC.LabelClickCallbackFunction["acCallBack"] = func;
        // icon = icon || "/vs/static/img/location.png";
        //icon = "http://10.200.43.87:55500/acimages/zhongbo/zichan.png";
        //icon = '/vs/static/img/location.png';
        icon = 'http://10.200.67.158:55500/acimages/camera64.png';
        isHighlight = isHighlight || false;
        //console.log('codes====',type, codes, icon)
        window.AC.CreateImageLabels(type, codes, icon, "window.AC.LabelClickCallbackFunction.acCallBack")
      } catch (error) {
        //console.log("pointLabel "+JSON.stringify(codes));
      }
    },
    jiedao(areaid, func, isHighlight, icon) {
      this.pointLabel(LOCATION_TYPE.STREET, [areaid], func, icon, isHighlight);
    },
    shequ(areaid, func, isHighlight, icon) {
      this.pointLabel(LOCATION_TYPE.COMMUNITY, [areaid], func, icon, isHighlight);
    },
    xiaoqu(areaid, func, isHighlight, icon) {
      this.pointLabel(LOCATION_TYPE.ZONE, [areaid], func, icon, isHighlight);
    },
    loudong(codes, func, isHighlight, icon) {
      let rcodes = [],
        iscode = {};
      for (let i in codes) {
        let c = codes[i];
        if (c) {
          if (c.length > 19) {
            c = c.substring(0, 19);
          }
          if (!iscode[c]) {
            iscode[c] = true;
            rcodes.push(c);
          }
        }
      }
      this.pointLabel(LOCATION_TYPE.BUILDING, rcodes, func, icon, isHighlight);
    },
    remove() {
      try {
        if (this.codes.length > 0) {
          window.AC.DeleteImageLabels(this.codes);
          this.codes = [];
        }
      } catch (e) {
        console.log("RemoveCodes::" + JSON.stringify(this.codes))
      }
    },
    CreateTableLabel(code, title, data) {
      try {
        window.AC.CreateTableLabel(7, code, title, data, false, true);
      } catch (error) {
        console.log("TableLabel::" + JSON.stringify(data) + " Code::" + code + " TITLE::" + title);
      }
    },
    CreateCustomTextLabel(type, codes) {
      for (let i in codes) {
        let code = codes[i]
        this.codes.push(code.gbcode);
      }
      try {
        window.AC.CreateCustomTextLabel(type, codes);
      } catch (e) {
        console.log("CreateCustomTextLabel::" + JSON.stringify(codes))
      }
    }
  }
}
