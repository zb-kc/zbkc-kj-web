import {get, post} from './request'

// 图片上传
export function grouplist () {
  return post('/zbkc/sysOrg/list')
}

// 请求详细地址
export function groupstatus (obj) {
  return get('/zbkc/sysOrg/status?' + obj)
}
