import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)
const router = new Router({
  linkExactActiveClass: 'act',
  // base: 'kj',
  mode: 'history',
  routes: [
    {
      path: '',
      name: 'index',
      component: () => import('@/views/index.vue')
    },
    {
      path: '/index',
      name: 'index',
      component: () => import('@/views/index.vue')
    },
    {
      // 首页
      path: '/home',
      name: 'home',
      component: () => import('@/views/home')
    },
    {
      // 地图定点地标-首页
      path: '/mapSite',
      name: 'mapSite',
      component: () => import('@/views/mapSite')
    },
    {
      // 首页-四大类资产统计分析-物业列表
      path: '/analysisList',
      name: 'analysisList',
      component: () => import('@/views/home/analysisList')
    },
    {
      // 首页-智能选址搜索-物业列表
      path: '/homeSearchList',
      name: 'homeSearchList',
      component: () => import('@/views/home/homeTabs/selectSearch/searchList')
    },
    {
      // 地图定点地标-占比
      path: '/mapSiteMix',
      name: 'mapSiteMix',
      component: () => import('@/views/mapSiteMix')
    },
    {
      // 政府物业
      path: '/governmentProperty',
      name: 'governmentProperty',
      component: () => import('@/views/governmentProperty')
    },
    {
      // 政府物业
      path: '/moreAnalysis',
      name: 'moreAnalysis',
      component: () => import('@/views/governmentProperty/moreAnalysis')
    },
    {
      // 政府物业-房屋编码核查-核查任务列表
      path: '/totalCheckList',
      name: 'totalCheckList',
      component: () => import('@/views/governmentProperty/checkDetail/totalCheckList')
    },
    {
      // 政府物业-房屋编码核查-异常任务列表
      path: '/irregularCheckList',
      name: 'irregularCheckList',
      component: () => import('@/views/governmentProperty/checkDetail/irregularCheckList')
    },
    {
      // 政府物业-房屋编码核查-已核查任务列表
      path: '/hasCheckList',
      name: 'hasCheckList',
      component: () => import('@/views/governmentProperty/checkDetail/hasCheckList')
    },
    {
      // 政府物业-房屋编码核查-未核查任务列表
      path: '/noneCheckList',
      name: 'noneCheckList',
      component: () => import('@/views/governmentProperty/checkDetail/noneCheckList')
    },
    {
      // 办公用房
      path: '/officeHouse',
      name: 'officeHouse',
      component: () => import('@/views/governmentProperty/officeHouse')
    },
    {
      // 办公用房-统计分析
      path: '/officeStatAnalysis',
      name: 'officeStatAnalysis',
      component: () => import('@/views/governmentProperty/officeHouse/officeRoom/officeStatAnalysis')
    },
    {
      // 公共服务设施-统计分析
      path: '/propertyStatAnalysis',
      name: 'propertyStatAnalysis',
      component: () => import('@/views/governmentProperty/publicFacilitie/propertyFacilitie/statAnalysis')
    },
    {
      // 商业-统计分析
      path: '/businessStatAnalysis',
      name: 'businessStatAnalysis',
      component: () => import('@/views/governmentProperty/business/propertyBusiness/statAnalysis')
    },
    {
      // 住宅-统计分析
      path: '/residenceStatAnalysis',
      name: 'residenceStatAnalysis',
      component: () => import('@/views/governmentProperty/residence/propertyResidence/statAnalysis')
    },
    {
      // 产业用房
      path: '/estateHous',
      name: 'estateHous',
      component: () => import('@/views/governmentProperty/estateHouse')
    },
    {
      // 公共服务设施
      path: '/publicFacilitie',
      name: 'publicFacilitie',
      component: () => import('@/views/governmentProperty/publicFacilitie')
    },
    {
      // 商业
      path: '/business',
      name: 'business',
      component: () => import('@/views/governmentProperty/business')
    },
    {
      // 住宅
      path: '/residence',
      name: 'residence',
      component: () => import('@/views/governmentProperty/residence')
    },
    {
      // 租赁社会物业
      path: '/rentSociety',
      name: 'rentSociety',
      component: () => import('@/views/governmentProperty/rentSociety')
    },
    {
      // 租赁社会物业-更多分析
      path: '/rentMoreAnalysis',
      name: 'rentMoreAnalysis',
      component: () => import('@/views/governmentProperty/rentSociety/propertyRent/moreAnalysis')
    },
    {
      // 租赁社会物业-物业列表左侧详情
      path: '/rentLeftDetail',
      name: 'rentLeftDetail',
      component: () => import('@/views/governmentProperty/rentSociety/propertyList/listDetail')
    },
    {
      // 租赁社会物业-物业列表中弹窗概况
      path: '/listOverview',
      name: 'listOverview',
      component: () => import('@/views/governmentProperty/rentSociety/propertyList/listDetail/listOverview')
    },
    {
      // 物业列表详情-左侧(除产业用房)
      path: '/leftDetail',
      name: 'leftDetail',
      component: () => import('@/views/governmentProperty/listDetail/leftDetail')
    },
    {
      // 物业列表详情-中间物业概况(除产业用房)
      path: '/centerDetail',
      name: 'centerDetail',
      component: () => import('@/views/governmentProperty/listDetail/centerDetail')
    },
    {
      // 产业用房统计分析
      path: '/estateAnalysis',
      name: 'estateAnalysis',
      component: () => import('@/views/home/estateAnalysis')
    },
    {
      // 产业用房统计分析
      path: '/buse',
      name: 'buse',
      component: () => import('@/views/home/estateAnalysis')
    },
    // {
    //   // 办公用房统计分析
    //   path: '/officeAnalysis',
    //   name: 'officeAnalysis',
    //   component: () => import('@/views/home/officeAnalysis')
    // },
    // {
    //   // 商业用房统计分析
    //   path: '/businessAnalysis',
    //   name: 'businessAnalysis',
    //   component: () => import('@/views/home/businessAnalysis')
    // },
    // {
    //   // 公共服务设施统计分析
    //   path: '/facilitiesAnalysis',
    //   name: 'facilitiesAnalysis',
    //   component: () => import('@/views/home/facilitiesAnalysis')
    // },
    // {
    //   // 住宅统计分析
    //   path: '/dwellingAnalysis',
    //   name: 'dwellingAnalysis',
    //   component: () => import('@/views/home/dwellingAnalysis')
    // },
    {
      // 产业用房-预到期企业列表
      path: '/preDue',
      name: 'preDue',
      component: () => import('@/views/governmentProperty/estateHouse/propertyHouse/preDue')
    },
    {
      // 产业用房-预到期企业详情
      path: '/companyDetail',
      name: 'companyDetail',
      component: () => import('@/views/governmentProperty/estateHouse/propertyHouse/preDue/dataDetail/companyDetail')
    },
    {
      // 产业用房-重点企业标签
      path: '/companyTag',
      name: 'companyTag',
      component: () => import('@/views/governmentProperty/estateHouse/propertyHouse/companyTag')
    },
    {
      // 产业用房-物业列表-物业详情
      path: '/parkDetail',
      name: 'parkDetail',
      component: () => import('@/views/governmentProperty/estateHouse/parkList/parkDetail')
    },
    {
      // 产业用房-物业列表-物业详情-园区概况
      path: '/parkMoreAnalysis',
      name: 'parkMoreAnalysis',
      component: () => import('@/views/governmentProperty/estateHouse/parkList/parkDetail/parkMoreAnalysis')
    },
    {
      // 产业用房-物业列表-物业详情-园区照片
      path: '/parkImg',
      name: 'parkImg',
      component: () => import('@/views/governmentProperty/estateHouse/parkList/parkDetail/parkImg')
    },
    {
      // 产业用房-物业列表-物业详情-园区VR
      path: '/parkVR',
      name: 'parkVR',
      component: () => import('@/views/governmentProperty/estateHouse/parkList/parkDetail/parkVR')
    },
    {
      // 国企物业
      path: '/stateProperty',
      name: 'stateProperty',
      component: () => import('@/views/stateProperty')
    },
    {
      // 国企物业-深汇通
      path: '/shenHuiTong',
      name: 'shenHuiTong',
      component: () => import('@/views/stateProperty/shenHuiTong')
    },
    {
      // 国企物业-深汇通-物业列表详情
      path: '/SHTLeftDetail',
      name: 'SHTLeftDetail',
      component: () => import('@/views/stateProperty/shenHuiTong/propertyList/listDetail/leftDetail')
    },
    {
      // 国企物业-深汇通-物业详情
      path: '/SHTCenterDetail',
      name: 'SHTCenterDetail',
      component: () => import('@/views/stateProperty/shenHuiTong/propertyList/listDetail/centerDetail')
    },
    {
      // 国企物业-园区照片
      path: '/stateParkImg',
      name: 'stateParkImg',
      component: () => import('@/views/stateProperty/parkImg')
    },
    {
      // 国企物业-园区VR
      path: '/stateParkVR',
      name: 'stateParkVR',
      component: () => import('@/views/stateProperty/parkVR')
    }
  ]
})

export default router
